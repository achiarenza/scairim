﻿Imports Newtonsoft.Json.Linq

Public Class frmLoginTV

    Dim oJson As JObject
    Private Sub frmLoginTV_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Icon = Scairim.Icon
        Dim file As String = IO.Path.Combine(IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Scairim"), "temp.json")
        If IO.File.Exists(file) Then
            oJson = JObject.Parse(IO.File.ReadAllText(file))
            If oJson.ContainsKey("UsernameTV") Then
                txtUsr.Text = oJson.Item("UsernameTV").ToString
            End If
            If oJson.ContainsKey("TokenTV") Then
                txtToken.Text = oJson.Item("TokenTV").ToString
            End If
        End If
    End Sub

    Private Sub btnLogin_Click(sender As Object, e As EventArgs) Handles btnLogin.Click
        Scairim.tokenTeamviewer = txtToken.Text
        ScriviInJson("temp.json", "TokenTV", Scairim.tokenTeamviewer)
        Logga("Token impostato.")
        Hide()
    End Sub
End Class