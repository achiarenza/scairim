﻿Imports System.Net
Imports System.IO
Imports Newtonsoft.Json.Linq
Imports Microsoft.Office.Interop.Excel
Imports System.Text

Module Utilities

    Public timeout = 120000
    Dim oHttp As HttpWebRequest
    Dim rHttp As HttpWebResponse
    Dim oJsonW, oJsonR, oJsonR2, oJsonR3 As JObject
    Dim oJArray As JArray
    Dim ScairimReportsHost As String = "it-srv002.mcubedigital.local"
    Dim usernameShare As String = "ScairimReports"
    Dim passwordShare As String = "8OE-_ze1DFzh3QrK0nVjRNyTU"
    Dim domainShare As String = "mcubedigital.local"
    'Dim FolderShare As String = "\\" + ScairimReportsHost + "\share_service\"
    Dim FolderShare As String = "\\it-nas1.mcubedigital.local\mcube-rho\ScairimPlayerUpdate\"
    'Dim agentUUID As String = "21414b52-3de1-45f1-a5a1-c4834701b838"
    Dim agentUUID As String = "030d48a8-c7ef-4a21-b1ba-a1f2aaa16273"
    'Dim playerUpdateAPI As String = "http://localhost:55963/api/PlayerUpdate"
    Dim playerUpdateAPI As String = "http://globalmonitoring.mcubeglobal.com/ScairimInterface/api/PlayerUpdate"

    '##############################
    '        Utility Zone
    '##############################

#Region "UtilityZone"

    Sub Loading(Action As String, Optional calledby As String = "")
        Select Case Action
            Case "show"
                frmWait.Show()
                frmWait.Refresh()
            Case "close"
                frmWait.Close()
            Case Else
                frmWait.Text = "Loading... " & Action & "%"
                frmWait.Refresh()
        End Select
    End Sub

    Sub Logga(text As String, Optional linea As Integer = 0)
        Dim lineaTesto
        Dim riga = ""

        If linea <> 0 Then
            riga = "Linea: " & linea.ToString & " - "
        End If

        Select Case text
            Case "-"
                lineaTesto = "-----------------------------------------------------------"
            Case ""
                lineaTesto = vbNewLine
            Case Else
                lineaTesto = "[" & Now() & "] - " & riga & text
        End Select
        Scairim.txtLog.AppendText(lineaTesto & vbNewLine)
    End Sub

    Function HttpSend(pMessage As String, pWsUrl As String, pHeaders As Object, pMethod As String, pResponseType As String, pTimeout As Integer)
        On Error Resume Next
        If pWsUrl.ToLower.Contains("https") Then
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 Or SecurityProtocolType.Tls Or SecurityProtocolType.Tls11 Or SecurityProtocolType.Tls12
        End If
        oHttp = CType(WebRequest.Create(pWsUrl), HttpWebRequest)
        oHttp.Timeout = pTimeout
        oHttp.Method = pMethod
        For x = 0 To UBound(pHeaders, 1)
            Select Case pHeaders(x, 0)
                Case "Accept"
                    oHttp.Accept = pHeaders(x, 1)
                Case "Content-Length"
                    oHttp.ContentLength = CType(pHeaders(x, 1), Long)
                Case "Content-Type"
                    oHttp.ContentType = pHeaders(x, 1)
                Case Else
                    oHttp.Headers.Add(pHeaders(x, 0), pHeaders(x, 1))
            End Select
        Next

        If Not pMessage = "" Then
            Dim Stream = New StreamWriter(oHttp.GetRequestStream())
            'Dim byteString As Byte() = Encoding.UTF8.GetBytes(pMessage)
            'Logga(Encoding.UTF8.GetString(byteString))
            'Stream.Write(byteString, 0, byteString.Length)
            Stream.Write(pMessage)
            Stream.Flush()
            Stream.Close()
            Stream = Nothing
        End If

        rHttp = CType(oHttp.GetResponse(), HttpWebResponse)

        If Err.Number = 0 Then
            Select Case pResponseType
                Case "text"
                    Dim Reader = New StreamReader(rHttp.GetResponseStream())
                    HttpSend = Reader.ReadToEnd()
                    Reader = Nothing
                Case Else
                    HttpSend = rHttp.StatusCode.ToString()
            End Select
        Else
            HttpSend = "ERROR - " & Err.Number & "-" & Err.Description
        End If

        'oHttp.GetResponse().Close()
        'rHttp.Close()
        'oHttp.ServicePoint.CloseConnectionGroup(oHttp.ConnectionGroupName)
        oHttp.Abort()
        oHttp = Nothing
        rHttp = Nothing
        Err.Clear()
    End Function

    Function FTPSend(file As String, Path As String, user As String, pass As String)
        Dim req As FtpWebRequest = CType(WebRequest.Create(Path), FtpWebRequest)
        req.Method = WebRequestMethods.Ftp.UploadFile
        req.Credentials = New NetworkCredential(user, pass)
        Dim fileContents As Byte()
        Using sourceStream As StreamReader = New StreamReader(file)
            fileContents = Encoding.UTF8.GetBytes(sourceStream.ReadToEnd())
        End Using
        req.ContentLength = fileContents.Length
        Using requestStream As Stream = req.GetRequestStream
            requestStream.Write(fileContents, 0, fileContents.Length)
        End Using
        Dim response As FtpWebResponse = CType(req.GetResponse, FtpWebResponse)
        FTPSend = response.StatusDescription
    End Function

    Public Sub ReleaseComObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        End Try
    End Sub

    Function IsExcelFile(file As String)
        If IO.Path.GetExtension(file) = ".xlsx" Or IO.Path.GetExtension(file) = ".xls" Then
            IsExcelFile = True
        Else
            IsExcelFile = False
        End If
    End Function

    Function AddTextComma(text As String, textToAdd As String, Optional find As Boolean = False)
        Dim Trovato = False
        If text = "" Then
            AddTextComma = textToAdd
        Else
            If find Then
                For Each p In Split(text, ",")
                    If p = textToAdd Then
                        Trovato = True
                    End If
                Next
                If Trovato Then
                    AddTextComma = text
                Else
                    AddTextComma = text & "," & textToAdd
                End If
            Else
                AddTextComma = text & "," & textToAdd
            End If
        End If
    End Function

    Sub ScriviIn(filename As String, text As String, Optional SearchForDuplicate As Boolean = False)
        Dim path As String = IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Scairim")
        Dim file As String = IO.Path.Combine(path, filename)
        Dim trovato As Boolean = False
        Directory.CreateDirectory(IO.Path.GetDirectoryName(file))
        If SearchForDuplicate = True And IO.File.Exists(file) Then
            For Each line In IO.File.ReadLines(file)
                If line = text Then
                    trovato = True
                End If
            Next
        End If
        If trovato = False Then
            Using sw As StreamWriter = IO.File.AppendText(file)
                sw.WriteLine(text)
            End Using
        End If
    End Sub

    Sub ScriviInJson(filename As String, key As String, value As String)
        Dim path As String = IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Scairim")
        Dim file As String = IO.Path.Combine(path, filename)
        Dim oJson As JObject
        Dim trovato As Boolean = False
        Directory.CreateDirectory(IO.Path.GetDirectoryName(file))
        If IO.File.Exists(file) Then
            oJson = JObject.Parse(IO.File.ReadAllText(file))
        Else
            oJson = New JObject()
        End If
        If key = "CM" Then
            If oJson.ContainsKey("CMs") Then
                For Each item In oJson.Item("CMs").Children()
                    If item.ToString = value Then
                        trovato = True
                    End If
                Next
                If trovato = False Then
                    oJson.Item("CMs").Value(Of JArray).Add(value)
                End If
            Else
                oJson.Add(New JProperty("CMs", New JArray(value)))
            End If
        Else
            If oJson.ContainsKey(key) Then
                If oJson.Item(key).ToString <> value Then
                    oJson.Item(key) = value
                End If
            Else
                oJson.Add(key, value)
            End If
        End If
        IO.File.WriteAllText(file, oJson.ToString)
        oJson.RemoveAll()
        oJson = Nothing
    End Sub

    Public Sub EstraiInExcel(dtgridExport As DataGridView)
        Dim excelfile As String
        Dim xlApp As Application = New Application()

        If xlApp Is Nothing Then
            Logga("Excel Non è installato nel sistema!!")
            Return
        End If

        Dim savefile As New SaveFileDialog With {
            .InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyComputer),
            .Filter = "excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*",
            .FilterIndex = 1,
            .RestoreDirectory = True,
            .OverwritePrompt = True
        }
        If savefile.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            excelfile = savefile.FileName
        Else
            Return
        End If

        Loading("show")

        Dim xlWorkBook As Workbook
        xlWorkBook = xlApp.Workbooks.Add()
        Dim xlWorkSheet = xlWorkBook.Sheets(1)

        For Each column In dtgridExport.Columns
            xlWorkSheet.Cells(1, column.Index + 1).Value = column.HeaderText
        Next

        Dim i As Integer = 2
        Dim c As Integer = 1
        Dim Contatore = 0
        For Each row In dtgridExport.Rows
            If Contatore Mod 10 = 0 Then
                Loading(CInt((Contatore * 100) / dtgridExport.Rows.Count))
            End If
            c = 1
            For Each column In dtgridExport.Columns
                If Not dtgridExport.Item(column.Index, row.Index).Value Is Nothing Then
                    xlWorkSheet.Cells(i, c).Value = dtgridExport.Item(column.Index, row.Index).Value.ToString
                End If
                c += 1
            Next
            i += 1
        Next

        xlWorkBook.SaveAs(excelfile)
        xlWorkBook.Close()
        xlApp.Quit()

        ReleaseComObject(xlWorkSheet)
        ReleaseComObject(xlWorkBook)
        ReleaseComObject(xlApp)

        Loading("close")

        Logga("Estrazione Esportata!")
        MsgBox("Estrazione Esportata!")
    End Sub

    Public Function ReplaceName(PlayerName As String)
        ReplaceName = Replace(Replace(Replace(Replace(Replace(PlayerName, "é", "e"), "à", "a"), "è", "e"), "ò", "o"), "ù", "u")
    End Function

    Public Function RemoveSpecialChars(Text As String)
        RemoveSpecialChars = Replace(Replace(Replace(Replace(Replace(Text, "é", "e"), "à", "a"), "è", "e"), "ò", "o"), "ù", "u")
    End Function

    Public Class AliasAccount
        Private _username, _password, _domainname As String

        Private _tokenHandle As New IntPtr(0)
        Private _dupeTokenHandle As New IntPtr(0)
        Private _impersonatedUser As System.Security.Principal.WindowsImpersonationContext


        Public Sub New(ByVal username As String, ByVal password As String)
            Dim nameparts() As String = username.Split("\")
            If nameparts.Length > 1 Then
                _domainname = nameparts(0)
                _username = nameparts(1)
            Else
                _username = username
            End If
            _password = password
        End Sub

        Public Sub New(ByVal username As String, ByVal password As String, ByVal domainname As String)
            _username = username
            _password = password
            _domainname = domainname
        End Sub


        Public Sub BeginImpersonation()
            'Const LOGON32_PROVIDER_DEFAULT As Integer = 0
            'Const LOGON32_LOGON_INTERACTIVE As Integer = 2
            Const LOGON32_LOGON_NEW_CREDENTIALS As Integer = 9
            Const LOGON32_PROVIDER_WINNT50 As Integer = 3
            Const SecurityImpersonation As Integer = 2

            Dim win32ErrorNumber As Integer

            _tokenHandle = IntPtr.Zero
            _dupeTokenHandle = IntPtr.Zero

            If Not LogonUser(_username, _domainname, _password, LOGON32_LOGON_NEW_CREDENTIALS, LOGON32_PROVIDER_WINNT50, _tokenHandle) Then
                win32ErrorNumber = System.Runtime.InteropServices.Marshal.GetLastWin32Error()
                Throw New ImpersonationException(win32ErrorNumber, GetErrorMessage(win32ErrorNumber), _username, _domainname)
            End If

            If Not DuplicateToken(_tokenHandle, SecurityImpersonation, _dupeTokenHandle) Then
                win32ErrorNumber = System.Runtime.InteropServices.Marshal.GetLastWin32Error()

                CloseHandle(_tokenHandle)
                Throw New ImpersonationException(win32ErrorNumber, "Unable to duplicate token!", _username, _domainname)
            End If

            Dim newId As New System.Security.Principal.WindowsIdentity(_dupeTokenHandle)
            _impersonatedUser = newId.Impersonate()
        End Sub


        Public Sub EndImpersonation()
            If Not _impersonatedUser Is Nothing Then
                _impersonatedUser.Undo()
                _impersonatedUser = Nothing

                If Not System.IntPtr.op_Equality(_tokenHandle, IntPtr.Zero) Then
                    CloseHandle(_tokenHandle)
                End If
                If Not System.IntPtr.op_Equality(_dupeTokenHandle, IntPtr.Zero) Then
                    CloseHandle(_dupeTokenHandle)
                End If
            End If
        End Sub


        Public ReadOnly Property username() As String
            Get
                Return _username
            End Get
        End Property

        Public ReadOnly Property domainname() As String
            Get
                Return _domainname
            End Get
        End Property


        Public ReadOnly Property currentWindowsUsername() As String
            Get
                Return System.Security.Principal.WindowsIdentity.GetCurrent().Name
            End Get
        End Property


#Region "Exception Class"
        Public Class ImpersonationException
            Inherits System.Exception

            Public ReadOnly win32ErrorNumber As Integer

            Public Sub New(ByVal win32ErrorNumber As Integer, ByVal msg As String, ByVal username As String, ByVal domainname As String)
                MyBase.New(String.Format("Impersonation of {1}\{0} failed! [{2}] {3}", username, domainname, win32ErrorNumber, msg))
                Me.win32ErrorNumber = win32ErrorNumber
            End Sub
        End Class
#End Region


#Region "External Declarations and Helpers"
        Private Declare Auto Function LogonUser Lib "advapi32.dll" (ByVal lpszUsername As [String],
          ByVal lpszDomain As [String], ByVal lpszPassword As [String],
          ByVal dwLogonType As Integer, ByVal dwLogonProvider As Integer,
          ByRef phToken As IntPtr) As Boolean


        Private Declare Auto Function DuplicateToken Lib "advapi32.dll" (ByVal ExistingTokenHandle As IntPtr,
              ByVal SECURITY_IMPERSONATION_LEVEL As Integer,
              ByRef DuplicateTokenHandle As IntPtr) As Boolean


        Private Declare Auto Function CloseHandle Lib "kernel32.dll" (ByVal handle As IntPtr) As Boolean


        <System.Runtime.InteropServices.DllImport("kernel32.dll")>
        Private Shared Function FormatMessage(ByVal dwFlags As Integer, ByRef lpSource As IntPtr,
          ByVal dwMessageId As Integer, ByVal dwLanguageId As Integer, ByRef lpBuffer As [String],
          ByVal nSize As Integer, ByRef Arguments As IntPtr) As Integer
        End Function


        Private Function GetErrorMessage(ByVal errorCode As Integer) As String
            Dim FORMAT_MESSAGE_ALLOCATE_BUFFER As Integer = &H100
            Dim FORMAT_MESSAGE_IGNORE_INSERTS As Integer = &H200
            Dim FORMAT_MESSAGE_FROM_SYSTEM As Integer = &H1000

            Dim messageSize As Integer = 255
            Dim lpMsgBuf As String = ""
            Dim dwFlags As Integer = FORMAT_MESSAGE_ALLOCATE_BUFFER Or FORMAT_MESSAGE_FROM_SYSTEM Or FORMAT_MESSAGE_IGNORE_INSERTS

            Dim ptrlpSource As IntPtr = IntPtr.Zero
            Dim prtArguments As IntPtr = IntPtr.Zero

            Dim retVal As Integer = FormatMessage(dwFlags, ptrlpSource, errorCode, 0, lpMsgBuf, messageSize, prtArguments)
            If 0 = retVal Then
                Throw New System.Exception("Failed to format message for error code " + errorCode.ToString() + ". ")
            End If

            Return lpMsgBuf
        End Function

#End Region

    End Class

    Public Function SendFileToShare(FileName As String, FileText As String)
        Try
            Dim impersinator As New AliasAccount(usernameShare, passwordShare, domainShare)
            impersinator.BeginImpersonation()
            IO.File.WriteAllText(FolderShare & FileName, FileText)
            impersinator.EndImpersonation()
            SendFileToShare = "done"
        Catch ex As Exception
            Logga("ERROR - SendFileToShare: " & ex.ToString)
            SendFileToShare = "ERROR - SendFileToShare: " & ex.ToString
        End Try
    End Function

    Public Function DeleteFileFromShare(FileName As String)
        Try
            Dim impersinator As New AliasAccount(usernameShare, passwordShare, domainShare)
            impersinator.BeginImpersonation()
            IO.File.Delete(FolderShare & FileName)
            impersinator.EndImpersonation()
            DeleteFileFromShare = "done"
        Catch ex As Exception
            Logga("ERROR - DeleteFileFromShare: " & ex.ToString)
            DeleteFileFromShare = "ERROR - DeleteFileFromShare: " & ex.ToString
        End Try
    End Function

    Public Function ReadFileFromShare(ByRef list As List(Of String))
        Try
            Dim impersinator As New AliasAccount(usernameShare, passwordShare, domainShare)
            impersinator.BeginImpersonation()
            For Each file In Directory.GetFiles(FolderShare)
                list.Add(IO.File.ReadAllText(file))
            Next
            impersinator.EndImpersonation()
            ReadFileFromShare = "done"
        Catch ex As Exception
            Logga("ERROR - ReadFileFromShare: " & ex.ToString)
            ReadFileFromShare = "ERROR - ReadFileFromShare: " & ex.ToString
        End Try
    End Function

    Public Function ReadFileFromShareElaborati(ByRef list As List(Of String))
        Try
            Dim impersinator As New AliasAccount(usernameShare, passwordShare, domainShare)
            impersinator.BeginImpersonation()
            For Each file In Directory.GetFiles(FolderShare + "Elaborati\")
                list.Add(IO.File.ReadAllText(file))
            Next
            impersinator.EndImpersonation()
            ReadFileFromShareElaborati = "done"
        Catch ex As Exception
            Logga("ERROR - ReadFileFromShareElaborati: " & ex.ToString)
            ReadFileFromShareElaborati = "ERROR - ReadFileFromShareElaborati: " & ex.ToString
        End Try
    End Function

#End Region

    '##############################
    '  Basic Scala Function Zone
    '##############################

#Region "BasicScalaFunctionZone"

    Function GetLogin(pUser As String, pPass As String, cUrlWebService As String)
        Dim jsonTxt
        oJsonW = New JObject(
            New JProperty("username", pUser),
            New JProperty("password", pPass),
            New JProperty("networkId", 0),
            New JProperty("rememberMe", False))
        jsonTxt = oJsonW.ToString()
        oJsonW.RemoveAll()

        Dim headers(2, 1)
        headers(0, 0) = "Accept"
        headers(0, 1) = "application/json"
        headers(1, 0) = "Content-Length"
        headers(1, 1) = Len(jsonTxt)
        headers(2, 0) = "Content-Type"
        headers(2, 1) = "application/json; charset=utf-8"

        GetLogin = HttpSend(jsonTxt, cUrlWebService & "/api/rest/auth/login", headers, "POST", "text", timeout)
    End Function

    Function GetLogout(pToken As String, pApiToken As String, cUrlWebService As String)
        Dim headers(3, 1)
        headers(0, 0) = "Accept"
        headers(0, 1) = "application/json"
        headers(1, 0) = "Content-Length"
        headers(1, 1) = 0
        headers(2, 0) = "Content-Type"
        headers(2, 1) = "application/json; charset=utf-8"
        headers(3, 0) = "apiToken"
        headers(3, 1) = pApiToken
        GetLogout = HttpSend("", cUrlWebService & "/api/rest/auth/logout/?" & "token=" & pToken, headers, "GET", "", timeout)
    End Function

    Function GetStuffs(pToken As String, pApiToken As String, pStuffs As String)
        Dim headers(3, 1)
        headers(0, 0) = "Accept"
        headers(0, 1) = "application/json"
        headers(1, 0) = "Content-Length"
        headers(1, 1) = 0
        headers(2, 0) = "Content-Type"
        headers(2, 1) = "application/json; charset=utf-8"
        headers(3, 0) = "apiToken"
        headers(3, 1) = pApiToken
        GetStuffs = HttpSend("", pStuffs, headers, "GET", "text", timeout)
    End Function

    Function GetStuffsBearer(bearerToken As String, pStuffs As String)
        Dim headers(1, 1)
        headers(0, 0) = "Authorization"
        headers(0, 1) = "Bearer " & bearerToken
        headers(1, 0) = "Content-Type"
        headers(1, 1) = "application/json"
        GetStuffsBearer = HttpSend("", pStuffs, headers, "GET", "text", timeout)
    End Function

    Function UpdateStuffsBearer(bearerToken As String, jsontxt As String, pStuffs As String)
        Dim headers(2, 1)
        headers(0, 0) = "Authorization"
        headers(0, 1) = "Bearer " & bearerToken
        headers(1, 0) = "Content-Type"
        headers(1, 1) = "application/json"
        headers(2, 0) = "Content-Length"
        headers(2, 1) = Len(jsontxt)
        UpdateStuffsBearer = HttpSend(jsontxt, pStuffs, headers, "PUT", "text", timeout)
    End Function

    Function CreatePlayer(pToken As String, pApiToken As String, jsontxt As String, url As String)
        Dim headers(3, 1)
        headers(0, 0) = "Accept"
        headers(0, 1) = "application/json"
        headers(1, 0) = "Content-Length"
        headers(1, 1) = Len(jsontxt)
        headers(2, 0) = "Content-Type"
        headers(2, 1) = "application/json; charset=utf-8"
        headers(3, 0) = "apiToken"
        headers(3, 1) = pApiToken
        CreatePlayer = HttpSend(jsontxt, url & "/api/rest/players", headers, "POST", "text", timeout)
    End Function

    Function ScheduleUpdatePlayer(jsontxt As String)
        Dim headers(3, 1)
        headers(0, 0) = "Accept"
        headers(0, 1) = "application/json"
        headers(1, 0) = "Content-Length"
        headers(1, 1) = Len(jsontxt)
        headers(2, 0) = "Content-Type"
        headers(2, 1) = "application/json"
        headers(3, 0) = "uuid"
        headers(3, 1) = agentUUID
        ScheduleUpdatePlayer = HttpSend(jsontxt, playerUpdateAPI, headers, "POST", "text", timeout)
    End Function

    Function GetUpdatePlayer(link As String)
        Dim headers(3, 1)
        headers(0, 0) = "Accept"
        headers(0, 1) = "application/json"
        headers(1, 0) = "Content-Length"
        headers(1, 1) = 0
        headers(2, 0) = "Content-Type"
        headers(2, 1) = "application/json"
        headers(3, 0) = "uuid"
        headers(3, 1) = agentUUID
        GetUpdatePlayer = HttpSend("", playerUpdateAPI + "?link=" + link, headers, "GET", "text", timeout)
    End Function

    Function DeleteUpdatePlayer(ids As String)
        Dim headers(3, 1)
        headers(0, 0) = "Accept"
        headers(0, 1) = "application/json"
        headers(1, 0) = "Content-Length"
        headers(1, 1) = 0
        headers(2, 0) = "Content-Type"
        headers(2, 1) = "application/json"
        headers(3, 0) = "uuid"
        headers(3, 1) = agentUUID
        DeleteUpdatePlayer = HttpSend("", playerUpdateAPI + "?ids=" + ids, headers, "DELETE", "text", timeout)
    End Function

    Function UpdateStuffs(pToken As String, pApiToken As String, jsontxt As String, pStuffs As String)
        Dim headers(3, 1)
        headers(0, 0) = "Accept"
        headers(0, 1) = "application/json"
        headers(1, 0) = "Content-Length"
        headers(1, 1) = Len(jsontxt)
        headers(2, 0) = "Content-Type"
        headers(2, 1) = "application/json; charset=utf-8"
        headers(3, 0) = "apiToken"
        headers(3, 1) = pApiToken
        UpdateStuffs = HttpSend(jsontxt, pStuffs, headers, "PUT", "text", timeout)
    End Function

    Function CreateStuffs(pToken As String, pApiToken As String, jsontxt As String, pStuffs As String)
        Dim headers(3, 1)
        headers(0, 0) = "Accept"
        headers(0, 1) = "application/json"
        headers(1, 0) = "Content-Length"
        headers(1, 1) = Len(jsontxt)
        headers(2, 0) = "Content-Type"
        headers(2, 1) = "application/json; charset=utf-8"
        headers(3, 0) = "apiToken"
        headers(3, 1) = pApiToken
        CreateStuffs = HttpSend(jsontxt, pStuffs, headers, "POST", "text", timeout)
    End Function

    Function DeleteStuffs(pToken As String, pApiToken As String, pStuffs As String)
        Dim headers(3, 1)
        headers(0, 0) = "Accept"
        headers(0, 1) = "application/json"
        headers(1, 0) = "Content-Length"
        headers(1, 1) = 0
        headers(2, 0) = "Content-Type"
        headers(2, 1) = "application/json; charset=utf-8"
        headers(3, 0) = "apiToken"
        headers(3, 1) = pApiToken
        DeleteStuffs = HttpSend("", pStuffs, headers, "DELETE", "text", timeout)
    End Function

#End Region

    '##############################
    '  Generic Json Function Zone
    '##############################

#Region "GenericJsonFunctionZone"

    Function Parsa(pJson As String)
        Try
            oJsonR = JObject.Parse(pJson)
            Parsa = True
        Catch e As Exception
            Logga("ERROR - Parsa: " & e.ToString)
            Parsa = False
        End Try
    End Function

    Function Parsa2(pJson As String)
        Try
            oJsonR2 = JObject.Parse(pJson)
            Parsa2 = True
        Catch e As Exception
            Logga("ERROR - Parsa: " & e.ToString)
            Parsa2 = False
        End Try
    End Function

    Function EstraiFromJsonSingleValue(toExtract As String)
        EstraiFromJsonSingleValue = oJsonR.GetValue(toExtract).ToString
    End Function

#End Region

    '#################################
    'Generic Scala-Json Function Zone
    '#################################

#Region "GenericScalaJsonFunctionZone"

    Function PlayerExists(PlayerName As String)
        PlayerExists = False
        If GetPlayerId(PlayerName) = "" Then
            PlayerExists = False
        Else
            PlayerExists = True
        End If
    End Function

    Function GetPlayerId(PlayerName As String)
        Dim result = GetStuffs(Scairim.token, Scairim.apiToken, Scairim.LinkCM & "/api/rest/players?search=" & PlayerName)
        If InStr(result, "ERROR") > 0 Then
            Logga("ERROR - GetPlayerId: Ricerca Player sul CM fallita. Controlla di aver fatto il Login correttamente.")
            GetPlayerId = ""
        Else
            If Parsa(result) = True Then
                If oJsonR.ContainsKey("list") Then
                    GetPlayerId = ""
                    For Each child In oJsonR.Item("list").Children()
                        If child.Item("name").ToString = PlayerName Then
                            GetPlayerId = child.Item("id").ToString
                        End If
                    Next
                Else
                    GetPlayerId = ""
                End If
            Else
                Logga("ERROR - GetPlayerId: Parsing JSON Fallito.")
                GetPlayerId = ""
            End If
        End If
    End Function

    Function GetMetadatoValue(txtJson As String, metadataName As String, Optional NotSet As Boolean = False)
        Dim Json As JObject
        Dim temp As String = ""
        If NotSet Then
            temp = "Not Set"
        End If
        If Not metadataName.Contains("Player") Then
            metadataName = "Player." & metadataName
        End If
        Json = JObject.Parse(txtJson)
        If Json.ContainsKey("metadataValue") Then
            For Each metadato In Json.Item("metadataValue").Children
                If metadato.Item("playerMetadata").Item("name").ToString = metadataName Then
                    If metadato.Item("playerMetadata").Item("valueType").ToString = "PICKLIST" Then
                        For Each value In metadato.Item("playerMetadata").Item("predefinedValues").Children()
                            If value.Item("id").ToString = metadato.Item("value").ToString Then
                                temp = value.Item("value").ToString()
                                Exit For
                            End If
                        Next
                    End If
                    If metadato.Item("playerMetadata").Item("valueType").ToString = "ANY" Then
                        temp = metadato.Item("value").ToString
                    End If
                    Exit For
                End If
            Next
        End If
        GetMetadatoValue = temp
    End Function

    Function GetMetadatoValue(Meta As JObject)
        Dim temp As String = ""
        If Meta.Item("playerMetadata").Item("valueType").ToString = "PICKLIST" Then
            For Each value In Meta.Item("playerMetadata").Item("predefinedValues").Children()
                If value.Item("id").ToString = Meta.Item("value").ToString Then
                    temp = value.Item("value").ToString
                End If
            Next
        ElseIf Meta.Item("playerMetadata").Item("valueType").ToString = "ANY" Then
            temp = Meta.Item("value").ToString
        Else
            temp = "Not Set"
        End If
        GetMetadatoValue = temp
    End Function

    Function GetPlayerPollingUnit(Playerid As String)
        Dim result = GetStuffs(Scairim.token, Scairim.apiToken, Scairim.LinkCM & "/api/rest/players/" & Playerid & "?fields=pollingUnit")
        If InStr(result, "ERROR") > 0 Then
            Logga("ERROR - GetPlayerPollingUnit: Ricerca Player sul CM fallita. Controlla di aver fatto il Login correttamente.")
            GetPlayerPollingUnit = ""
        Else
            If Parsa(result) = True Then
                GetPlayerPollingUnit = oJsonR.Item("pollingUnit").ToString
            Else
                Logga("ERROR - GetPlayerPollingUnit: Parsing JSON Fallito.")
                GetPlayerPollingUnit = ""
            End If
        End If
    End Function

    Function UpdatePlayerMetadato(PlayerID As String, PlayerName As String, Metadato As String, Value As String, Scheduled As Boolean, ScheduleDate As Date)
        Dim result = GetStuffs(Scairim.token, Scairim.apiToken, Scairim.LinkCM & "/api/rest/playerMetadata?limit=0&search=" & Metadato)
        Dim MetadatoId As String = ""
        Dim MetadatoType As String = ""
        Dim MetadatoValue As String = ""
        Dim ValuePicklist As Integer = 0
        Dim pickvalues As List(Of String) = New List(Of String)
        Dim JsonPicklist As JArray = New JArray
        If InStr(result, "ERROR") > 0 Then
            UpdatePlayerMetadato = "ERROR - UpdatePlayerMetadato: Ricerca Metadato sul CM fallita. Controlla di aver fatto il Login correttamente."
        Else
            If Parsa(result) = True Then
                If oJsonR.ContainsKey("list") Then
                    For Each child In oJsonR.Item("list").Children()
                        If child.Item("name").ToString = "Player." & Metadato Then
                            MetadatoId = child.Item("id").ToString
                            MetadatoType = child.Item("datatype").ToString
                            MetadatoValue = child.Item("valueType").ToString
                            If MetadatoValue = "PICKLIST" Then
                                JsonPicklist = child.Item("predefinedValues")
                                For Each pickval In child.Item("predefinedValues")
                                    If Value = pickval.Item("value").ToString Then
                                        ValuePicklist = pickval.Item("id").ToString
                                    End If
                                    pickvalues.Add(pickval.Item("value").ToString)
                                Next
                            End If
                        End If
                    Next
                    If MetadatoId = "" Or MetadatoType = "" Or MetadatoValue = "" Then
                        UpdatePlayerMetadato = "ERROR - UpdatePlayerMetadato: Metadato non trovato."
                    Else
                        Select Case True
                            Case MetadatoType = "BOOLEAN"
                                If Value.ToLower = "true" Then
                                    UpdatePlayerMetadato = UpdatePlayerMetadatoBoolean(PlayerID, PlayerName, MetadatoId, True, Scheduled)
                                Else
                                    UpdatePlayerMetadato = UpdatePlayerMetadatoBoolean(PlayerID, PlayerName, MetadatoId, False, Scheduled)
                                End If
                                'UpdatePlayerMetadato = "OK - UpdatePlayerMetadato: Metadato " & Metadato & " settato come " & Value & " sul player Id " & PlayerID
                            Case MetadatoValue = "ANY"
                                UpdatePlayerMetadato = UpdatePlayerMetadatoAny(PlayerID, PlayerName, MetadatoId, MetadatoType, MetadatoValue, Value, Scheduled)
                            Case MetadatoValue = "PICKLIST"
                                If pickvalues.Contains(Value) Then
                                    UpdatePlayerMetadato = UpdatePlayerMetadatoPicklist(PlayerID, PlayerName, MetadatoId, MetadatoType, MetadatoValue,
                                                                                        JsonPicklist, ValuePicklist, Scheduled)
                                    pickvalues.Clear()
                                Else
                                    Dim temp As String = ""
                                    For Each v In pickvalues
                                        temp = AddTextComma(temp, v)
                                    Next
                                    UpdatePlayerMetadato = "ERROR - UpdatePlayerMetadato: Valore non presente nella picklist! Valori consentiti: " & temp
                                    pickvalues.Clear()
                                End If
                            Case Else
                                UpdatePlayerMetadato = "ERROR - UpdatePlayerMetadato: Caso inesistente"
                        End Select
                        'UpdatePlayerMetadato = "OK - UpdatePlayerMetadato: Metadato " & Metadato & " settato come " & Value & " sul player Id " & PlayerID
                    End If
                Else
                    UpdatePlayerMetadato = "ERROR - UpdatePlayerMetadato: Metadato non trovato."
                End If
            Else
                UpdatePlayerMetadato = "ERROR - UpdatePlayerMetadato: Parsing JSON Fallito."
            End If
            If Scheduled = True And InStr(UpdatePlayerMetadato, "ERROR") = 0 Then
                oJsonR2 = JObject.Parse(UpdatePlayerMetadato)
                oJArray = New JArray(
                    New JObject(
                    New JProperty("uploader", Scairim.Username),
                    New JProperty("link", Scairim.LinkCM),
                    New JProperty("metadato", Metadato),
                    New JProperty("metadata_value", Value),
                    New JProperty("player_id", PlayerID),
                    New JProperty("player_name", PlayerName),
                    New JProperty("schedule", ScheduleDate),
                    New JProperty("update", UpdatePlayerMetadato)))
                UpdatePlayerMetadato = ScheduleUpdatePlayer(oJArray.ToString)
            End If
        End If
    End Function

    Function UpdatePlayerMetadatoBoolean(PlayerID As String, PlayerName As String, MetadatoId As String, Value As Boolean, ReturnJson As Boolean)
        oJsonW = New JObject(
            New JProperty("name", PlayerName),
            New JProperty("pollingUnit", GetPlayerPollingUnit(PlayerID)),
            New JProperty("metadataValue",
                          New JArray(
                          New JObject(
                          New JProperty("value", Value),
                            New JProperty("playerMetadata", New JObject(
                                New JProperty("id", MetadatoId),
                                New JProperty("datatype", "BOOLEAN"),
                                New JProperty("valueType", "ANY")))))))
        'Logga(Scairim.LinkCM & "/api/rest/players/" & PlayerID)
        'Logga(oJsonW.ToString)
        If ReturnJson = True Then
            UpdatePlayerMetadatoBoolean = oJsonW.ToString
        Else
            UpdatePlayerMetadatoBoolean = UpdateStuffs(Scairim.token, Scairim.apiToken, oJsonW.ToString, Scairim.LinkCM & "/api/rest/players/" & PlayerID)
        End If
        oJsonW.RemoveAll()
    End Function

    Function UpdatePlayerMetadatoAny(PlayerID As String, PlayerName As String, MetadatoId As String, MetadatoType As String, MetadatoValue As String,
                                     Value As String, ReturnJson As Boolean)
        If MetadatoType = "INTEGER" And Not IsNumeric(Value) Then
            Return "ERROR - UpdatePlayerMetadato: il metadato specificato deve essere un numero."
        End If
        oJsonW = New JObject(
            New JProperty("name", PlayerName),
            New JProperty("pollingUnit", GetPlayerPollingUnit(PlayerID)),
            New JProperty("metadataValue",
                          New JArray(
                          New JObject(
                          New JProperty("value", Value),
                            New JProperty("playerMetadata", New JObject(
                                New JProperty("id", MetadatoId),
                                New JProperty("datatype", MetadatoType),
                                New JProperty("valueType", MetadatoValue)))))))
        'Logga(Scairim.LinkCM & "/api/rest/players/" & PlayerID)
        'Logga(oJsonW.ToString)
        If ReturnJson = True Then
            UpdatePlayerMetadatoAny = oJsonW.ToString
        Else
            UpdatePlayerMetadatoAny = UpdateStuffs(Scairim.token, Scairim.apiToken, oJsonW.ToString, Scairim.LinkCM & "/api/rest/players/" & PlayerID)
        End If
        oJsonW.RemoveAll()
    End Function

    Function UpdatePlayerMetadatoPicklist(PlayerID As String, PlayerName As String, MetadatoId As String, MetadatoType As String,
                                          MetadatoValue As String, JsonPicklist As JArray, Value As Integer, ReturnJson As Boolean)
        oJsonW = New JObject(
            New JProperty("name", PlayerName),
            New JProperty("pollingUnit", GetPlayerPollingUnit(PlayerID)),
            New JProperty("metadataValue",
                          New JArray(
                          New JObject(
                          New JProperty("value", Value),
                            New JProperty("playerMetadata", New JObject(
                                New JProperty("id", MetadatoId),
                                New JProperty("datatype", MetadatoType),
                                New JProperty("valueType", MetadatoValue),
                                New JProperty("predefinedValues", JsonPicklist)))))))
        'Logga(Scairim.LinkCM & "/api/rest/players/" & PlayerID)
        'Logga(oJsonW.ToString)
        If ReturnJson = True Then
            UpdatePlayerMetadatoPicklist = oJsonW.ToString
        Else
            UpdatePlayerMetadatoPicklist = UpdateStuffs(Scairim.token, Scairim.apiToken, oJsonW.ToString, Scairim.LinkCM & "/api/rest/players/" & PlayerID)
        End If
        oJsonW.RemoveAll()
    End Function

    Function UpdatePlayerGroups(PlayerID As String, PlayerName As String, Groups As List(Of String), GroupIdMap As Dictionary(Of String, String))
        Dim check = GetStuffs(Scairim.token, Scairim.apiToken, Scairim.LinkCM & "/api/rest/players/" & PlayerID & "?fields=playergroups")
        If InStr(check, "(401)") > 0 Then
            Logga("Sessione scaduta, per favore rifai il Login.")
            UpdatePlayerGroups = "ERROR - Sessione scaduta"
            Exit Function
        End If
        oJsonR = JObject.Parse(check)
        Dim PlayerGroups As New JArray
        If oJsonR.ContainsKey("playergroups") Then
            For Each child In oJsonR.Item("playergroups").Children()
                PlayerGroups.Add(
                    New JObject(
                        New JProperty("id", child.Item("id")),
                        New JProperty("name", child.Item("name"))
                    )
                )
            Next
        End If
        For Each group In Groups
            PlayerGroups.Add(
                    New JObject(
                        New JProperty("id", GroupIdMap.Item(group)),
                        New JProperty("name", group)
                    )
                )
        Next
        oJsonW = New JObject(
            New JProperty("name", PlayerName),
            New JProperty("pollingUnit", GetPlayerPollingUnit(PlayerID)),
            New JProperty("playergroups", PlayerGroups)
            )
        'Logga(Scairim.LinkCM & "/api/rest/players/" & PlayerID)
        'Logga(oJsonW.ToString)
        'UpdatePlayerGroups = "Test"
        UpdatePlayerGroups = UpdateStuffs(Scairim.token, Scairim.apiToken, oJsonW.ToString, Scairim.LinkCM & "/api/rest/players/" & PlayerID)
        oJsonW.RemoveAll()
    End Function

    Function UpdatePlayerDescFromMetadata(PlayerID As String, PlayerName As String, Metadata As List(Of String), MetadataIdMap As Dictionary(Of String, String))
        Dim check = GetStuffs(Scairim.token, Scairim.apiToken, Scairim.LinkCM & "/api/rest/players/" & PlayerID & "?fields=description,metadataValue")
        Dim metaDesc As String = ""
        Dim playerDesc As String = ""
        Dim metadatoValue As String = ""
        If InStr(check, "(401)") > 0 Then
            Logga("Sessione scaduta, per favore rifai il Login.")
            UpdatePlayerDescFromMetadata = "ERROR - Sessione scaduta"
            Exit Function
        End If
        oJsonR = JObject.Parse(check)
        If oJsonR.ContainsKey("description") Then
            playerDesc = oJsonR.Item("description")
        End If
        If playerDesc.Contains("#") Then
            playerDesc = playerDesc.Split("#")(2).Trim
        End If
        metaDesc = "#"
        For Each metadato In Metadata
            metadatoValue = GetMetadatoValue(check, metadato, True)
            If metadatoValue <> "Not Set" Then
                If metaDesc <> "#" Then
                    metaDesc &= " |"
                End If
                metaDesc &= " " & metadato.Replace("Player.", "") & ": " & metadatoValue
            End If
        Next
        metaDesc &= " # "
        Dim finalDesc = metaDesc & playerDesc
        oJsonW = New JObject(
            New JProperty("name", PlayerName),
            New JProperty("description", RemoveSpecialChars(String.Concat(finalDesc.Take(255))))
            )
        'Logga(oJsonW.ToString)
        UpdatePlayerDescFromMetadata = UpdateStuffs(Scairim.token, Scairim.apiToken, oJsonW.ToString, Scairim.LinkCM & "/api/rest/players/" & PlayerID)
        oJsonW.RemoveAll()
    End Function

    Function UpdatePlayerName(PlayerID As String, PlayerName As String)
        oJsonW = New JObject(
            New JProperty("name", PlayerName)
            )
        'Logga(Scairim.LinkCM & "/api/rest/players/" & PlayerID)
        'Logga(oJsonW.ToString)
        'UpdatePlayerName = "Test"
        UpdatePlayerName = UpdateStuffs(Scairim.token, Scairim.apiToken, oJsonW.ToString, Scairim.LinkCM & "/api/rest/players/" & PlayerID)
        oJsonW.RemoveAll()
    End Function

    Function UpdatePlayerEnabled(PlayerID As String, PlayerName As String, Enabled As String)
        oJsonW = New JObject(
            New JProperty("name", PlayerName),
            New JProperty("enabled", Enabled)
            )
        'Logga(Scairim.LinkCM & "/api/rest/players/" & PlayerID)
        'Logga(oJsonW.ToString)
        'UpdatePlayerName = "Test"
        UpdatePlayerEnabled = UpdateStuffs(Scairim.token, Scairim.apiToken, oJsonW.ToString, Scairim.LinkCM & "/api/rest/players/" & PlayerID)
        oJsonW.RemoveAll()
    End Function

    Function UpdatePlayerNameTV(bearerToken As String, PlayerID As String, PlayerName As String)
        oJsonW = New JObject(
            New JProperty("alias", PlayerName)
            )
        'Logga(oJsonW.ToString)
        'UpdatePlayerName = "Test"
        UpdatePlayerNameTV = UpdateStuffsBearer(bearerToken, oJsonW.ToString, "https://webapi.teamviewer.com/api/v1/devices/" & PlayerID)
        oJsonW.RemoveAll()
    End Function

    Function CreateMetadata(MetadataName As String, valueType As String, dataType As String)
        oJsonW = New JObject(
            New JProperty("name", MetadataName),
            New JProperty("datatype", dataType),
            New JProperty("valueType", valueType))
        'Logga(oJsonW.ToString)
        CreateMetadata = CreateStuffs(Scairim.token, Scairim.apiToken, oJsonW.ToString, Scairim.LinkCM & "/api/rest/playerMetadata")
        oJsonW.RemoveAll()
    End Function

    Function AddPicklistValue(MetadataId As String, predefinedValues As List(Of String))
        Dim pred As JArray
        oJsonW = New JObject(
            New JProperty("id", MetadataId))
        oJsonR = JObject.Parse(GetStuffs(Scairim.token, Scairim.apiToken, Scairim.LinkCM & "/api/rest/playerMetadata/" & MetadataId))
        If oJsonR.Item("predefinedValues") Is Nothing Then
            pred = New JArray()
        Else
            pred = oJsonR.Item("predefinedValues")
        End If
        For Each v In pred.Children
            If predefinedValues.Contains(v.Item("value").ToString) Then
                predefinedValues.Remove(v.Item("value").ToString)
            End If
        Next
        For Each value In predefinedValues
            pred.Add(New JObject(
                New JProperty("id", "0"),
                New JProperty("value", value),
                New JProperty("sortOrder", "1")))
        Next
        oJsonW.Add(New JProperty("predefinedValues", pred))
        'Logga(oJsonW.ToString)
        AddPicklistValue = UpdateStuffs(Scairim.token, Scairim.apiToken, oJsonW.ToString, Scairim.LinkCM & "/api/rest/playerMetadata/multiple/" & MetadataId)
        oJsonW.RemoveAll()
    End Function

#End Region

    '##################################
    'Specific Scala-Json Function Zone
    '##################################

#Region "SpecificScalaJsonFunctionZone"

    Function CreaSanPaoloPlayer(Name As String, Banca As String, CodFiliale As String, MarcaMonitor As String, Seriale As String, IsVetrinaEsterna As Boolean)
        'Logga("-------FOR TEST--------")
        'Logga("PlayerName = " & Name)
        'Logga("Banca = " & Banca)
        'Logga("CodiceFiliale = " & CodFiliale)
        'Logga("MarcaMonitor = " & MarcaMonitor)
        'Logga("Seriale = " & Seriale)
        'If IsVetrinaEsterna Then
        '    Logga("Vetrina Si")
        'Else
        '    Logga("Vetrina No")
        'End If
        oJsonW = New JObject(
            New JProperty("name", Name),
            New JProperty("type", "SCALA"),
            New JProperty(New JProperty("id", 1))
            )
        'Parsa(CreatePlayer(Scairim.token, Scairim.apiToken, oJsonW.ToString, Scairim.LinkCM))
        oJsonW.RemoveAll()
        'Dim PlayerId As String = GetPlayerId(Name)
        Dim PlayerId As String = GetPlayerId("0100267-OS01")
        'Logga("Player " & Name & " creato con ID: " & PlayerId)

        'Logga(UpdatePlayerMetadato(PlayerId, "Monitor_Telecomando", "TRUE"))

        CreaSanPaoloPlayer = "OK"
    End Function

#End Region

End Module
