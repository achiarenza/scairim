﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmExtractionByMetadata
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dtgridExport = New System.Windows.Forms.DataGridView()
        Me.txtMetadato1 = New System.Windows.Forms.TextBox()
        Me.txtValore1 = New System.Windows.Forms.TextBox()
        Me.txtValore2 = New System.Windows.Forms.TextBox()
        Me.txtMetadato2 = New System.Windows.Forms.TextBox()
        Me.txtValore3 = New System.Windows.Forms.TextBox()
        Me.txtMetadato3 = New System.Windows.Forms.TextBox()
        Me.txtValore4 = New System.Windows.Forms.TextBox()
        Me.txtMetadato4 = New System.Windows.Forms.TextBox()
        Me.txtValore7 = New System.Windows.Forms.TextBox()
        Me.txtMetadato7 = New System.Windows.Forms.TextBox()
        Me.txtValore6 = New System.Windows.Forms.TextBox()
        Me.txtMetadato6 = New System.Windows.Forms.TextBox()
        Me.txtValore5 = New System.Windows.Forms.TextBox()
        Me.txtMetadato5 = New System.Windows.Forms.TextBox()
        Me.txtValore8 = New System.Windows.Forms.TextBox()
        Me.txtMetadato8 = New System.Windows.Forms.TextBox()
        Me.txtValore9 = New System.Windows.Forms.TextBox()
        Me.txtMetadato9 = New System.Windows.Forms.TextBox()
        Me.txtValore10 = New System.Windows.Forms.TextBox()
        Me.txtMetadato10 = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.btnEstraiExcel = New System.Windows.Forms.Button()
        Me.btnGo = New System.Windows.Forms.Button()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.txtName = New System.Windows.Forms.TextBox()
        Me.chkUpLow = New System.Windows.Forms.CheckBox()
        CType(Me.dtgridExport, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dtgridExport
        '
        Me.dtgridExport.AllowUserToAddRows = False
        Me.dtgridExport.AllowUserToDeleteRows = False
        Me.dtgridExport.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dtgridExport.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dtgridExport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgridExport.Location = New System.Drawing.Point(9, 231)
        Me.dtgridExport.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.dtgridExport.Name = "dtgridExport"
        Me.dtgridExport.RowTemplate.Height = 24
        Me.dtgridExport.Size = New System.Drawing.Size(776, 319)
        Me.dtgridExport.TabIndex = 0
        '
        'txtMetadato1
        '
        Me.txtMetadato1.Location = New System.Drawing.Point(9, 73)
        Me.txtMetadato1.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txtMetadato1.Name = "txtMetadato1"
        Me.txtMetadato1.Size = New System.Drawing.Size(152, 20)
        Me.txtMetadato1.TabIndex = 1
        '
        'txtValore1
        '
        Me.txtValore1.Location = New System.Drawing.Point(9, 115)
        Me.txtValore1.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txtValore1.Name = "txtValore1"
        Me.txtValore1.Size = New System.Drawing.Size(152, 20)
        Me.txtValore1.TabIndex = 2
        '
        'txtValore2
        '
        Me.txtValore2.Location = New System.Drawing.Point(165, 115)
        Me.txtValore2.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txtValore2.Name = "txtValore2"
        Me.txtValore2.Size = New System.Drawing.Size(152, 20)
        Me.txtValore2.TabIndex = 4
        '
        'txtMetadato2
        '
        Me.txtMetadato2.Location = New System.Drawing.Point(165, 73)
        Me.txtMetadato2.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txtMetadato2.Name = "txtMetadato2"
        Me.txtMetadato2.Size = New System.Drawing.Size(152, 20)
        Me.txtMetadato2.TabIndex = 3
        '
        'txtValore3
        '
        Me.txtValore3.Location = New System.Drawing.Point(321, 115)
        Me.txtValore3.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txtValore3.Name = "txtValore3"
        Me.txtValore3.Size = New System.Drawing.Size(152, 20)
        Me.txtValore3.TabIndex = 6
        '
        'txtMetadato3
        '
        Me.txtMetadato3.Location = New System.Drawing.Point(321, 73)
        Me.txtMetadato3.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txtMetadato3.Name = "txtMetadato3"
        Me.txtMetadato3.Size = New System.Drawing.Size(152, 20)
        Me.txtMetadato3.TabIndex = 5
        '
        'txtValore4
        '
        Me.txtValore4.Location = New System.Drawing.Point(477, 115)
        Me.txtValore4.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txtValore4.Name = "txtValore4"
        Me.txtValore4.Size = New System.Drawing.Size(152, 20)
        Me.txtValore4.TabIndex = 8
        '
        'txtMetadato4
        '
        Me.txtMetadato4.Location = New System.Drawing.Point(477, 73)
        Me.txtMetadato4.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txtMetadato4.Name = "txtMetadato4"
        Me.txtMetadato4.Size = New System.Drawing.Size(152, 20)
        Me.txtMetadato4.TabIndex = 7
        '
        'txtValore7
        '
        Me.txtValore7.Location = New System.Drawing.Point(165, 196)
        Me.txtValore7.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txtValore7.Name = "txtValore7"
        Me.txtValore7.Size = New System.Drawing.Size(152, 20)
        Me.txtValore7.TabIndex = 10
        '
        'txtMetadato7
        '
        Me.txtMetadato7.Location = New System.Drawing.Point(165, 159)
        Me.txtMetadato7.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txtMetadato7.Name = "txtMetadato7"
        Me.txtMetadato7.Size = New System.Drawing.Size(152, 20)
        Me.txtMetadato7.TabIndex = 9
        '
        'txtValore6
        '
        Me.txtValore6.Location = New System.Drawing.Point(9, 196)
        Me.txtValore6.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txtValore6.Name = "txtValore6"
        Me.txtValore6.Size = New System.Drawing.Size(152, 20)
        Me.txtValore6.TabIndex = 12
        '
        'txtMetadato6
        '
        Me.txtMetadato6.Location = New System.Drawing.Point(9, 159)
        Me.txtMetadato6.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txtMetadato6.Name = "txtMetadato6"
        Me.txtMetadato6.Size = New System.Drawing.Size(152, 20)
        Me.txtMetadato6.TabIndex = 11
        '
        'txtValore5
        '
        Me.txtValore5.Location = New System.Drawing.Point(633, 115)
        Me.txtValore5.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txtValore5.Name = "txtValore5"
        Me.txtValore5.Size = New System.Drawing.Size(152, 20)
        Me.txtValore5.TabIndex = 14
        '
        'txtMetadato5
        '
        Me.txtMetadato5.Location = New System.Drawing.Point(633, 73)
        Me.txtMetadato5.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txtMetadato5.Name = "txtMetadato5"
        Me.txtMetadato5.Size = New System.Drawing.Size(152, 20)
        Me.txtMetadato5.TabIndex = 13
        '
        'txtValore8
        '
        Me.txtValore8.Location = New System.Drawing.Point(321, 196)
        Me.txtValore8.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txtValore8.Name = "txtValore8"
        Me.txtValore8.Size = New System.Drawing.Size(152, 20)
        Me.txtValore8.TabIndex = 16
        '
        'txtMetadato8
        '
        Me.txtMetadato8.Location = New System.Drawing.Point(321, 159)
        Me.txtMetadato8.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txtMetadato8.Name = "txtMetadato8"
        Me.txtMetadato8.Size = New System.Drawing.Size(152, 20)
        Me.txtMetadato8.TabIndex = 15
        '
        'txtValore9
        '
        Me.txtValore9.Location = New System.Drawing.Point(477, 196)
        Me.txtValore9.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txtValore9.Name = "txtValore9"
        Me.txtValore9.Size = New System.Drawing.Size(152, 20)
        Me.txtValore9.TabIndex = 18
        '
        'txtMetadato9
        '
        Me.txtMetadato9.Location = New System.Drawing.Point(477, 159)
        Me.txtMetadato9.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txtMetadato9.Name = "txtMetadato9"
        Me.txtMetadato9.Size = New System.Drawing.Size(152, 20)
        Me.txtMetadato9.TabIndex = 17
        '
        'txtValore10
        '
        Me.txtValore10.Location = New System.Drawing.Point(633, 196)
        Me.txtValore10.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txtValore10.Name = "txtValore10"
        Me.txtValore10.Size = New System.Drawing.Size(152, 20)
        Me.txtValore10.TabIndex = 20
        '
        'txtMetadato10
        '
        Me.txtMetadato10.Location = New System.Drawing.Point(633, 159)
        Me.txtMetadato10.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txtMetadato10.Name = "txtMetadato10"
        Me.txtMetadato10.Size = New System.Drawing.Size(152, 20)
        Me.txtMetadato10.TabIndex = 19
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 57)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(61, 13)
        Me.Label1.TabIndex = 21
        Me.Label1.Text = "Metadato 1"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(9, 99)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(46, 13)
        Me.Label2.TabIndex = 22
        Me.Label2.Text = "Valore 1"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(163, 99)
        Me.Label3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(46, 13)
        Me.Label3.TabIndex = 24
        Me.Label3.Text = "Valore 2"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(163, 57)
        Me.Label4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(61, 13)
        Me.Label4.TabIndex = 23
        Me.Label4.Text = "Metadato 2"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(319, 99)
        Me.Label5.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(46, 13)
        Me.Label5.TabIndex = 26
        Me.Label5.Text = "Valore 3"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(319, 57)
        Me.Label6.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(61, 13)
        Me.Label6.TabIndex = 25
        Me.Label6.Text = "Metadato 3"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(475, 99)
        Me.Label7.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(46, 13)
        Me.Label7.TabIndex = 28
        Me.Label7.Text = "Valore 4"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(475, 57)
        Me.Label8.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(61, 13)
        Me.Label8.TabIndex = 27
        Me.Label8.Text = "Metadato 4"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(631, 99)
        Me.Label9.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(46, 13)
        Me.Label9.TabIndex = 30
        Me.Label9.Text = "Valore 5"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(631, 57)
        Me.Label10.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(61, 13)
        Me.Label10.TabIndex = 29
        Me.Label10.Text = "Metadato 5"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(7, 182)
        Me.Label11.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(46, 13)
        Me.Label11.TabIndex = 32
        Me.Label11.Text = "Valore 6"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(7, 143)
        Me.Label12.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(61, 13)
        Me.Label12.TabIndex = 31
        Me.Label12.Text = "Metadato 6"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(163, 182)
        Me.Label13.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(46, 13)
        Me.Label13.TabIndex = 34
        Me.Label13.Text = "Valore 7"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(163, 143)
        Me.Label14.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(61, 13)
        Me.Label14.TabIndex = 33
        Me.Label14.Text = "Metadato 7"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(319, 182)
        Me.Label15.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(46, 13)
        Me.Label15.TabIndex = 36
        Me.Label15.Text = "Valore 8"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(319, 143)
        Me.Label16.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(61, 13)
        Me.Label16.TabIndex = 35
        Me.Label16.Text = "Metadato 8"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(475, 182)
        Me.Label17.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(46, 13)
        Me.Label17.TabIndex = 38
        Me.Label17.Text = "Valore 9"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(475, 143)
        Me.Label18.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(61, 13)
        Me.Label18.TabIndex = 37
        Me.Label18.Text = "Metadato 9"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(631, 182)
        Me.Label19.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(52, 13)
        Me.Label19.TabIndex = 40
        Me.Label19.Text = "Valore 10"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(631, 143)
        Me.Label20.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(67, 13)
        Me.Label20.TabIndex = 39
        Me.Label20.Text = "Metadato 10"
        '
        'btnEstraiExcel
        '
        Me.btnEstraiExcel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnEstraiExcel.Location = New System.Drawing.Point(9, 561)
        Me.btnEstraiExcel.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.btnEstraiExcel.Name = "btnEstraiExcel"
        Me.btnEstraiExcel.Size = New System.Drawing.Size(122, 32)
        Me.btnEstraiExcel.TabIndex = 41
        Me.btnEstraiExcel.Text = "Estrai in Excel"
        Me.btnEstraiExcel.UseVisualStyleBackColor = True
        '
        'btnGo
        '
        Me.btnGo.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnGo.Location = New System.Drawing.Point(321, 561)
        Me.btnGo.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.btnGo.Name = "btnGo"
        Me.btnGo.Size = New System.Drawing.Size(134, 32)
        Me.btnGo.TabIndex = 42
        Me.btnGo.Text = "Trovami quei Player!!!"
        Me.btnGo.UseVisualStyleBackColor = True
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(9, 11)
        Me.Label21.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(60, 13)
        Me.Label21.TabIndex = 44
        Me.Label21.Text = "Filtro Nome"
        '
        'txtName
        '
        Me.txtName.Location = New System.Drawing.Point(9, 28)
        Me.txtName.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(152, 20)
        Me.txtName.TabIndex = 43
        '
        'chkUpLow
        '
        Me.chkUpLow.AutoSize = True
        Me.chkUpLow.Location = New System.Drawing.Point(166, 28)
        Me.chkUpLow.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.chkUpLow.Name = "chkUpLow"
        Me.chkUpLow.Size = New System.Drawing.Size(96, 17)
        Me.chkUpLow.TabIndex = 45
        Me.chkUpLow.Text = "Case Sensitive"
        Me.chkUpLow.UseVisualStyleBackColor = True
        '
        'frmExtractionByMetadata
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(794, 600)
        Me.Controls.Add(Me.chkUpLow)
        Me.Controls.Add(Me.Label21)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.btnGo)
        Me.Controls.Add(Me.btnEstraiExcel)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtValore10)
        Me.Controls.Add(Me.txtMetadato10)
        Me.Controls.Add(Me.txtValore9)
        Me.Controls.Add(Me.txtMetadato9)
        Me.Controls.Add(Me.txtValore8)
        Me.Controls.Add(Me.txtMetadato8)
        Me.Controls.Add(Me.txtValore5)
        Me.Controls.Add(Me.txtMetadato5)
        Me.Controls.Add(Me.txtValore6)
        Me.Controls.Add(Me.txtMetadato6)
        Me.Controls.Add(Me.txtValore7)
        Me.Controls.Add(Me.txtMetadato7)
        Me.Controls.Add(Me.txtValore4)
        Me.Controls.Add(Me.txtMetadato4)
        Me.Controls.Add(Me.txtValore3)
        Me.Controls.Add(Me.txtMetadato3)
        Me.Controls.Add(Me.txtValore2)
        Me.Controls.Add(Me.txtMetadato2)
        Me.Controls.Add(Me.txtValore1)
        Me.Controls.Add(Me.txtMetadato1)
        Me.Controls.Add(Me.dtgridExport)
        Me.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.MinimumSize = New System.Drawing.Size(810, 638)
        Me.Name = "frmExtractionByMetadata"
        Me.Text = "Extraction By Metadati"
        CType(Me.dtgridExport, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents dtgridExport As DataGridView
    Friend WithEvents txtMetadato1 As TextBox
    Friend WithEvents txtValore1 As TextBox
    Friend WithEvents txtValore2 As TextBox
    Friend WithEvents txtMetadato2 As TextBox
    Friend WithEvents txtValore3 As TextBox
    Friend WithEvents txtMetadato3 As TextBox
    Friend WithEvents txtValore4 As TextBox
    Friend WithEvents txtMetadato4 As TextBox
    Friend WithEvents txtValore7 As TextBox
    Friend WithEvents txtMetadato7 As TextBox
    Friend WithEvents txtValore6 As TextBox
    Friend WithEvents txtMetadato6 As TextBox
    Friend WithEvents txtValore5 As TextBox
    Friend WithEvents txtMetadato5 As TextBox
    Friend WithEvents txtValore8 As TextBox
    Friend WithEvents txtMetadato8 As TextBox
    Friend WithEvents txtValore9 As TextBox
    Friend WithEvents txtMetadato9 As TextBox
    Friend WithEvents txtValore10 As TextBox
    Friend WithEvents txtMetadato10 As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents btnEstraiExcel As Button
    Friend WithEvents btnGo As Button
    Friend WithEvents Label21 As Label
    Friend WithEvents txtName As TextBox
    Friend WithEvents chkUpLow As CheckBox
End Class
