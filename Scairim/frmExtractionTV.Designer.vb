﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmExtractionTV
    Inherits System.Windows.Forms.Form

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla mediante l'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dtgridExport = New System.Windows.Forms.DataGridView()
        Me.btnEstrai = New System.Windows.Forms.Button()
        Me.btnExportExcel = New System.Windows.Forms.Button()
        Me.chkUpLow = New System.Windows.Forms.CheckBox()
        Me.txtPlayerName = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.chkStatus = New System.Windows.Forms.CheckBox()
        Me.chkGroups = New System.Windows.Forms.CheckBox()
        Me.chkRemoteID = New System.Windows.Forms.CheckBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.btnRmv = New System.Windows.Forms.Button()
        Me.lbSelectedGroups = New System.Windows.Forms.ListBox()
        Me.lbAvailableGroups = New System.Windows.Forms.ListBox()
        CType(Me.dtgridExport, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'dtgridExport
        '
        Me.dtgridExport.AllowUserToAddRows = False
        Me.dtgridExport.AllowUserToDeleteRows = False
        Me.dtgridExport.AllowUserToOrderColumns = True
        Me.dtgridExport.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dtgridExport.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dtgridExport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgridExport.Location = New System.Drawing.Point(9, 215)
        Me.dtgridExport.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.dtgridExport.Name = "dtgridExport"
        Me.dtgridExport.ReadOnly = True
        Me.dtgridExport.RowHeadersWidth = 51
        Me.dtgridExport.RowTemplate.Height = 24
        Me.dtgridExport.Size = New System.Drawing.Size(734, 306)
        Me.dtgridExport.TabIndex = 8
        '
        'btnEstrai
        '
        Me.btnEstrai.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnEstrai.Location = New System.Drawing.Point(335, 532)
        Me.btnEstrai.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.btnEstrai.Name = "btnEstrai"
        Me.btnEstrai.Size = New System.Drawing.Size(81, 28)
        Me.btnEstrai.TabIndex = 9
        Me.btnEstrai.Text = "Do it!"
        Me.btnEstrai.UseVisualStyleBackColor = True
        '
        'btnExportExcel
        '
        Me.btnExportExcel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnExportExcel.Location = New System.Drawing.Point(9, 532)
        Me.btnExportExcel.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.btnExportExcel.Name = "btnExportExcel"
        Me.btnExportExcel.Size = New System.Drawing.Size(106, 28)
        Me.btnExportExcel.TabIndex = 20
        Me.btnExportExcel.Text = "Export in Excel"
        Me.btnExportExcel.UseVisualStyleBackColor = True
        Me.btnExportExcel.Visible = False
        '
        'chkUpLow
        '
        Me.chkUpLow.AutoSize = True
        Me.chkUpLow.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkUpLow.Location = New System.Drawing.Point(128, 28)
        Me.chkUpLow.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.chkUpLow.Name = "chkUpLow"
        Me.chkUpLow.Size = New System.Drawing.Size(96, 17)
        Me.chkUpLow.TabIndex = 27
        Me.chkUpLow.Text = "Case Sensitive"
        Me.chkUpLow.UseVisualStyleBackColor = True
        '
        'txtPlayerName
        '
        Me.txtPlayerName.Location = New System.Drawing.Point(9, 27)
        Me.txtPlayerName.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txtPlayerName.Name = "txtPlayerName"
        Me.txtPlayerName.Size = New System.Drawing.Size(115, 20)
        Me.txtPlayerName.TabIndex = 26
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(9, 11)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(114, 13)
        Me.Label2.TabIndex = 25
        Me.Label2.Text = "Player Name Contains:"
        '
        'chkStatus
        '
        Me.chkStatus.AutoSize = True
        Me.chkStatus.Location = New System.Drawing.Point(9, 57)
        Me.chkStatus.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.chkStatus.Name = "chkStatus"
        Me.chkStatus.Size = New System.Drawing.Size(56, 17)
        Me.chkStatus.TabIndex = 28
        Me.chkStatus.Text = "Status"
        Me.chkStatus.UseVisualStyleBackColor = True
        '
        'chkGroups
        '
        Me.chkGroups.AutoSize = True
        Me.chkGroups.Location = New System.Drawing.Point(9, 77)
        Me.chkGroups.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.chkGroups.Name = "chkGroups"
        Me.chkGroups.Size = New System.Drawing.Size(55, 17)
        Me.chkGroups.TabIndex = 29
        Me.chkGroups.Text = "Group"
        Me.chkGroups.UseVisualStyleBackColor = True
        '
        'chkRemoteID
        '
        Me.chkRemoteID.AutoSize = True
        Me.chkRemoteID.Location = New System.Drawing.Point(9, 98)
        Me.chkRemoteID.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.chkRemoteID.Name = "chkRemoteID"
        Me.chkRemoteID.Size = New System.Drawing.Size(133, 17)
        Me.chkRemoteID.TabIndex = 32
        Me.chkRemoteID.Text = "Remote connection ID"
        Me.chkRemoteID.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.btnAdd)
        Me.GroupBox1.Controls.Add(Me.btnRmv)
        Me.GroupBox1.Controls.Add(Me.lbSelectedGroups)
        Me.GroupBox1.Controls.Add(Me.lbAvailableGroups)
        Me.GroupBox1.Location = New System.Drawing.Point(332, 11)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(411, 200)
        Me.GroupBox1.TabIndex = 33
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Group"
        '
        'btnAdd
        '
        Me.btnAdd.Location = New System.Drawing.Point(194, 75)
        Me.btnAdd.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(27, 27)
        Me.btnAdd.TabIndex = 12
        Me.btnAdd.Text = ">>"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'btnRmv
        '
        Me.btnRmv.Location = New System.Drawing.Point(194, 124)
        Me.btnRmv.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.btnRmv.Name = "btnRmv"
        Me.btnRmv.Size = New System.Drawing.Size(27, 27)
        Me.btnRmv.TabIndex = 13
        Me.btnRmv.Text = "<<"
        Me.btnRmv.UseVisualStyleBackColor = True
        '
        'lbSelectedGroups
        '
        Me.lbSelectedGroups.FormattingEnabled = True
        Me.lbSelectedGroups.Location = New System.Drawing.Point(241, 32)
        Me.lbSelectedGroups.Name = "lbSelectedGroups"
        Me.lbSelectedGroups.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
        Me.lbSelectedGroups.Size = New System.Drawing.Size(164, 160)
        Me.lbSelectedGroups.Sorted = True
        Me.lbSelectedGroups.TabIndex = 1
        '
        'lbAvailableGroups
        '
        Me.lbAvailableGroups.FormattingEnabled = True
        Me.lbAvailableGroups.Location = New System.Drawing.Point(6, 32)
        Me.lbAvailableGroups.Name = "lbAvailableGroups"
        Me.lbAvailableGroups.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
        Me.lbAvailableGroups.Size = New System.Drawing.Size(168, 160)
        Me.lbAvailableGroups.Sorted = True
        Me.lbAvailableGroups.TabIndex = 0
        '
        'frmExtractionTV
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(752, 569)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.chkRemoteID)
        Me.Controls.Add(Me.chkGroups)
        Me.Controls.Add(Me.chkStatus)
        Me.Controls.Add(Me.chkUpLow)
        Me.Controls.Add(Me.txtPlayerName)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.btnExportExcel)
        Me.Controls.Add(Me.btnEstrai)
        Me.Controls.Add(Me.dtgridExport)
        Me.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.Name = "frmExtractionTV"
        Me.Text = "Extraction Teamviewer"
        CType(Me.dtgridExport, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents dtgridExport As DataGridView
    Friend WithEvents btnEstrai As Button
    Friend WithEvents btnExportExcel As Button
    Friend WithEvents chkUpLow As CheckBox
    Friend WithEvents txtPlayerName As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents chkStatus As CheckBox
    Friend WithEvents chkGroups As CheckBox
    Friend WithEvents chkRemoteID As CheckBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents btnAdd As Button
    Friend WithEvents btnRmv As Button
    Friend WithEvents lbSelectedGroups As ListBox
    Friend WithEvents lbAvailableGroups As ListBox
End Class
