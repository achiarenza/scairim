﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmUpdatePlayers
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmUpdatePlayers))
        Me.txtPlayers = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtMetadatoBoolean = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.rdOFF = New System.Windows.Forms.RadioButton()
        Me.rdOn = New System.Windows.Forms.RadioButton()
        Me.btnGo = New System.Windows.Forms.Button()
        Me.lbAvailableGroups = New System.Windows.Forms.ListBox()
        Me.lbGroupsAdd = New System.Windows.Forms.ListBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.btnRmv = New System.Windows.Forms.Button()
        Me.txtReplace = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtSuffix = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtPrefix = New System.Windows.Forms.TextBox()
        Me.txtWith = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtMetadatoAnyValue = New System.Windows.Forms.TextBox()
        Me.txtMetadatoAny = New System.Windows.Forms.TextBox()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.GroupBox8 = New System.Windows.Forms.GroupBox()
        Me.rdbDisable = New System.Windows.Forms.RadioButton()
        Me.rdbEnable = New System.Windows.Forms.RadioButton()
        Me.chkModifyEnabled = New System.Windows.Forms.CheckBox()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.lbAvailableMetadata = New System.Windows.Forms.ListBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.btnAdd2 = New System.Windows.Forms.Button()
        Me.btnRmv2 = New System.Windows.Forms.Button()
        Me.lbSelectedMetadata = New System.Windows.Forms.ListBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.btnShowDone = New System.Windows.Forms.Button()
        Me.dtgridExportScheduleDone = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnDeleteSchedule = New System.Windows.Forms.Button()
        Me.dtgridExportSchedule = New System.Windows.Forms.DataGridView()
        Me.clPlayer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clMetadato = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clValue = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clDateTime = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clUser = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.btnBrowseExcel = New System.Windows.Forms.Button()
        Me.txtMetadataExcel = New System.Windows.Forms.TextBox()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.chkSchedule = New System.Windows.Forms.CheckBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.dtSchedule = New System.Windows.Forms.DateTimePicker()
        Me.btnMeta = New System.Windows.Forms.Button()
        Me.ToolTipUpdate = New System.Windows.Forms.ToolTip(Me.components)
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.GroupBox8.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        CType(Me.dtgridExportScheduleDone, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtgridExportSchedule, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtPlayers
        '
        Me.txtPlayers.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtPlayers.Location = New System.Drawing.Point(9, 24)
        Me.txtPlayers.Margin = New System.Windows.Forms.Padding(2)
        Me.txtPlayers.Multiline = True
        Me.txtPlayers.Name = "txtPlayers"
        Me.txtPlayers.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtPlayers.Size = New System.Drawing.Size(388, 640)
        Me.txtPlayers.TabIndex = 0
        Me.ToolTipUpdate.SetToolTip(Me.txtPlayers, "Inserisci qui i player da aggiornare, uno per riga.")
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 9)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(107, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Player List (1 per line)"
        '
        'txtMetadatoBoolean
        '
        Me.txtMetadatoBoolean.Location = New System.Drawing.Point(4, 17)
        Me.txtMetadatoBoolean.Margin = New System.Windows.Forms.Padding(2)
        Me.txtMetadatoBoolean.Name = "txtMetadatoBoolean"
        Me.txtMetadatoBoolean.Size = New System.Drawing.Size(200, 20)
        Me.txtMetadatoBoolean.TabIndex = 3
        Me.ToolTipUpdate.SetToolTip(Me.txtMetadatoBoolean, "Nome di un Metadato di tipo On/Off (senza Player.)")
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.rdOFF)
        Me.GroupBox1.Controls.Add(Me.rdOn)
        Me.GroupBox1.Controls.Add(Me.txtMetadatoBoolean)
        Me.GroupBox1.Location = New System.Drawing.Point(5, 16)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(2)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(2)
        Me.GroupBox1.Size = New System.Drawing.Size(208, 88)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Metadata Boolean"
        '
        'rdOFF
        '
        Me.rdOFF.AutoSize = True
        Me.rdOFF.Location = New System.Drawing.Point(66, 40)
        Me.rdOFF.Margin = New System.Windows.Forms.Padding(2)
        Me.rdOFF.Name = "rdOFF"
        Me.rdOFF.Size = New System.Drawing.Size(45, 17)
        Me.rdOFF.TabIndex = 5
        Me.rdOFF.TabStop = True
        Me.rdOFF.Text = "OFF"
        Me.rdOFF.UseVisualStyleBackColor = True
        '
        'rdOn
        '
        Me.rdOn.AutoSize = True
        Me.rdOn.Location = New System.Drawing.Point(11, 40)
        Me.rdOn.Margin = New System.Windows.Forms.Padding(2)
        Me.rdOn.Name = "rdOn"
        Me.rdOn.Size = New System.Drawing.Size(41, 17)
        Me.rdOn.TabIndex = 4
        Me.rdOn.TabStop = True
        Me.rdOn.Text = "ON"
        Me.rdOn.UseVisualStyleBackColor = True
        '
        'btnGo
        '
        Me.btnGo.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnGo.Location = New System.Drawing.Point(184, 584)
        Me.btnGo.Margin = New System.Windows.Forms.Padding(2)
        Me.btnGo.Name = "btnGo"
        Me.btnGo.Size = New System.Drawing.Size(87, 36)
        Me.btnGo.TabIndex = 5
        Me.btnGo.Text = "Lavora!"
        Me.ToolTipUpdate.SetToolTip(Me.btnGo, "Elabora ciò che è stato richiesto sopra")
        Me.btnGo.UseVisualStyleBackColor = True
        '
        'lbAvailableGroups
        '
        Me.lbAvailableGroups.FormattingEnabled = True
        Me.lbAvailableGroups.Location = New System.Drawing.Point(21, 38)
        Me.lbAvailableGroups.Margin = New System.Windows.Forms.Padding(2)
        Me.lbAvailableGroups.Name = "lbAvailableGroups"
        Me.lbAvailableGroups.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
        Me.lbAvailableGroups.Size = New System.Drawing.Size(150, 108)
        Me.lbAvailableGroups.TabIndex = 6
        '
        'lbGroupsAdd
        '
        Me.lbGroupsAdd.FormattingEnabled = True
        Me.lbGroupsAdd.Location = New System.Drawing.Point(221, 38)
        Me.lbGroupsAdd.Margin = New System.Windows.Forms.Padding(2)
        Me.lbGroupsAdd.Name = "lbGroupsAdd"
        Me.lbGroupsAdd.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
        Me.lbGroupsAdd.Size = New System.Drawing.Size(156, 108)
        Me.lbGroupsAdd.TabIndex = 7
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(19, 22)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(106, 13)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "Available CM Groups"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(219, 22)
        Me.Label3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(75, 13)
        Me.Label3.TabIndex = 9
        Me.Label3.Text = "Groups to Add"
        '
        'btnAdd
        '
        Me.btnAdd.Location = New System.Drawing.Point(182, 53)
        Me.btnAdd.Margin = New System.Windows.Forms.Padding(2)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(27, 27)
        Me.btnAdd.TabIndex = 10
        Me.btnAdd.Text = ">>"
        Me.ToolTipUpdate.SetToolTip(Me.btnAdd, "Aggiungi gruppi ai player. Ricorda che devi cliccare ""Lavora!""")
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'btnRmv
        '
        Me.btnRmv.Location = New System.Drawing.Point(182, 102)
        Me.btnRmv.Margin = New System.Windows.Forms.Padding(2)
        Me.btnRmv.Name = "btnRmv"
        Me.btnRmv.Size = New System.Drawing.Size(27, 27)
        Me.btnRmv.TabIndex = 11
        Me.btnRmv.Text = "<<"
        Me.ToolTipUpdate.SetToolTip(Me.btnRmv, "Rimuovi gruppi da aggiungere. Questo non rimuoverà i gruppi dai Player")
        Me.btnRmv.UseVisualStyleBackColor = True
        '
        'txtReplace
        '
        Me.txtReplace.Location = New System.Drawing.Point(21, 42)
        Me.txtReplace.Margin = New System.Windows.Forms.Padding(2)
        Me.txtReplace.Name = "txtReplace"
        Me.txtReplace.Size = New System.Drawing.Size(150, 20)
        Me.txtReplace.TabIndex = 12
        Me.ToolTipUpdate.SetToolTip(Me.txtReplace, "Cosa vuoi sostituire dal nome dei player")
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.txtSuffix)
        Me.GroupBox2.Controls.Add(Me.Label10)
        Me.GroupBox2.Controls.Add(Me.txtPrefix)
        Me.GroupBox2.Controls.Add(Me.txtWith)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.txtReplace)
        Me.GroupBox2.Location = New System.Drawing.Point(31, 74)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(2)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(2)
        Me.GroupBox2.Size = New System.Drawing.Size(392, 123)
        Me.GroupBox2.TabIndex = 13
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Change Name"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(219, 70)
        Me.Label11.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(36, 13)
        Me.Label11.TabIndex = 19
        Me.Label11.Text = "Suffix:"
        '
        'txtSuffix
        '
        Me.txtSuffix.Location = New System.Drawing.Point(222, 86)
        Me.txtSuffix.Margin = New System.Windows.Forms.Padding(2)
        Me.txtSuffix.Name = "txtSuffix"
        Me.txtSuffix.Size = New System.Drawing.Size(150, 20)
        Me.txtSuffix.TabIndex = 18
        Me.ToolTipUpdate.SetToolTip(Me.txtSuffix, "Cosa vuoi sostituire dal nome dei player")
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(18, 70)
        Me.Label10.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(36, 13)
        Me.Label10.TabIndex = 17
        Me.Label10.Text = "Prefix:"
        '
        'txtPrefix
        '
        Me.txtPrefix.Location = New System.Drawing.Point(21, 86)
        Me.txtPrefix.Margin = New System.Windows.Forms.Padding(2)
        Me.txtPrefix.Name = "txtPrefix"
        Me.txtPrefix.Size = New System.Drawing.Size(150, 20)
        Me.txtPrefix.TabIndex = 16
        Me.ToolTipUpdate.SetToolTip(Me.txtPrefix, "Cosa vuoi sostituire dal nome dei player")
        '
        'txtWith
        '
        Me.txtWith.Location = New System.Drawing.Point(221, 42)
        Me.txtWith.Margin = New System.Windows.Forms.Padding(2)
        Me.txtWith.Name = "txtWith"
        Me.txtWith.Size = New System.Drawing.Size(156, 20)
        Me.txtWith.TabIndex = 15
        Me.ToolTipUpdate.SetToolTip(Me.txtWith, "Con cosa vuoi sostituire ciò che hai specificato sotto ""Replace""")
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(219, 26)
        Me.Label5.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(32, 13)
        Me.Label5.TabIndex = 14
        Me.Label5.Text = "With:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(18, 26)
        Me.Label4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(50, 13)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "Replace:"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.Label7)
        Me.GroupBox4.Controls.Add(Me.txtMetadatoAnyValue)
        Me.GroupBox4.Controls.Add(Me.txtMetadatoAny)
        Me.GroupBox4.Location = New System.Drawing.Point(217, 16)
        Me.GroupBox4.Margin = New System.Windows.Forms.Padding(2)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Padding = New System.Windows.Forms.Padding(2)
        Me.GroupBox4.Size = New System.Drawing.Size(231, 88)
        Me.GroupBox4.TabIndex = 6
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Metadata Any"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(5, 44)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(34, 13)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Value"
        '
        'txtMetadatoAnyValue
        '
        Me.txtMetadatoAnyValue.Location = New System.Drawing.Point(5, 59)
        Me.txtMetadatoAnyValue.Margin = New System.Windows.Forms.Padding(2)
        Me.txtMetadatoAnyValue.Name = "txtMetadatoAnyValue"
        Me.txtMetadatoAnyValue.Size = New System.Drawing.Size(222, 20)
        Me.txtMetadatoAnyValue.TabIndex = 4
        Me.ToolTipUpdate.SetToolTip(Me.txtMetadatoAnyValue, "Valore del metadato sopra riportato")
        '
        'txtMetadatoAny
        '
        Me.txtMetadatoAny.Location = New System.Drawing.Point(5, 17)
        Me.txtMetadatoAny.Margin = New System.Windows.Forms.Padding(2)
        Me.txtMetadatoAny.Name = "txtMetadatoAny"
        Me.txtMetadatoAny.Size = New System.Drawing.Size(222, 20)
        Me.txtMetadatoAny.TabIndex = 3
        Me.ToolTipUpdate.SetToolTip(Me.txtMetadatoAny, "Nome di un Metadato di qualsiasi tipo non On/Off (senza Player.)")
        '
        'TabControl1
        '
        Me.TabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Location = New System.Drawing.Point(402, 12)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(461, 651)
        Me.TabControl1.TabIndex = 15
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.GroupBox8)
        Me.TabPage1.Controls.Add(Me.GroupBox7)
        Me.TabPage1.Controls.Add(Me.GroupBox3)
        Me.TabPage1.Controls.Add(Me.GroupBox2)
        Me.TabPage1.Controls.Add(Me.btnGo)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(453, 625)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "General"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'GroupBox8
        '
        Me.GroupBox8.Controls.Add(Me.rdbDisable)
        Me.GroupBox8.Controls.Add(Me.rdbEnable)
        Me.GroupBox8.Controls.Add(Me.chkModifyEnabled)
        Me.GroupBox8.Location = New System.Drawing.Point(31, 6)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(224, 63)
        Me.GroupBox8.TabIndex = 16
        Me.GroupBox8.TabStop = False
        Me.GroupBox8.Text = "Enabled"
        '
        'rdbDisable
        '
        Me.rdbDisable.AutoSize = True
        Me.rdbDisable.Location = New System.Drawing.Point(155, 29)
        Me.rdbDisable.Name = "rdbDisable"
        Me.rdbDisable.Size = New System.Drawing.Size(60, 17)
        Me.rdbDisable.TabIndex = 2
        Me.rdbDisable.TabStop = True
        Me.rdbDisable.Text = "Disable"
        Me.rdbDisable.UseVisualStyleBackColor = True
        '
        'rdbEnable
        '
        Me.rdbEnable.AutoSize = True
        Me.rdbEnable.Location = New System.Drawing.Point(91, 29)
        Me.rdbEnable.Name = "rdbEnable"
        Me.rdbEnable.Size = New System.Drawing.Size(58, 17)
        Me.rdbEnable.TabIndex = 1
        Me.rdbEnable.TabStop = True
        Me.rdbEnable.Text = "Enable"
        Me.rdbEnable.UseVisualStyleBackColor = True
        '
        'chkModifyEnabled
        '
        Me.chkModifyEnabled.AutoSize = True
        Me.chkModifyEnabled.Location = New System.Drawing.Point(11, 30)
        Me.chkModifyEnabled.Name = "chkModifyEnabled"
        Me.chkModifyEnabled.Size = New System.Drawing.Size(57, 17)
        Me.chkModifyEnabled.TabIndex = 0
        Me.chkModifyEnabled.Text = "Modify"
        Me.chkModifyEnabled.UseVisualStyleBackColor = True
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.lbAvailableMetadata)
        Me.GroupBox7.Controls.Add(Me.Label6)
        Me.GroupBox7.Controls.Add(Me.Label9)
        Me.GroupBox7.Controls.Add(Me.btnAdd2)
        Me.GroupBox7.Controls.Add(Me.btnRmv2)
        Me.GroupBox7.Controls.Add(Me.lbSelectedMetadata)
        Me.GroupBox7.Location = New System.Drawing.Point(31, 362)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(392, 207)
        Me.GroupBox7.TabIndex = 15
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Add Metadata to Description"
        '
        'lbAvailableMetadata
        '
        Me.lbAvailableMetadata.FormattingEnabled = True
        Me.lbAvailableMetadata.Location = New System.Drawing.Point(21, 38)
        Me.lbAvailableMetadata.Margin = New System.Windows.Forms.Padding(2)
        Me.lbAvailableMetadata.Name = "lbAvailableMetadata"
        Me.lbAvailableMetadata.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
        Me.lbAvailableMetadata.Size = New System.Drawing.Size(150, 147)
        Me.lbAvailableMetadata.TabIndex = 6
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(219, 22)
        Me.Label6.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(97, 13)
        Me.Label6.TabIndex = 9
        Me.Label6.Text = "Selected Metadata"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(19, 22)
        Me.Label9.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(130, 13)
        Me.Label9.TabIndex = 8
        Me.Label9.Text = "Available Player Metadata"
        '
        'btnAdd2
        '
        Me.btnAdd2.Location = New System.Drawing.Point(182, 75)
        Me.btnAdd2.Margin = New System.Windows.Forms.Padding(2)
        Me.btnAdd2.Name = "btnAdd2"
        Me.btnAdd2.Size = New System.Drawing.Size(27, 27)
        Me.btnAdd2.TabIndex = 10
        Me.btnAdd2.Text = ">>"
        Me.ToolTipUpdate.SetToolTip(Me.btnAdd2, "Aggiungi gruppi ai player. Ricorda che devi cliccare ""Lavora!""")
        Me.btnAdd2.UseVisualStyleBackColor = True
        '
        'btnRmv2
        '
        Me.btnRmv2.Location = New System.Drawing.Point(182, 124)
        Me.btnRmv2.Margin = New System.Windows.Forms.Padding(2)
        Me.btnRmv2.Name = "btnRmv2"
        Me.btnRmv2.Size = New System.Drawing.Size(27, 27)
        Me.btnRmv2.TabIndex = 11
        Me.btnRmv2.Text = "<<"
        Me.ToolTipUpdate.SetToolTip(Me.btnRmv2, "Rimuovi gruppi da aggiungere. Questo non rimuoverà i gruppi dai Player")
        Me.btnRmv2.UseVisualStyleBackColor = True
        '
        'lbSelectedMetadata
        '
        Me.lbSelectedMetadata.FormattingEnabled = True
        Me.lbSelectedMetadata.Location = New System.Drawing.Point(221, 38)
        Me.lbSelectedMetadata.Margin = New System.Windows.Forms.Padding(2)
        Me.lbSelectedMetadata.Name = "lbSelectedMetadata"
        Me.lbSelectedMetadata.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
        Me.lbSelectedMetadata.Size = New System.Drawing.Size(156, 147)
        Me.lbSelectedMetadata.TabIndex = 7
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.lbAvailableGroups)
        Me.GroupBox3.Controls.Add(Me.Label3)
        Me.GroupBox3.Controls.Add(Me.Label2)
        Me.GroupBox3.Controls.Add(Me.btnAdd)
        Me.GroupBox3.Controls.Add(Me.btnRmv)
        Me.GroupBox3.Controls.Add(Me.lbGroupsAdd)
        Me.GroupBox3.Location = New System.Drawing.Point(31, 202)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(392, 154)
        Me.GroupBox3.TabIndex = 14
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Add Groups"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.btnShowDone)
        Me.TabPage2.Controls.Add(Me.dtgridExportScheduleDone)
        Me.TabPage2.Controls.Add(Me.btnDeleteSchedule)
        Me.TabPage2.Controls.Add(Me.dtgridExportSchedule)
        Me.TabPage2.Controls.Add(Me.GroupBox6)
        Me.TabPage2.Controls.Add(Me.GroupBox5)
        Me.TabPage2.Controls.Add(Me.btnMeta)
        Me.TabPage2.Controls.Add(Me.GroupBox4)
        Me.TabPage2.Controls.Add(Me.GroupBox1)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(453, 625)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Metadata"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'btnShowDone
        '
        Me.btnShowDone.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnShowDone.Location = New System.Drawing.Point(354, 576)
        Me.btnShowDone.Name = "btnShowDone"
        Me.btnShowDone.Size = New System.Drawing.Size(93, 38)
        Me.btnShowDone.TabIndex = 15
        Me.btnShowDone.Text = "Show Done"
        Me.btnShowDone.UseVisualStyleBackColor = True
        '
        'dtgridExportScheduleDone
        '
        Me.dtgridExportScheduleDone.AllowUserToAddRows = False
        Me.dtgridExportScheduleDone.AllowUserToDeleteRows = False
        Me.dtgridExportScheduleDone.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dtgridExportScheduleDone.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dtgridExportScheduleDone.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgridExportScheduleDone.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.id})
        Me.dtgridExportScheduleDone.Location = New System.Drawing.Point(5, 214)
        Me.dtgridExportScheduleDone.Margin = New System.Windows.Forms.Padding(2)
        Me.dtgridExportScheduleDone.Name = "dtgridExportScheduleDone"
        Me.dtgridExportScheduleDone.ReadOnly = True
        Me.dtgridExportScheduleDone.RowHeadersWidth = 51
        Me.dtgridExportScheduleDone.RowTemplate.Height = 24
        Me.dtgridExportScheduleDone.Size = New System.Drawing.Size(443, 357)
        Me.dtgridExportScheduleDone.TabIndex = 14
        Me.ToolTipUpdate.SetToolTip(Me.dtgridExportScheduleDone, resources.GetString("dtgridExportScheduleDone.ToolTip"))
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Player"
        Me.DataGridViewTextBoxColumn1.MinimumWidth = 6
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 61
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Metadata"
        Me.DataGridViewTextBoxColumn2.MinimumWidth = 6
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 77
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Metadata Value"
        Me.DataGridViewTextBoxColumn3.MinimumWidth = 6
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 98
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "DateTime"
        Me.DataGridViewTextBoxColumn4.MinimumWidth = 6
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 78
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Username"
        Me.DataGridViewTextBoxColumn5.MinimumWidth = 6
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Width = 80
        '
        'id
        '
        Me.id.HeaderText = "ID"
        Me.id.Name = "id"
        Me.id.ReadOnly = True
        Me.id.Visible = False
        Me.id.Width = 43
        '
        'btnDeleteSchedule
        '
        Me.btnDeleteSchedule.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnDeleteSchedule.Location = New System.Drawing.Point(5, 576)
        Me.btnDeleteSchedule.Name = "btnDeleteSchedule"
        Me.btnDeleteSchedule.Size = New System.Drawing.Size(59, 38)
        Me.btnDeleteSchedule.TabIndex = 13
        Me.btnDeleteSchedule.Text = "Delete"
        Me.ToolTipUpdate.SetToolTip(Me.btnDeleteSchedule, "Elimina le righe selezionate sopra e le corrispondenti schedulazioni")
        Me.btnDeleteSchedule.UseVisualStyleBackColor = True
        '
        'dtgridExportSchedule
        '
        Me.dtgridExportSchedule.AllowUserToAddRows = False
        Me.dtgridExportSchedule.AllowUserToDeleteRows = False
        Me.dtgridExportSchedule.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dtgridExportSchedule.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dtgridExportSchedule.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgridExportSchedule.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.clPlayer, Me.clMetadato, Me.clValue, Me.clDateTime, Me.clUser, Me.DataGridViewTextBoxColumn6})
        Me.dtgridExportSchedule.Location = New System.Drawing.Point(5, 214)
        Me.dtgridExportSchedule.Margin = New System.Windows.Forms.Padding(2)
        Me.dtgridExportSchedule.Name = "dtgridExportSchedule"
        Me.dtgridExportSchedule.ReadOnly = True
        Me.dtgridExportSchedule.RowHeadersWidth = 51
        Me.dtgridExportSchedule.RowTemplate.Height = 24
        Me.dtgridExportSchedule.Size = New System.Drawing.Size(443, 357)
        Me.dtgridExportSchedule.TabIndex = 12
        Me.ToolTipUpdate.SetToolTip(Me.dtgridExportSchedule, resources.GetString("dtgridExportSchedule.ToolTip"))
        '
        'clPlayer
        '
        Me.clPlayer.HeaderText = "Player"
        Me.clPlayer.MinimumWidth = 6
        Me.clPlayer.Name = "clPlayer"
        Me.clPlayer.ReadOnly = True
        Me.clPlayer.Width = 61
        '
        'clMetadato
        '
        Me.clMetadato.HeaderText = "Metadata"
        Me.clMetadato.MinimumWidth = 6
        Me.clMetadato.Name = "clMetadato"
        Me.clMetadato.ReadOnly = True
        Me.clMetadato.Width = 77
        '
        'clValue
        '
        Me.clValue.HeaderText = "Metadata Value"
        Me.clValue.MinimumWidth = 6
        Me.clValue.Name = "clValue"
        Me.clValue.ReadOnly = True
        Me.clValue.Width = 98
        '
        'clDateTime
        '
        Me.clDateTime.HeaderText = "DateTime"
        Me.clDateTime.MinimumWidth = 6
        Me.clDateTime.Name = "clDateTime"
        Me.clDateTime.ReadOnly = True
        Me.clDateTime.Width = 78
        '
        'clUser
        '
        Me.clUser.HeaderText = "Username"
        Me.clUser.MinimumWidth = 6
        Me.clUser.Name = "clUser"
        Me.clUser.ReadOnly = True
        Me.clUser.Width = 80
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "ID"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Visible = False
        Me.DataGridViewTextBoxColumn6.Width = 43
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.btnBrowseExcel)
        Me.GroupBox6.Controls.Add(Me.txtMetadataExcel)
        Me.GroupBox6.Location = New System.Drawing.Point(217, 109)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(230, 100)
        Me.GroupBox6.TabIndex = 11
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Excel Schedule"
        '
        'btnBrowseExcel
        '
        Me.btnBrowseExcel.Location = New System.Drawing.Point(88, 56)
        Me.btnBrowseExcel.Name = "btnBrowseExcel"
        Me.btnBrowseExcel.Size = New System.Drawing.Size(64, 26)
        Me.btnBrowseExcel.TabIndex = 1
        Me.btnBrowseExcel.Text = "Browse"
        Me.btnBrowseExcel.UseVisualStyleBackColor = True
        '
        'txtMetadataExcel
        '
        Me.txtMetadataExcel.Location = New System.Drawing.Point(8, 30)
        Me.txtMetadataExcel.Name = "txtMetadataExcel"
        Me.txtMetadataExcel.Size = New System.Drawing.Size(216, 20)
        Me.txtMetadataExcel.TabIndex = 0
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.chkSchedule)
        Me.GroupBox5.Controls.Add(Me.Label8)
        Me.GroupBox5.Controls.Add(Me.dtSchedule)
        Me.GroupBox5.Location = New System.Drawing.Point(3, 109)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(210, 100)
        Me.GroupBox5.TabIndex = 9
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Schedule"
        '
        'chkSchedule
        '
        Me.chkSchedule.AutoSize = True
        Me.chkSchedule.Location = New System.Drawing.Point(6, 19)
        Me.chkSchedule.Name = "chkSchedule"
        Me.chkSchedule.Size = New System.Drawing.Size(83, 17)
        Me.chkSchedule.TabIndex = 2
        Me.chkSchedule.Text = "Scheduled?"
        Me.ToolTipUpdate.SetToolTip(Me.chkSchedule, "Selezionando questa casella schedulerai l'aggiornamento secondo la data e l'orari" &
        "o sotto impostate")
        Me.chkSchedule.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(6, 39)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(59, 13)
        Me.Label8.TabIndex = 1
        Me.Label8.Text = "Change on"
        '
        'dtSchedule
        '
        Me.dtSchedule.CustomFormat = "dd/MM/yyyy  -  HH:mm"
        Me.dtSchedule.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtSchedule.Location = New System.Drawing.Point(9, 55)
        Me.dtSchedule.Name = "dtSchedule"
        Me.dtSchedule.Size = New System.Drawing.Size(168, 20)
        Me.dtSchedule.TabIndex = 0
        Me.ToolTipUpdate.SetToolTip(Me.dtSchedule, "Imposta quando deve essere schedulato l'aggiornamento dei metadati")
        Me.dtSchedule.Value = New Date(2019, 8, 14, 11, 21, 30, 0)
        '
        'btnMeta
        '
        Me.btnMeta.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnMeta.Location = New System.Drawing.Point(183, 576)
        Me.btnMeta.Name = "btnMeta"
        Me.btnMeta.Size = New System.Drawing.Size(90, 38)
        Me.btnMeta.TabIndex = 8
        Me.btnMeta.Text = "Cambia quei Metadati!"
        Me.ToolTipUpdate.SetToolTip(Me.btnMeta, "Con questo bottone cambierai i metadati come impostato. L'aggiornamento verrà sch" &
        "edulato se la checkbox corrispondente è flaggata.")
        Me.btnMeta.UseVisualStyleBackColor = True
        '
        'frmUpdatePlayers
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(869, 682)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtPlayers)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.MinimumSize = New System.Drawing.Size(885, 721)
        Me.Name = "frmUpdatePlayers"
        Me.Text = "Update Players"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.GroupBox8.ResumeLayout(False)
        Me.GroupBox8.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        CType(Me.dtgridExportScheduleDone, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtgridExportSchedule, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txtPlayers As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents txtMetadatoBoolean As TextBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents rdOFF As RadioButton
    Friend WithEvents rdOn As RadioButton
    Friend WithEvents btnGo As Button
    Friend WithEvents lbAvailableGroups As ListBox
    Friend WithEvents lbGroupsAdd As ListBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents btnAdd As Button
    Friend WithEvents btnRmv As Button
    Friend WithEvents txtReplace As TextBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents txtWith As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents txtMetadatoAnyValue As TextBox
    Friend WithEvents txtMetadatoAny As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents btnMeta As Button
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents Label8 As Label
    Friend WithEvents dtSchedule As DateTimePicker
    Friend WithEvents GroupBox6 As GroupBox
    Friend WithEvents btnBrowseExcel As Button
    Friend WithEvents txtMetadataExcel As TextBox
    Friend WithEvents chkSchedule As CheckBox
    Friend WithEvents dtgridExportSchedule As DataGridView
    Friend WithEvents btnDeleteSchedule As Button
    Friend WithEvents ToolTipUpdate As ToolTip
    Friend WithEvents btnShowDone As Button
    Friend WithEvents dtgridExportScheduleDone As DataGridView
    Friend WithEvents GroupBox7 As GroupBox
    Friend WithEvents lbAvailableMetadata As ListBox
    Friend WithEvents Label6 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents btnAdd2 As Button
    Friend WithEvents btnRmv2 As Button
    Friend WithEvents lbSelectedMetadata As ListBox
    Friend WithEvents Label10 As Label
    Friend WithEvents txtPrefix As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents txtSuffix As TextBox
    Friend WithEvents GroupBox8 As GroupBox
    Friend WithEvents rdbDisable As RadioButton
    Friend WithEvents rdbEnable As RadioButton
    Friend WithEvents chkModifyEnabled As CheckBox
    Friend WithEvents DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As DataGridViewTextBoxColumn
    Friend WithEvents id As DataGridViewTextBoxColumn
    Friend WithEvents clPlayer As DataGridViewTextBoxColumn
    Friend WithEvents clMetadato As DataGridViewTextBoxColumn
    Friend WithEvents clValue As DataGridViewTextBoxColumn
    Friend WithEvents clDateTime As DataGridViewTextBoxColumn
    Friend WithEvents clUser As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As DataGridViewTextBoxColumn
End Class
