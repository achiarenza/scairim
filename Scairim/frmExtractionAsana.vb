﻿Imports Excel = Microsoft.Office.Interop.Excel
Imports Newtonsoft.Json.Linq

Public Class frmExtractionAsana

    Dim LinkAsana As String = "https://app.asana.com/api/1.0"
    Dim Json, Json2 As JObject
    Dim result As String
    Dim loggato As Boolean

    Private Sub FrmExtractionAsana_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Icon = Scairim.Icon
        cmbProjects.Items.Clear()
        loggato = False

        If IO.File.Exists(IO.Path.Combine(IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Scairim"), "temp.txt")) Then
            For Each line In IO.File.ReadAllLines(IO.Path.Combine(IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Scairim"), "temp.txt"))
                If Split(line, "=")(0) = "BearerToken" Then
                    txtBearer.Text = Split(line, "=")(1)
                End If
            Next
        End If

    End Sub

    Private Sub BtnGo_Click(sender As Object, e As EventArgs) Handles btnGo.Click
        If cmbProjects.Text.Trim <> "" And (loggato) Then
            Logga("Faccio un'estrazione!!!")
            Try
                Loading("show")
                result = Estrai(Split(cmbProjects.Text, ",")(1).Trim)
                Loading("close")
            Catch ex As Exception
                Logga("ERROR - Estrazione: " & ex.ToString)
                Exit Sub
            End Try
            Logga(result)
            MsgBox(result)
            btnExportExcel.Visible = True
        Else
            Logga("Per favore esegui il login e scegli un progetto :)")
        End If
    End Sub

    Private Sub BtnLoginAsana_Click(sender As Object, e As EventArgs) Handles btnLoginAsana.Click

        Loading("show")

        If chkRemember.Checked Then
            ScriviIn("temp.txt", "BearerToken=" & txtBearer.Text, True)
        End If

        cmbProjects.Items.Clear()
        result = GetStuffsBearer(txtBearer.Text, LinkAsana & "/projects?limit=100&workspace=16768288464304")
        If InStr(result, "(401)") = 0 Then
            Json = JObject.Parse(result)
            While True
                For Each child In Json.Item("data").Children()
                    cmbProjects.Items.Add(child.Item("name").ToString & ", " & child.Item("id").ToString)
                Next
                If Json.Item("next_page").HasValues Then
                    result = GetStuffsBearer(txtBearer.Text, Json.Item("next_page").Item("uri").ToString)
                    Json = JObject.Parse(result)
                Else
                    Exit While
                End If
            End While
            loggato = True
            Loading("close")
            Logga("Login effettuato su Asana!")
            MsgBox("Login effettuato!")
        Else
            Logga("Bearer token non autorizzato!")
            MsgBox("Bearer token non autorizzato!")
        End If
    End Sub

    Private Sub BtnExportExcel_Click(sender As Object, e As EventArgs) Handles btnExportExcel.Click
        Dim excelfile As String
        Dim xlApp As Excel.Application = New Microsoft.Office.Interop.Excel.Application()

        If xlApp Is Nothing Then
            Logga("Excel Non è installato nel sistema!!")
            Return
        End If

        Dim savefile As New SaveFileDialog With {
            .InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyComputer),
            .Filter = "excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*",
            .FilterIndex = 1,
            .RestoreDirectory = True,
            .OverwritePrompt = True
        }
        If savefile.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            excelfile = savefile.FileName
        Else
            Return
        End If

        Loading("show")

        Dim xlWorkBook As Excel.Workbook
        xlWorkBook = xlApp.Workbooks.Add()
        Dim xlWorkSheet = xlWorkBook.Sheets(1)

        For Each column In dtgridExport.Columns
            xlWorkSheet.Cells(1, column.Index + 1).Value = column.HeaderText
        Next

        Dim i As Integer = 2
        Dim c As Integer = 1
        Dim Contatore = 0
        For Each row In dtgridExport.Rows
            If Contatore Mod 10 = 0 Then
                Loading(CInt((Contatore * 100) / dtgridExport.Rows.Count))
            End If
            c = 1
            For Each column In dtgridExport.Columns
                If Not dtgridExport.Item(column.Index, row.Index).Value Is Nothing Then
                    xlWorkSheet.Cells(i, c).Value = dtgridExport.Item(column.Index, row.Index).Value.ToString
                End If
                c += 1
            Next
            i += 1
        Next

        xlWorkBook.SaveAs(excelfile)
        xlWorkBook.Close()
        xlApp.Quit()

        ReleaseComObject(xlWorkSheet)
        ReleaseComObject(xlWorkBook)
        ReleaseComObject(xlApp)

        Loading("close")
        Logga("Estrazione Esportata!")
        MsgBox("Estrazione Esportata!")
    End Sub

    Private Function Estrai(ProjectId As String)

        Dim RowIndex As Integer
        Dim ColumnCount As Integer = 0
        Dim clIndxTaskName As Integer
        Dim clIndxTaskAssignee As Integer
        Dim clIndxTaskDueDate As Integer
        Dim clIndxTaskCreateDate As Integer
        Dim clIndxTaskStatus As Integer
        Dim clIndxTaskTags(10) As Integer
        Dim clIndxTaskURL As Integer
        Dim temp As String = ""
        Dim name As String = ""
        Dim check As String
        Dim t As Integer = 0

        dtgridExport.Rows.Clear()
        dtgridExport.Columns.Clear()

        dtgridExport.Columns.Add("TaskName", "Name")
        clIndxTaskName = ColumnCount
        ColumnCount = ColumnCount + 1

        dtgridExport.Columns.Add("TaskAssignee", "Assignee")
        clIndxTaskAssignee = ColumnCount
        ColumnCount = ColumnCount + 1

        dtgridExport.Columns.Add("CreateDate", "Creation Date")
        clIndxTaskCreateDate = ColumnCount
        ColumnCount = ColumnCount + 1

        dtgridExport.Columns.Add("TaskDueDate", "Due Date")
        clIndxTaskDueDate = ColumnCount
        ColumnCount = ColumnCount + 1

        dtgridExport.Columns.Add("TaskStatus", "Completion Date")
        clIndxTaskStatus = ColumnCount
        ColumnCount = ColumnCount + 1

        dtgridExport.Columns.Add("TaskURL", "URL")
        clIndxTaskURL = ColumnCount
        ColumnCount = ColumnCount + 1

        check = GetStuffsBearer(txtBearer.Text, LinkAsana & "/projects/" & ProjectId & "/tasks?opt_fields=id,assignee,created_at,completed_at,due_on,name&limit=100")
        Json = JObject.Parse(check)
        While True
            If Json.Item("data").HasValues Then
                For Each child In Json.Item("data").Children()
                    name = child.Item("name").ToString
                    If Not name.EndsWith(":") And name <> "" Then
                        RowIndex = dtgridExport.Rows.Add()
                        dtgridExport.Item(clIndxTaskName, RowIndex).Value = name
                        If child.Item("assignee").HasValues Then
                            temp = GetStuffsBearer(txtBearer.Text, LinkAsana & "/users/" & child.Item("assignee").Item("id").ToString & "?opt_fields=name")
                            Json2 = JObject.Parse(temp)
                            dtgridExport.Item(clIndxTaskAssignee, RowIndex).Value = Json2.Item("data").Item("name").ToString
                        Else
                            dtgridExport.Item(clIndxTaskAssignee, RowIndex).Value = ""
                        End If
                        dtgridExport.Item(clIndxTaskCreateDate, RowIndex).Value = Split(child.Item("created_at").ToString, " ")(0)
                        If child.Item("completed_at").ToString <> "" Then
                            dtgridExport.Item(clIndxTaskStatus, RowIndex).Value = Split(child.Item("completed_at").ToString, " ")(0)
                        Else
                            dtgridExport.Item(clIndxTaskStatus, RowIndex).Value = "Ongoing"
                        End If
                        If child.Item("due_on").ToString <> "" Then
                            dtgridExport.Item(clIndxTaskDueDate, RowIndex).Value = child.Item("due_on").ToString
                        Else
                            dtgridExport.Item(clIndxTaskDueDate, RowIndex).Value = ""
                        End If
                        dtgridExport.Item(clIndxTaskURL, RowIndex).Value = "https://app.asana.com/0/0/" & child.Item("id").ToString & "/f"
                        temp = GetStuffsBearer(txtBearer.Text, LinkAsana & "/tasks/" & child.Item("id").ToString & "/tags?limit=100")
                        Json2 = JObject.Parse(temp)
                        If Json2.Item("data").HasValues Then
                            t = 0
                            For Each tagg In Json2.Item("data").Children()
                                If Not dtgridExport.Columns.Contains("TaskTag" & (t + 1)) Then
                                    dtgridExport.Columns.Add("TaskTag" & (t + 1), "Tag " & (t + 1))
                                    clIndxTaskTags(t) = ColumnCount
                                    ColumnCount = ColumnCount + 1
                                End If
                                dtgridExport.Item(clIndxTaskTags(t), RowIndex).Value = tagg.Item("name")
                                t = t + 1
                            Next
                        End If
                    End If
                Next
            End If
            If Json.Item("next_page").HasValues Then
                result = GetStuffsBearer(txtBearer.Text, Json.Item("next_page").Item("uri").ToString)
                Json = JObject.Parse(result)
            Else
                Exit While
            End If
        End While
        Json = Nothing
        Json2 = Nothing
        Return "Estrazione Completata!!!"
    End Function

End Class