﻿Imports Newtonsoft.Json.Linq

Public Class frmDeleteOldStuffs

    Dim Json, Json2 As JObject

    Private Sub DeleteOldStuffs_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Icon = Scairim.Icon
    End Sub

    Private Sub BtnCheck_Click(sender As Object, e As EventArgs) Handles btnCheck.Click

        Dim result As String

        Utilities.timeout = 120000
        txtStuffsToDelete.Text = ""
        If (Scairim.logged) Then
            Logga("Controllo cosa eliminare...")
            Try
                result = CheckToDelete()
            Catch ex As Exception
                Logga("ERROR - CheckToDelete: " & ex.ToString)
                Exit Sub
            End Try
            Logga(result)
            MsgBox(result)
            btnDelete.Visible = True
        Else
            Logga("Se prima non fai il login su un CM non posso fare nulla :)")
        End If

    End Sub

    Private Sub BtnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Dim result As String

        If (Scairim.logged) Then
            Logga("Elimino...")
            Try
                result = Delete()
                txtStuffsToDelete.Text = ""
            Catch ex As Exception
                Logga("ERROR - Delete: " & ex.ToString)
                txtStuffsToDelete.Text = ""
                Exit Sub
            End Try
            Logga(result)
            MsgBox(result)
            btnDelete.Visible = False
        Else
            Logga("Se prima non fai il login su un CM non posso fare nulla :)")
        End If
    End Sub

    Private Function CheckToDelete()
        Dim query As String
        Dim result As String
        Dim count As Integer
        Dim ChannelCount As Integer
        Dim PlaylistCount As Integer
        Dim MessageCount As Integer
        Dim DateToCheck As Date

        If Not Date.TryParse(txtDate.Text, DateToCheck) Then
            DateToCheck = Now()
        End If

        If chkPlaylists.Checked Then
            query = "playlists?limit=9999&offset=0&fields=id,name,lastModified,channelsCount,asSubPlaylistsCount"
            result = GetStuffs(Scairim.token, Scairim.apiToken, Scairim.LinkCM & "/api/rest/" & query)
            If InStr(result, "(401)") > 0 Then
                Return "Sessione scaduta, per favore rifai il Login."
            End If
            If result = "" Then
                Return "Risposta vuota"
            End If
            Json = JObject.Parse(result)
            If Json.Item("list").HasValues Then
                Logga("Controllo " & Json.Item("count").ToString & " playlist...")
                For Each playlist In Json.Item("list").Children
                    If DateAndTime.DateValue(playlist.Item("lastModified").ToString) < DateToCheck Then
                        query = "playlists/usage?ids=" & playlist.Item("id").ToString
                        result = GetStuffs(Scairim.token, Scairim.apiToken, Scairim.LinkCM & "/api/rest/" & query)
                        If InStr(result, "(401)") > 0 Then
                            Return "Sessione scaduta, per favore rifai il Login."
                        End If
                        If result = "" Then
                            Return "Risposta vuota"
                        End If
                        Json2 = JObject.Parse(result)
                        If playlist.Item("channelsCount") Is Nothing Then
                            ChannelCount = 0
                        Else
                            ChannelCount = CInt(playlist.Item("channelsCount").ToString)
                        End If
                        If playlist.Item("asSubPlaylistsCount") Is Nothing Then
                            PlaylistCount = 0
                        Else
                            PlaylistCount = CInt(playlist.Item("asSubPlaylistsCount").ToString)
                        End If
                        count = 0 + ChannelCount + PlaylistCount
                        For Each valore In Json2.Values
                            count = count + CInt(valore.ToString)
                        Next
                        If count = 0 Then
                            txtStuffsToDelete.AppendText("Playlist: " & playlist.Item("name").ToString & "," & playlist.Item("id").ToString & vbNewLine)
                        End If
                    End If
                Next
            End If
        End If
        If chkMedia.Checked Then
            query = "media?limit=0&offset=0&sort=name&fields=name,lastModified,usingPlaylistsCount,usingMessagesCount,usingTemplatesCount"
            result = GetStuffs(Scairim.token, Scairim.apiToken, Scairim.LinkCM & "/api/rest/" & query)
            If InStr(result, "(401)") > 0 Then
                Return "Sessione scaduta, per favore rifai il Login."
            End If
            If result = "" Then
                Return "Risposta vuota"
            End If
            Json = JObject.Parse(result)
            If Json.Item("list").HasValues Then
                Logga("Controllo " & Json.Item("count").ToString & " media...")
                For Each medias In Json.Item("list").Children
                    If DateAndTime.DateValue(medias.Item("lastModified").ToString) < DateToCheck Then
                        If medias.Item("templatesCount") Is Nothing Then
                            ChannelCount = 0
                        Else
                            ChannelCount = CInt(medias.Item("templatesCount").ToString)
                        End If
                        If medias.Item("playlistsCount") Is Nothing Then
                            PlaylistCount = 0
                        Else
                            PlaylistCount = CInt(medias.Item("playlistsCount").ToString)
                        End If
                        If medias.Item("messagesCount") Is Nothing Then
                            MessageCount = 0
                        Else
                            MessageCount = CInt(medias.Item("messagesCount").ToString)
                        End If
                        If ChannelCount + PlaylistCount + MessageCount = 0 Then
                            txtStuffsToDelete.AppendText("Media: " & medias.Item("name").ToString & "," & medias.Item("id").ToString & vbNewLine)
                        End If
                    End If
                Next
            End If
        End If
        Json = Nothing
        Json2 = Nothing
        Return "Check completato!!!"
    End Function

    Private Function Delete()
        Dim result
        For Each linea In txtStuffsToDelete.Lines
            If linea.Trim <> "" Then
                If Split(linea, ":")(0).Trim = "Playlist" Then
                    result = GetStuffs(Scairim.token, Scairim.apiToken, Scairim.LinkCM & "/api/rest/playlists/" & Split(Split(linea, ":")(1).Trim, ",")(1).Trim & "?fields=name")
                    Json = JObject.Parse(result)
                    If Split(Split(linea, ":")(1).Trim, ",")(0).Trim = Json.Item("name").ToString And Split(Split(linea, ":")(1).Trim, ",")(1).Trim = Json.Item("id").ToString Then
                        Logga("Elimino Playlist " & Split(Split(linea, ":")(1).Trim, ",")(0).Trim & " ID: " & Split(Split(linea, ":")(1).Trim, ",")(1).Trim & "...")
                        result = DeleteStuffs(Scairim.token, Scairim.apiToken, Scairim.LinkCM & "/api/rest/playlists/" & Split(Split(linea, ":")(1).Trim, ",")(1).Trim)
                        If InStr(result, "NoContent") > 0 Then
                            Logga("Done")
                        Else
                            Logga(result)
                        End If
                    End If
                End If
                If Split(linea, ":")(0).Trim = "Media" Then
                    result = GetStuffs(Scairim.token, Scairim.apiToken, Scairim.LinkCM & "/api/rest/media/" & Split(Split(linea, ":")(1).Trim, ",")(1).Trim & "?fields=name")
                    Json = JObject.Parse(result)
                    If Split(Split(linea, ":")(1).Trim, ",")(0).Trim = Json.Item("name").ToString And Split(Split(linea, ":")(1).Trim, ",")(1).Trim = Json.Item("id").ToString Then
                        Logga("Elimino Media " & Json.Item("name").ToString & " ID: " & Json.Item("id").ToString & "...")
                        result = DeleteStuffs(Scairim.token, Scairim.apiToken, Scairim.LinkCM & "/api/rest/media/" & Split(Split(linea, ":")(1).Trim, ",")(1).Trim)
                        If InStr(result, "NoContent") > 0 Then
                            Logga("Done")
                        Else
                            Logga(result)
                        End If
                    End If
                End If
            End If
        Next
        Json = Nothing
        Return "Eliminazione effettuata!"
    End Function

End Class