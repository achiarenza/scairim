﻿Imports Excel = Microsoft.Office.Interop.Excel
Imports Newtonsoft.Json.Linq
Public Class frmExtractionTV

    Dim Json, Json2 As JObject
    Dim result As String
    Dim GroupIdMap As New Dictionary(Of String, String)

    Private Sub btnEstrai_Click(sender As Object, e As EventArgs) Handles btnEstrai.Click
        Utilities.timeout = 120000
        Logga("Faccio un'estrazione!!!")
        Try
            Loading("show", "EBP")
            Estrai()
            Loading("close")
        Catch ex As Exception
            Logga("ERROR - frmExtractionTV: " & ex.ToString)
        End Try
        Logga(result)
        MsgBox(result)
        btnExportExcel.Visible = True
    End Sub

    Private Sub frmExtractionTV_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Icon = Scairim.Icon
        btnExportExcel.Visible = False
        dtgridExport.Rows.Clear()
        dtgridExport.Columns.Clear()
        lbAvailableGroups.Items.Clear()
        lbSelectedGroups.Items.Clear()
        CheckForIllegalCrossThreadCalls = False
        Loading("show")
        Try
            Dim check = GetStuffsBearer(Scairim.tokenTeamviewer, "https://webapi.teamviewer.com/api/v1/groups")
            If InStr(check, "(401)") > 0 Then
                Logga("Not authorized, please use a correct token")
                Exit Sub
            End If
            Json = JObject.Parse(check)
            If Json.ContainsKey("groups") Then
                For Each child In Json.Item("groups").Children()
                    lbAvailableGroups.Items.Add(child.Item("name").ToString)
                    GroupIdMap.Add(child.Item("id").ToString, child.Item("name").ToString)
                Next
            End If
        Catch ex As Exception
            Logga("ERROR - frmExtractionTV_Load: " & ex.ToString)
        End Try
        Loading("close")
    End Sub

    Private Sub btnExportExcel_Click(sender As Object, e As EventArgs) Handles btnExportExcel.Click
        Dim excelfile As String
        Dim xlApp As Excel.Application = New Microsoft.Office.Interop.Excel.Application()

        If xlApp Is Nothing Then
            Logga("Excel Non è installato nel sistema!!")
            Return
        End If

        Dim savefile As New SaveFileDialog With {
            .InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyComputer),
            .Filter = "excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*",
            .FilterIndex = 1,
            .RestoreDirectory = True,
            .OverwritePrompt = True
        }
        If savefile.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            excelfile = savefile.FileName
        Else
            Return
        End If

        Loading("show")

        Dim xlWorkBook As Excel.Workbook
        xlWorkBook = xlApp.Workbooks.Add()
        Dim xlWorkSheet = xlWorkBook.Sheets(1)

        For Each column In dtgridExport.Columns
            xlWorkSheet.Cells(1, column.Index + 1).Value = column.HeaderText
        Next

        Dim i As Integer = 2
        Dim c As Integer = 1
        Dim Contatore = 0
        For Each row In dtgridExport.Rows
            If Contatore Mod 10 = 0 Then
                Loading(CInt((Contatore * 100) / dtgridExport.Rows.Count))
            End If
            c = 1
            For Each column In dtgridExport.Columns
                If Not dtgridExport.Item(column.Index, row.Index).Value Is Nothing Then
                    xlWorkSheet.Cells(i, c).Value = dtgridExport.Item(column.Index, row.Index).Value.ToString
                End If
                c += 1
            Next
            i += 1
            Contatore += 1
        Next

        xlWorkBook.SaveAs(excelfile)
        xlWorkBook.Close()
        xlApp.Quit()

        ReleaseComObject(xlWorkSheet)
        ReleaseComObject(xlWorkBook)
        ReleaseComObject(xlApp)

        Loading("close")

        Logga("Extraction exported!")
        MsgBox("Extraction exported!")
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        For i As Integer = lbAvailableGroups.SelectedItems.Count - 1 To 0 Step -1
            lbSelectedGroups.Items.Add(lbAvailableGroups.SelectedItems.Item(i))
            lbAvailableGroups.Items.Remove(lbAvailableGroups.SelectedItems(i))
        Next
    End Sub

    Private Sub btnRmv_Click(sender As Object, e As EventArgs) Handles btnRmv.Click
        For i As Integer = lbSelectedGroups.SelectedItems.Count - 1 To 0 Step -1
            lbAvailableGroups.Items.Add(lbSelectedGroups.SelectedItems.Item(i))
            lbSelectedGroups.Items.Remove(lbSelectedGroups.SelectedItems(i))
        Next
    End Sub

    Private Sub Estrai()
        Dim RowIndex As Integer
        Dim check As String
        Dim NumChild As Integer
        Dim Contatore As Integer

        dtgridExport.Rows.Clear()
        dtgridExport.Columns.Clear()

        dtgridExport.Columns.Add("PlayerName", "Player Name")

        If chkRemoteID.Checked Then
            dtgridExport.Columns.Add("RemoteID", "Connection ID")
        End If
        If chkGroups.Checked Then
            dtgridExport.Columns.Add("Group", "Group")
        End If
        If chkStatus.Checked Then
            dtgridExport.Columns.Add("Status", "Status")
        End If

        check = GetStuffsBearer(Scairim.tokenTeamviewer, "https://webapi.teamviewer.com/api/v1/devices")
        If InStr(check, "(401)") > 0 Then
            result = "Not authorized, please check your token"
            Exit Sub
        End If
        Json = JObject.Parse(check)
        If Json.ContainsKey("devices") Then
            NumChild = Json.Item("devices").Children().Count
            Contatore = 0
            For Each child In Json.Item("devices").Children()
                If Contatore Mod 10 = 0 Then
                    Loading(CInt((Contatore * 100) / NumChild))
                End If
                If Not txtPlayerName.Text = "" Then
                    If chkUpLow.Checked Then
                        If InStr(child.Item("alias").ToString, txtPlayerName.Text) = 0 Then
                            Contatore += 1
                            Continue For
                        End If
                    Else
                        If InStr(child.Item("alias").ToString.ToLower, txtPlayerName.Text.ToLower) = 0 Then
                            Contatore += 1
                            Continue For
                        End If
                    End If
                End If
                If lbSelectedGroups.Items.Count > 0 Then
                    If Not lbSelectedGroups.Items.Contains(GroupIdMap(child.Item("groupid").ToString)) Then
                        Contatore += 1
                        Continue For
                    End If
                End If
                RowIndex = dtgridExport.Rows.Add()
                dtgridExport.Item(dtgridExport.Columns("PlayerName").Index, RowIndex).Value = child.Item("alias").ToString
                If chkRemoteID.Checked Then
                    dtgridExport.Item(dtgridExport.Columns("RemoteID").Index, RowIndex).Value = child.Item("remotecontrol_id").ToString.Replace("r", "")
                End If
                If chkGroups.Checked Then
                    dtgridExport.Item(dtgridExport.Columns("Group").Index, RowIndex).Value = GroupIdMap(child.Item("groupid").ToString)
                End If
                If chkStatus.Checked Then
                    If child.Item("last_seen") Is Nothing Then
                        dtgridExport.Item(dtgridExport.Columns("Status").Index, RowIndex).Value = child.Item("online_state").ToString
                    Else
                        dtgridExport.Item(dtgridExport.Columns("Status").Index, RowIndex).Value = CDate(child.Item("last_seen")).ToString("yyyy/mm/dd hh:MM")
                    End If
                End If
                Contatore += 1
            Next
        End If
        result = "Estrazione Completata!!!"
    End Sub

End Class