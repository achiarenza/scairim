﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUsers
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dtgridExport = New System.Windows.Forms.DataGridView()
        Me.btnGo = New System.Windows.Forms.Button()
        Me.txtName = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.chkUpLow = New System.Windows.Forms.CheckBox()
        Me.chkFirstname = New System.Windows.Forms.CheckBox()
        Me.chkLastname = New System.Windows.Forms.CheckBox()
        Me.chkEmail = New System.Windows.Forms.CheckBox()
        Me.chkFullName = New System.Windows.Forms.CheckBox()
        Me.chkLastLogin = New System.Windows.Forms.CheckBox()
        Me.chkRoles = New System.Windows.Forms.CheckBox()
        Me.btnDisable = New System.Windows.Forms.Button()
        Me.btnDelete = New System.Windows.Forms.Button()
        Me.chkEnabled = New System.Windows.Forms.CheckBox()
        Me.btnEnable = New System.Windows.Forms.Button()
        CType(Me.dtgridExport, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dtgridExport
        '
        Me.dtgridExport.AllowUserToAddRows = False
        Me.dtgridExport.AllowUserToDeleteRows = False
        Me.dtgridExport.AllowUserToOrderColumns = True
        Me.dtgridExport.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dtgridExport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgridExport.Location = New System.Drawing.Point(12, 138)
        Me.dtgridExport.Name = "dtgridExport"
        Me.dtgridExport.ReadOnly = True
        Me.dtgridExport.RowTemplate.Height = 24
        Me.dtgridExport.Size = New System.Drawing.Size(750, 391)
        Me.dtgridExport.TabIndex = 0
        '
        'btnGo
        '
        Me.btnGo.Location = New System.Drawing.Point(629, 535)
        Me.btnGo.Name = "btnGo"
        Me.btnGo.Size = New System.Drawing.Size(133, 42)
        Me.btnGo.TabIndex = 1
        Me.btnGo.Text = "Lista Utenti"
        Me.btnGo.UseVisualStyleBackColor = True
        '
        'txtName
        '
        Me.txtName.Location = New System.Drawing.Point(12, 38)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(168, 22)
        Me.txtName.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(108, 17)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Username Filter"
        '
        'chkUpLow
        '
        Me.chkUpLow.AutoSize = True
        Me.chkUpLow.Location = New System.Drawing.Point(186, 38)
        Me.chkUpLow.Name = "chkUpLow"
        Me.chkUpLow.Size = New System.Drawing.Size(123, 21)
        Me.chkUpLow.TabIndex = 4
        Me.chkUpLow.Text = "Case Sensitive"
        Me.chkUpLow.UseVisualStyleBackColor = True
        '
        'chkFirstname
        '
        Me.chkFirstname.AutoSize = True
        Me.chkFirstname.Location = New System.Drawing.Point(16, 75)
        Me.chkFirstname.Name = "chkFirstname"
        Me.chkFirstname.Size = New System.Drawing.Size(94, 21)
        Me.chkFirstname.TabIndex = 5
        Me.chkFirstname.Text = "FirstName"
        Me.chkFirstname.UseVisualStyleBackColor = True
        '
        'chkLastname
        '
        Me.chkLastname.AutoSize = True
        Me.chkLastname.Location = New System.Drawing.Point(16, 102)
        Me.chkLastname.Name = "chkLastname"
        Me.chkLastname.Size = New System.Drawing.Size(94, 21)
        Me.chkLastname.TabIndex = 6
        Me.chkLastname.Text = "LastName"
        Me.chkLastname.UseVisualStyleBackColor = True
        '
        'chkEmail
        '
        Me.chkEmail.AutoSize = True
        Me.chkEmail.Location = New System.Drawing.Point(186, 75)
        Me.chkEmail.Name = "chkEmail"
        Me.chkEmail.Size = New System.Drawing.Size(64, 21)
        Me.chkEmail.TabIndex = 7
        Me.chkEmail.Text = "Email"
        Me.chkEmail.UseVisualStyleBackColor = True
        '
        'chkFullName
        '
        Me.chkFullName.AutoSize = True
        Me.chkFullName.Location = New System.Drawing.Point(186, 102)
        Me.chkFullName.Name = "chkFullName"
        Me.chkFullName.Size = New System.Drawing.Size(93, 21)
        Me.chkFullName.TabIndex = 8
        Me.chkFullName.Text = "Full Name"
        Me.chkFullName.UseVisualStyleBackColor = True
        '
        'chkLastLogin
        '
        Me.chkLastLogin.AutoSize = True
        Me.chkLastLogin.Location = New System.Drawing.Point(352, 75)
        Me.chkLastLogin.Name = "chkLastLogin"
        Me.chkLastLogin.Size = New System.Drawing.Size(96, 21)
        Me.chkLastLogin.TabIndex = 9
        Me.chkLastLogin.Text = "Last Login"
        Me.chkLastLogin.UseVisualStyleBackColor = True
        '
        'chkRoles
        '
        Me.chkRoles.AutoSize = True
        Me.chkRoles.Location = New System.Drawing.Point(352, 102)
        Me.chkRoles.Name = "chkRoles"
        Me.chkRoles.Size = New System.Drawing.Size(66, 21)
        Me.chkRoles.TabIndex = 10
        Me.chkRoles.Text = "Roles"
        Me.chkRoles.UseVisualStyleBackColor = True
        '
        'btnDisable
        '
        Me.btnDisable.Location = New System.Drawing.Point(116, 535)
        Me.btnDisable.Name = "btnDisable"
        Me.btnDisable.Size = New System.Drawing.Size(98, 42)
        Me.btnDisable.TabIndex = 11
        Me.btnDisable.Text = "Disabilita"
        Me.btnDisable.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Location = New System.Drawing.Point(220, 535)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(98, 42)
        Me.btnDelete.TabIndex = 12
        Me.btnDelete.Text = "Elimina"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'chkEnabled
        '
        Me.chkEnabled.AutoSize = True
        Me.chkEnabled.Location = New System.Drawing.Point(491, 75)
        Me.chkEnabled.Name = "chkEnabled"
        Me.chkEnabled.Size = New System.Drawing.Size(82, 21)
        Me.chkEnabled.TabIndex = 13
        Me.chkEnabled.Text = "Enabled"
        Me.chkEnabled.UseVisualStyleBackColor = True
        '
        'btnEnable
        '
        Me.btnEnable.Location = New System.Drawing.Point(12, 535)
        Me.btnEnable.Name = "btnEnable"
        Me.btnEnable.Size = New System.Drawing.Size(98, 42)
        Me.btnEnable.TabIndex = 14
        Me.btnEnable.Text = "Abilita"
        Me.btnEnable.UseVisualStyleBackColor = True
        '
        'frmUsers
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(774, 589)
        Me.Controls.Add(Me.btnEnable)
        Me.Controls.Add(Me.chkEnabled)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.btnDisable)
        Me.Controls.Add(Me.chkRoles)
        Me.Controls.Add(Me.chkLastLogin)
        Me.Controls.Add(Me.chkFullName)
        Me.Controls.Add(Me.chkEmail)
        Me.Controls.Add(Me.chkLastname)
        Me.Controls.Add(Me.chkFirstname)
        Me.Controls.Add(Me.chkUpLow)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.btnGo)
        Me.Controls.Add(Me.dtgridExport)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmUsers"
        Me.Text = "User Management"
        CType(Me.dtgridExport, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents dtgridExport As DataGridView
    Friend WithEvents btnGo As Button
    Friend WithEvents txtName As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents chkUpLow As CheckBox
    Friend WithEvents chkFirstname As CheckBox
    Friend WithEvents chkLastname As CheckBox
    Friend WithEvents chkEmail As CheckBox
    Friend WithEvents chkFullName As CheckBox
    Friend WithEvents chkLastLogin As CheckBox
    Friend WithEvents chkRoles As CheckBox
    Friend WithEvents btnDisable As Button
    Friend WithEvents btnDelete As Button
    Friend WithEvents chkEnabled As CheckBox
    Friend WithEvents btnEnable As Button
End Class
