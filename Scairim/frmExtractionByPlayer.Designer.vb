﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmExtractionByPlayer
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.btnEstrai = New System.Windows.Forms.Button()
        Me.chkPlayerID = New System.Windows.Forms.CheckBox()
        Me.chkChannel = New System.Windows.Forms.CheckBox()
        Me.chkStatus = New System.Windows.Forms.CheckBox()
        Me.chkLastHearthbeat = New System.Windows.Forms.CheckBox()
        Me.chkFrames = New System.Windows.Forms.CheckBox()
        Me.dtgridExport = New System.Windows.Forms.DataGridView()
        Me.chkFrameset = New System.Windows.Forms.CheckBox()
        Me.chkFramesWithRes = New System.Windows.Forms.CheckBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtPlayerName = New System.Windows.Forms.TextBox()
        Me.chkDescription = New System.Windows.Forms.CheckBox()
        Me.chkUUID = New System.Windows.Forms.CheckBox()
        Me.chkMAC = New System.Windows.Forms.CheckBox()
        Me.chkVersion = New System.Windows.Forms.CheckBox()
        Me.chkGroups = New System.Windows.Forms.CheckBox()
        Me.chkAll = New System.Windows.Forms.CheckBox()
        Me.btnExportExcel = New System.Windows.Forms.Button()
        Me.chkPlayerSerial = New System.Windows.Forms.CheckBox()
        Me.chkMonitorSerial = New System.Windows.Forms.CheckBox()
        Me.chkUpLow = New System.Windows.Forms.CheckBox()
        Me.chkIP = New System.Windows.Forms.CheckBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.btnRmv = New System.Windows.Forms.Button()
        Me.lbSelectedMetadata = New System.Windows.Forms.ListBox()
        Me.lbAvailableMetadata = New System.Windows.Forms.ListBox()
        Me.chkPlayerEnabled = New System.Windows.Forms.CheckBox()
        Me.txtLimit = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtOffset = New System.Windows.Forms.TextBox()
        CType(Me.dtgridExport, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnEstrai
        '
        Me.btnEstrai.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnEstrai.Location = New System.Drawing.Point(578, 756)
        Me.btnEstrai.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnEstrai.Name = "btnEstrai"
        Me.btnEstrai.Size = New System.Drawing.Size(108, 34)
        Me.btnEstrai.TabIndex = 0
        Me.btnEstrai.Text = "Do it!"
        Me.btnEstrai.UseVisualStyleBackColor = True
        '
        'chkPlayerID
        '
        Me.chkPlayerID.AutoSize = True
        Me.chkPlayerID.Location = New System.Drawing.Point(12, 34)
        Me.chkPlayerID.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkPlayerID.Name = "chkPlayerID"
        Me.chkPlayerID.Size = New System.Drawing.Size(69, 17)
        Me.chkPlayerID.TabIndex = 2
        Me.chkPlayerID.Text = "Player ID"
        Me.chkPlayerID.UseVisualStyleBackColor = True
        '
        'chkChannel
        '
        Me.chkChannel.AutoSize = True
        Me.chkChannel.Location = New System.Drawing.Point(469, 7)
        Me.chkChannel.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkChannel.Name = "chkChannel"
        Me.chkChannel.Size = New System.Drawing.Size(65, 17)
        Me.chkChannel.TabIndex = 3
        Me.chkChannel.Text = "Channel"
        Me.chkChannel.UseVisualStyleBackColor = True
        '
        'chkStatus
        '
        Me.chkStatus.AutoSize = True
        Me.chkStatus.Location = New System.Drawing.Point(9, 135)
        Me.chkStatus.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkStatus.Name = "chkStatus"
        Me.chkStatus.Size = New System.Drawing.Size(56, 17)
        Me.chkStatus.TabIndex = 4
        Me.chkStatus.Text = "Status"
        Me.chkStatus.UseVisualStyleBackColor = True
        '
        'chkLastHearthbeat
        '
        Me.chkLastHearthbeat.AutoSize = True
        Me.chkLastHearthbeat.Location = New System.Drawing.Point(9, 160)
        Me.chkLastHearthbeat.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkLastHearthbeat.Name = "chkLastHearthbeat"
        Me.chkLastHearthbeat.Size = New System.Drawing.Size(100, 17)
        Me.chkLastHearthbeat.TabIndex = 5
        Me.chkLastHearthbeat.Text = "LastHearthBeat"
        Me.chkLastHearthbeat.UseVisualStyleBackColor = True
        '
        'chkFrames
        '
        Me.chkFrames.AutoSize = True
        Me.chkFrames.Location = New System.Drawing.Point(469, 62)
        Me.chkFrames.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkFrames.Name = "chkFrames"
        Me.chkFrames.Size = New System.Drawing.Size(60, 17)
        Me.chkFrames.TabIndex = 6
        Me.chkFrames.Text = "Frames"
        Me.chkFrames.UseVisualStyleBackColor = True
        '
        'dtgridExport
        '
        Me.dtgridExport.AllowUserToAddRows = False
        Me.dtgridExport.AllowUserToDeleteRows = False
        Me.dtgridExport.AllowUserToOrderColumns = True
        Me.dtgridExport.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dtgridExport.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dtgridExport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgridExport.Location = New System.Drawing.Point(12, 260)
        Me.dtgridExport.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.dtgridExport.Name = "dtgridExport"
        Me.dtgridExport.ReadOnly = True
        Me.dtgridExport.RowHeadersWidth = 51
        Me.dtgridExport.RowTemplate.Height = 24
        Me.dtgridExport.Size = New System.Drawing.Size(1241, 483)
        Me.dtgridExport.TabIndex = 7
        '
        'chkFrameset
        '
        Me.chkFrameset.AutoSize = True
        Me.chkFrameset.Location = New System.Drawing.Point(469, 34)
        Me.chkFrameset.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkFrameset.Name = "chkFrameset"
        Me.chkFrameset.Size = New System.Drawing.Size(122, 17)
        Me.chkFrameset.TabIndex = 8
        Me.chkFrameset.Text = "Frameset Resolution"
        Me.chkFrameset.UseVisualStyleBackColor = True
        '
        'chkFramesWithRes
        '
        Me.chkFramesWithRes.AutoSize = True
        Me.chkFramesWithRes.Location = New System.Drawing.Point(553, 62)
        Me.chkFramesWithRes.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkFramesWithRes.Name = "chkFramesWithRes"
        Me.chkFramesWithRes.Size = New System.Drawing.Size(106, 17)
        Me.chkFramesWithRes.TabIndex = 9
        Me.chkFramesWithRes.Text = "With Resolutions"
        Me.chkFramesWithRes.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(9, 62)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(114, 13)
        Me.Label2.TabIndex = 11
        Me.Label2.Text = "Player Name Contains:"
        '
        'txtPlayerName
        '
        Me.txtPlayerName.Location = New System.Drawing.Point(9, 82)
        Me.txtPlayerName.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtPlayerName.Name = "txtPlayerName"
        Me.txtPlayerName.Size = New System.Drawing.Size(152, 20)
        Me.txtPlayerName.TabIndex = 12
        '
        'chkDescription
        '
        Me.chkDescription.AutoSize = True
        Me.chkDescription.Location = New System.Drawing.Point(9, 110)
        Me.chkDescription.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkDescription.Name = "chkDescription"
        Me.chkDescription.Size = New System.Drawing.Size(111, 17)
        Me.chkDescription.TabIndex = 13
        Me.chkDescription.Text = "Player Description"
        Me.chkDescription.UseVisualStyleBackColor = True
        '
        'chkUUID
        '
        Me.chkUUID.AutoSize = True
        Me.chkUUID.Location = New System.Drawing.Point(297, 7)
        Me.chkUUID.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkUUID.Name = "chkUUID"
        Me.chkUUID.Size = New System.Drawing.Size(78, 17)
        Me.chkUUID.TabIndex = 14
        Me.chkUUID.Text = "Player uuid"
        Me.chkUUID.UseVisualStyleBackColor = True
        '
        'chkMAC
        '
        Me.chkMAC.AutoSize = True
        Me.chkMAC.Location = New System.Drawing.Point(297, 34)
        Me.chkMAC.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkMAC.Name = "chkMAC"
        Me.chkMAC.Size = New System.Drawing.Size(119, 17)
        Me.chkMAC.TabIndex = 15
        Me.chkMAC.Text = "Player mac Address"
        Me.chkMAC.UseVisualStyleBackColor = True
        '
        'chkVersion
        '
        Me.chkVersion.AutoSize = True
        Me.chkVersion.Location = New System.Drawing.Point(297, 62)
        Me.chkVersion.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkVersion.Name = "chkVersion"
        Me.chkVersion.Size = New System.Drawing.Size(93, 17)
        Me.chkVersion.TabIndex = 16
        Me.chkVersion.Text = "Player Version"
        Me.chkVersion.UseVisualStyleBackColor = True
        '
        'chkGroups
        '
        Me.chkGroups.AutoSize = True
        Me.chkGroups.Location = New System.Drawing.Point(297, 89)
        Me.chkGroups.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkGroups.Name = "chkGroups"
        Me.chkGroups.Size = New System.Drawing.Size(92, 17)
        Me.chkGroups.TabIndex = 17
        Me.chkGroups.Text = "Player Groups"
        Me.chkGroups.UseVisualStyleBackColor = True
        '
        'chkAll
        '
        Me.chkAll.AutoSize = True
        Me.chkAll.Location = New System.Drawing.Point(12, 7)
        Me.chkAll.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkAll.Name = "chkAll"
        Me.chkAll.Size = New System.Drawing.Size(100, 17)
        Me.chkAll.TabIndex = 18
        Me.chkAll.Text = "Seleziona Tutto"
        Me.chkAll.UseVisualStyleBackColor = True
        '
        'btnExportExcel
        '
        Me.btnExportExcel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnExportExcel.Location = New System.Drawing.Point(9, 756)
        Me.btnExportExcel.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnExportExcel.Name = "btnExportExcel"
        Me.btnExportExcel.Size = New System.Drawing.Size(141, 34)
        Me.btnExportExcel.TabIndex = 19
        Me.btnExportExcel.Text = "Export in Excel"
        Me.btnExportExcel.UseVisualStyleBackColor = True
        Me.btnExportExcel.Visible = False
        '
        'chkPlayerSerial
        '
        Me.chkPlayerSerial.AutoSize = True
        Me.chkPlayerSerial.Location = New System.Drawing.Point(297, 142)
        Me.chkPlayerSerial.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkPlayerSerial.Name = "chkPlayerSerial"
        Me.chkPlayerSerial.Size = New System.Drawing.Size(122, 17)
        Me.chkPlayerSerial.TabIndex = 20
        Me.chkPlayerSerial.Text = "Player Serial number"
        Me.chkPlayerSerial.UseVisualStyleBackColor = True
        '
        'chkMonitorSerial
        '
        Me.chkMonitorSerial.AutoSize = True
        Me.chkMonitorSerial.Location = New System.Drawing.Point(297, 167)
        Me.chkMonitorSerial.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkMonitorSerial.Name = "chkMonitorSerial"
        Me.chkMonitorSerial.Size = New System.Drawing.Size(130, 17)
        Me.chkMonitorSerial.TabIndex = 21
        Me.chkMonitorSerial.Text = "Monitor Serial Number"
        Me.chkMonitorSerial.UseVisualStyleBackColor = True
        '
        'chkUpLow
        '
        Me.chkUpLow.AutoSize = True
        Me.chkUpLow.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkUpLow.Location = New System.Drawing.Point(167, 84)
        Me.chkUpLow.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkUpLow.Name = "chkUpLow"
        Me.chkUpLow.Size = New System.Drawing.Size(96, 17)
        Me.chkUpLow.TabIndex = 24
        Me.chkUpLow.Text = "Case Sensitive"
        Me.chkUpLow.UseVisualStyleBackColor = True
        '
        'chkIP
        '
        Me.chkIP.AutoSize = True
        Me.chkIP.Location = New System.Drawing.Point(297, 116)
        Me.chkIP.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkIP.Name = "chkIP"
        Me.chkIP.Size = New System.Drawing.Size(68, 17)
        Me.chkIP.TabIndex = 26
        Me.chkIP.Text = "Player IP"
        Me.chkIP.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.btnAdd)
        Me.GroupBox1.Controls.Add(Me.btnRmv)
        Me.GroupBox1.Controls.Add(Me.lbSelectedMetadata)
        Me.GroupBox1.Controls.Add(Me.lbAvailableMetadata)
        Me.GroupBox1.Location = New System.Drawing.Point(701, 7)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Size = New System.Drawing.Size(548, 246)
        Me.GroupBox1.TabIndex = 27
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Metadata"
        '
        'btnAdd
        '
        Me.btnAdd.Location = New System.Drawing.Point(259, 92)
        Me.btnAdd.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(36, 33)
        Me.btnAdd.TabIndex = 12
        Me.btnAdd.Text = ">>"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'btnRmv
        '
        Me.btnRmv.Location = New System.Drawing.Point(259, 153)
        Me.btnRmv.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnRmv.Name = "btnRmv"
        Me.btnRmv.Size = New System.Drawing.Size(36, 33)
        Me.btnRmv.TabIndex = 13
        Me.btnRmv.Text = "<<"
        Me.btnRmv.UseVisualStyleBackColor = True
        '
        'lbSelectedMetadata
        '
        Me.lbSelectedMetadata.FormattingEnabled = True
        Me.lbSelectedMetadata.Location = New System.Drawing.Point(321, 39)
        Me.lbSelectedMetadata.Margin = New System.Windows.Forms.Padding(4)
        Me.lbSelectedMetadata.Name = "lbSelectedMetadata"
        Me.lbSelectedMetadata.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
        Me.lbSelectedMetadata.Size = New System.Drawing.Size(217, 186)
        Me.lbSelectedMetadata.Sorted = True
        Me.lbSelectedMetadata.TabIndex = 1
        '
        'lbAvailableMetadata
        '
        Me.lbAvailableMetadata.FormattingEnabled = True
        Me.lbAvailableMetadata.Location = New System.Drawing.Point(8, 39)
        Me.lbAvailableMetadata.Margin = New System.Windows.Forms.Padding(4)
        Me.lbAvailableMetadata.Name = "lbAvailableMetadata"
        Me.lbAvailableMetadata.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
        Me.lbAvailableMetadata.Size = New System.Drawing.Size(223, 186)
        Me.lbAvailableMetadata.Sorted = True
        Me.lbAvailableMetadata.TabIndex = 0
        '
        'chkPlayerEnabled
        '
        Me.chkPlayerEnabled.AutoSize = True
        Me.chkPlayerEnabled.Location = New System.Drawing.Point(9, 190)
        Me.chkPlayerEnabled.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkPlayerEnabled.Name = "chkPlayerEnabled"
        Me.chkPlayerEnabled.Size = New System.Drawing.Size(97, 17)
        Me.chkPlayerEnabled.TabIndex = 28
        Me.chkPlayerEnabled.Text = "Player Enabled"
        Me.chkPlayerEnabled.UseVisualStyleBackColor = True
        '
        'txtLimit
        '
        Me.txtLimit.Location = New System.Drawing.Point(469, 116)
        Me.txtLimit.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtLimit.Name = "txtLimit"
        Me.txtLimit.Size = New System.Drawing.Size(152, 20)
        Me.txtLimit.TabIndex = 29
        Me.txtLimit.Text = "9999"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(466, 101)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(166, 13)
        Me.Label1.TabIndex = 30
        Me.Label1.Text = "Number of Players: (9999 Default)"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(466, 143)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(122, 13)
        Me.Label3.TabIndex = 32
        Me.Label3.Text = "Player Offset: (0 Default)"
        '
        'txtOffset
        '
        Me.txtOffset.Location = New System.Drawing.Point(469, 158)
        Me.txtOffset.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtOffset.Name = "txtOffset"
        Me.txtOffset.Size = New System.Drawing.Size(152, 20)
        Me.txtOffset.TabIndex = 31
        Me.txtOffset.Text = "0"
        '
        'frmExtractionByPlayer
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1267, 801)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtOffset)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtLimit)
        Me.Controls.Add(Me.chkPlayerEnabled)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.chkIP)
        Me.Controls.Add(Me.chkUpLow)
        Me.Controls.Add(Me.chkMonitorSerial)
        Me.Controls.Add(Me.chkPlayerSerial)
        Me.Controls.Add(Me.btnExportExcel)
        Me.Controls.Add(Me.chkAll)
        Me.Controls.Add(Me.chkGroups)
        Me.Controls.Add(Me.chkVersion)
        Me.Controls.Add(Me.chkMAC)
        Me.Controls.Add(Me.chkUUID)
        Me.Controls.Add(Me.chkDescription)
        Me.Controls.Add(Me.txtPlayerName)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.chkFramesWithRes)
        Me.Controls.Add(Me.chkFrameset)
        Me.Controls.Add(Me.dtgridExport)
        Me.Controls.Add(Me.chkFrames)
        Me.Controls.Add(Me.chkLastHearthbeat)
        Me.Controls.Add(Me.chkStatus)
        Me.Controls.Add(Me.chkChannel)
        Me.Controls.Add(Me.chkPlayerID)
        Me.Controls.Add(Me.btnEstrai)
        Me.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.MinimumSize = New System.Drawing.Size(1283, 840)
        Me.Name = "frmExtractionByPlayer"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Extraction By Player"
        CType(Me.dtgridExport, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnEstrai As Button
    Friend WithEvents chkPlayerID As CheckBox
    Friend WithEvents chkChannel As CheckBox
    Friend WithEvents chkStatus As CheckBox
    Friend WithEvents chkLastHearthbeat As CheckBox
    Friend WithEvents chkFrames As CheckBox
    Friend WithEvents chkFrameset As CheckBox
    Friend WithEvents chkFramesWithRes As CheckBox
    Friend WithEvents Label2 As Label
    Friend WithEvents txtPlayerName As TextBox
    Friend WithEvents chkDescription As CheckBox
    Friend WithEvents chkUUID As CheckBox
    Friend WithEvents chkMAC As CheckBox
    Friend WithEvents chkVersion As CheckBox
    Friend WithEvents chkGroups As CheckBox
    Friend WithEvents chkAll As CheckBox
    Friend WithEvents btnExportExcel As Button
    Friend WithEvents chkPlayerSerial As CheckBox
    Friend WithEvents chkMonitorSerial As CheckBox
    Friend WithEvents chkUpLow As CheckBox
    Friend WithEvents chkIP As CheckBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents lbSelectedMetadata As ListBox
    Friend WithEvents lbAvailableMetadata As ListBox
    Friend WithEvents btnAdd As Button
    Friend WithEvents btnRmv As Button
    Private WithEvents dtgridExport As DataGridView
    Friend WithEvents chkPlayerEnabled As CheckBox
    Friend WithEvents txtLimit As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents txtOffset As TextBox
End Class
