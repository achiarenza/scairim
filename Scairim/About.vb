﻿Public Class About
    Private Sub About_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Icon = Scairim.Icon
        If My.Application.IsNetworkDeployed Then
            lblVersion.Text = "ver. " & My.Application.Deployment.CurrentVersion.ToString
        Else
            lblVersion.Text = "ver. Not Deployed"
        End If
        txtRelease.Text = My.Resources.ReleaseNotes_Scairim
    End Sub
End Class