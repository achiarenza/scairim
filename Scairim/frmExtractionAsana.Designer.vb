﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmExtractionAsana
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtBearer = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmbProjects = New System.Windows.Forms.ComboBox()
        Me.label3 = New System.Windows.Forms.Label()
        Me.btnLoginAsana = New System.Windows.Forms.Button()
        Me.chkRemember = New System.Windows.Forms.CheckBox()
        Me.dtgridExport = New System.Windows.Forms.DataGridView()
        Me.btnGo = New System.Windows.Forms.Button()
        Me.btnExportExcel = New System.Windows.Forms.Button()
        CType(Me.dtgridExport, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtBearer
        '
        Me.txtBearer.Location = New System.Drawing.Point(12, 31)
        Me.txtBearer.Name = "txtBearer"
        Me.txtBearer.Size = New System.Drawing.Size(228, 22)
        Me.txtBearer.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(95, 17)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Bearer Token"
        '
        'cmbProjects
        '
        Me.cmbProjects.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cmbProjects.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbProjects.FormattingEnabled = True
        Me.cmbProjects.Location = New System.Drawing.Point(12, 97)
        Me.cmbProjects.Name = "cmbProjects"
        Me.cmbProjects.Size = New System.Drawing.Size(328, 24)
        Me.cmbProjects.Sorted = True
        Me.cmbProjects.TabIndex = 2
        '
        'label3
        '
        Me.label3.AutoSize = True
        Me.label3.Location = New System.Drawing.Point(13, 77)
        Me.label3.Name = "label3"
        Me.label3.Size = New System.Drawing.Size(52, 17)
        Me.label3.TabIndex = 3
        Me.label3.Text = "Project"
        '
        'btnLoginAsana
        '
        Me.btnLoginAsana.Location = New System.Drawing.Point(265, 25)
        Me.btnLoginAsana.Name = "btnLoginAsana"
        Me.btnLoginAsana.Size = New System.Drawing.Size(75, 34)
        Me.btnLoginAsana.TabIndex = 4
        Me.btnLoginAsana.Text = "Login"
        Me.btnLoginAsana.UseVisualStyleBackColor = True
        '
        'chkRemember
        '
        Me.chkRemember.AutoSize = True
        Me.chkRemember.Checked = True
        Me.chkRemember.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkRemember.Location = New System.Drawing.Point(368, 31)
        Me.chkRemember.Name = "chkRemember"
        Me.chkRemember.Size = New System.Drawing.Size(130, 21)
        Me.chkRemember.TabIndex = 5
        Me.chkRemember.Text = "Save the Token"
        Me.chkRemember.UseVisualStyleBackColor = True
        '
        'dtgridExport
        '
        Me.dtgridExport.AllowUserToAddRows = False
        Me.dtgridExport.AllowUserToDeleteRows = False
        Me.dtgridExport.AllowUserToOrderColumns = True
        Me.dtgridExport.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dtgridExport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgridExport.Location = New System.Drawing.Point(12, 146)
        Me.dtgridExport.Name = "dtgridExport"
        Me.dtgridExport.ReadOnly = True
        Me.dtgridExport.RowTemplate.Height = 24
        Me.dtgridExport.Size = New System.Drawing.Size(894, 357)
        Me.dtgridExport.TabIndex = 6
        '
        'btnGo
        '
        Me.btnGo.Location = New System.Drawing.Point(423, 509)
        Me.btnGo.Name = "btnGo"
        Me.btnGo.Size = New System.Drawing.Size(75, 34)
        Me.btnGo.TabIndex = 7
        Me.btnGo.Text = "Go!"
        Me.btnGo.UseVisualStyleBackColor = True
        '
        'btnExportExcel
        '
        Me.btnExportExcel.Location = New System.Drawing.Point(16, 509)
        Me.btnExportExcel.Name = "btnExportExcel"
        Me.btnExportExcel.Size = New System.Drawing.Size(111, 34)
        Me.btnExportExcel.TabIndex = 8
        Me.btnExportExcel.Text = "Export in Excel"
        Me.btnExportExcel.UseVisualStyleBackColor = True
        Me.btnExportExcel.Visible = False
        '
        'frmExtractionAsana
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(918, 555)
        Me.Controls.Add(Me.btnExportExcel)
        Me.Controls.Add(Me.btnGo)
        Me.Controls.Add(Me.dtgridExport)
        Me.Controls.Add(Me.chkRemember)
        Me.Controls.Add(Me.btnLoginAsana)
        Me.Controls.Add(Me.label3)
        Me.Controls.Add(Me.cmbProjects)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtBearer)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "frmExtractionAsana"
        Me.Text = "Estraction on Asana"
        CType(Me.dtgridExport, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txtBearer As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents label3 As Label
    Friend WithEvents btnLoginAsana As Button
    Friend WithEvents chkRemember As CheckBox
    Friend WithEvents cmbProjects As ComboBox
    Friend WithEvents dtgridExport As DataGridView
    Friend WithEvents btnGo As Button
    Friend WithEvents btnExportExcel As Button
End Class
