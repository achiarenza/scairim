﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCreatePlayer
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmCreatePlayer))
        Me.grpBox1 = New System.Windows.Forms.GroupBox()
        Me.rdbIntesaSanPaolo = New System.Windows.Forms.RadioButton()
        Me.rdbAnything = New System.Windows.Forms.RadioButton()
        Me.rdbGenericMulti = New System.Windows.Forms.RadioButton()
        Me.rdbGenericSingle = New System.Windows.Forms.RadioButton()
        Me.txtFile = New System.Windows.Forms.TextBox()
        Me.lblInfo1 = New System.Windows.Forms.Label()
        Me.btnFile = New System.Windows.Forms.Button()
        Me.btnGo = New System.Windows.Forms.Button()
        Me.grpBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'grpBox1
        '
        Me.grpBox1.Controls.Add(Me.rdbIntesaSanPaolo)
        Me.grpBox1.Controls.Add(Me.rdbAnything)
        Me.grpBox1.Controls.Add(Me.rdbGenericMulti)
        Me.grpBox1.Controls.Add(Me.rdbGenericSingle)
        Me.grpBox1.Location = New System.Drawing.Point(12, 12)
        Me.grpBox1.Name = "grpBox1"
        Me.grpBox1.Size = New System.Drawing.Size(235, 182)
        Me.grpBox1.TabIndex = 0
        Me.grpBox1.TabStop = False
        Me.grpBox1.Text = "Type of Creation"
        '
        'rdbIntesaSanPaolo
        '
        Me.rdbIntesaSanPaolo.AutoSize = True
        Me.rdbIntesaSanPaolo.Location = New System.Drawing.Point(9, 151)
        Me.rdbIntesaSanPaolo.Name = "rdbIntesaSanPaolo"
        Me.rdbIntesaSanPaolo.Size = New System.Drawing.Size(136, 21)
        Me.rdbIntesaSanPaolo.TabIndex = 3
        Me.rdbIntesaSanPaolo.TabStop = True
        Me.rdbIntesaSanPaolo.Text = "Intesa San Paolo"
        Me.rdbIntesaSanPaolo.UseVisualStyleBackColor = True
        '
        'rdbAnything
        '
        Me.rdbAnything.AutoSize = True
        Me.rdbAnything.Location = New System.Drawing.Point(9, 113)
        Me.rdbAnything.Name = "rdbAnything"
        Me.rdbAnything.Size = New System.Drawing.Size(176, 21)
        Me.rdbAnything.TabIndex = 2
        Me.rdbAnything.TabStop = True
        Me.rdbAnything.Text = "Anything (Not Working)"
        Me.rdbAnything.UseVisualStyleBackColor = True
        '
        'rdbGenericMulti
        '
        Me.rdbGenericMulti.AutoSize = True
        Me.rdbGenericMulti.Location = New System.Drawing.Point(9, 73)
        Me.rdbGenericMulti.Name = "rdbGenericMulti"
        Me.rdbGenericMulti.Size = New System.Drawing.Size(223, 21)
        Me.rdbGenericMulti.TabIndex = 1
        Me.rdbGenericMulti.TabStop = True
        Me.rdbGenericMulti.Text = "Generic Multiple (Not Working)"
        Me.rdbGenericMulti.UseVisualStyleBackColor = True
        '
        'rdbGenericSingle
        '
        Me.rdbGenericSingle.AutoSize = True
        Me.rdbGenericSingle.Location = New System.Drawing.Point(9, 33)
        Me.rdbGenericSingle.Name = "rdbGenericSingle"
        Me.rdbGenericSingle.Size = New System.Drawing.Size(214, 21)
        Me.rdbGenericSingle.TabIndex = 0
        Me.rdbGenericSingle.TabStop = True
        Me.rdbGenericSingle.Text = "Generic Single (Not Working)"
        Me.rdbGenericSingle.UseVisualStyleBackColor = True
        '
        'txtFile
        '
        Me.txtFile.Location = New System.Drawing.Point(12, 228)
        Me.txtFile.Name = "txtFile"
        Me.txtFile.Size = New System.Drawing.Size(199, 22)
        Me.txtFile.TabIndex = 1
        '
        'lblInfo1
        '
        Me.lblInfo1.AutoSize = True
        Me.lblInfo1.Location = New System.Drawing.Point(12, 208)
        Me.lblInfo1.Name = "lblInfo1"
        Me.lblInfo1.Size = New System.Drawing.Size(73, 17)
        Me.lblInfo1.TabIndex = 2
        Me.lblInfo1.Text = "File to use"
        '
        'btnFile
        '
        Me.btnFile.Location = New System.Drawing.Point(217, 225)
        Me.btnFile.Name = "btnFile"
        Me.btnFile.Size = New System.Drawing.Size(30, 28)
        Me.btnFile.TabIndex = 3
        Me.btnFile.Text = "..."
        Me.btnFile.UseVisualStyleBackColor = True
        '
        'btnGo
        '
        Me.btnGo.Location = New System.Drawing.Point(85, 265)
        Me.btnGo.Name = "btnGo"
        Me.btnGo.Size = New System.Drawing.Size(96, 39)
        Me.btnGo.TabIndex = 4
        Me.btnGo.Text = "FusRohDah!"
        Me.btnGo.UseVisualStyleBackColor = True
        '
        'FrmCreatePlayer
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(259, 316)
        Me.Controls.Add(Me.btnGo)
        Me.Controls.Add(Me.btnFile)
        Me.Controls.Add(Me.lblInfo1)
        Me.Controls.Add(Me.txtFile)
        Me.Controls.Add(Me.grpBox1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FrmCreatePlayer"
        Me.Text = "Player Creation"
        Me.grpBox1.ResumeLayout(False)
        Me.grpBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents grpBox1 As GroupBox
    Friend WithEvents rdbIntesaSanPaolo As RadioButton
    Friend WithEvents rdbAnything As RadioButton
    Friend WithEvents rdbGenericMulti As RadioButton
    Friend WithEvents rdbGenericSingle As RadioButton
    Friend WithEvents txtFile As TextBox
    Friend WithEvents lblInfo1 As Label
    Friend WithEvents btnFile As Button
    Friend WithEvents btnGo As Button
End Class
