﻿Imports Newtonsoft.Json.Linq
Public Class frmUpdateTV

    Dim result As String
    Dim Json, Json2 As JObject
    Dim JsonW As JObject

    Private Sub frmUpdateTV_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Icon = Scairim.Icon
        txtPlayers.Text = ""
        txtReplace.Text = ""
        txtWith.Text = ""
        txtPrefix.Text = ""
    End Sub

    Private Sub btnGo_Click(sender As Object, e As EventArgs) Handles btnGo.Click
        Utilities.timeout = 120000
        Logga("Aggiorno i player!!!")
        Try
            Loading("show")
            result = UpdatePlayers()
            Loading("close")
        Catch ex As Exception
            Logga("ERROR - frmUpdateTV: " & ex.ToString)
        End Try
        JsonW = Nothing
        Logga(result)
        MsgBox(result)
    End Sub

    Private Function UpdatePlayers()
        Dim res As String = ""
        Dim check As String
        Dim NumChild As Integer = txtPlayers.Lines.Count
        Dim Contatore As Integer
        Dim temp As String = ""
        Dim GroupString As New List(Of String)
        Dim MetadataString As New List(Of String)
        Dim WasError As Boolean = False
        Dim tempToken As JToken

        check = GetStuffsBearer(Scairim.tokenTeamviewer, "https://webapi.teamviewer.com/api/v1/devices")
        If InStr(check, "(401)") > 0 Then
            UpdatePlayers = "Not authorized, please check your token"
            Exit Function
        End If
        Json = JObject.Parse(check)

        Contatore = 0
        WasError = False
        For Each playerName In txtPlayers.Lines
            If Contatore Mod 10 = 0 Then
                Loading(CInt((Contatore * 100) / NumChild))
            End If
            tempToken = Nothing
            tempToken = Json.SelectToken("$.devices[?(@.alias == '" & playerName & "')]")
            If tempToken Is Nothing Then
                Logga("Player Not Found: " & playerName)
                Contatore += 1
                Continue For
            End If
            temp = ""
            If txtReplace.Text <> "" Then
                temp = Replace(ReplaceName(playerName), txtReplace.Text, txtWith.Text)
                res = UpdatePlayerNameTV(Scairim.tokenTeamviewer, tempToken.Item("device_id").ToString, temp)
                If InStr(res.ToUpper, "ERROR") > 0 Then
                    Logga("Impossible to update: " & playerName & " - " & res)
                    WasError = True
                Else
                    Logga("Updated: " & playerName & " to: " & temp)
                End If
            End If
            If txtPrefix.Text <> "" Then
                temp = txtPrefix.Text & playerName
                res = UpdatePlayerNameTV(Scairim.tokenTeamviewer, tempToken.Item("device_id").ToString, temp)
                If InStr(res.ToUpper, "ERROR") > 0 Then
                    Logga("Impossible to update: " & playerName & " - " & res)
                    WasError = True
                Else
                    Logga("Updated: " & playerName & " to: " & temp)
                End If
            End If
            Contatore += 1
        Next
        If WasError = True Then
            res = "WARNING! Please check logs..."
        Else
            res = "Done!"
        End If
        UpdatePlayers = res
    End Function

End Class