﻿Imports Newtonsoft.Json.Linq
Public Class Scairim

    Public SpecialPermission As String = "a.chiarenza,m.pacucci,m.restelli,a.caprioli"
    Public Username
    Public Password
    Public LinkCM
    Public apiToken
    Public token
    Public logged As Boolean
    Public wait As Form
    Public tokenTeamviewer
    Dim oJson As JObject

    Private Sub Scairim_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If My.Application.IsNetworkDeployed Then
            Me.Text = Me.Text & " " & My.Application.Deployment.CurrentVersion.ToString
            Logga("Benvenuto in Scairim " & My.Application.Deployment.CurrentVersion.ToString)
        Else
            Logga("Benvenuto in Scairim")
        End If
        logged = False
        If IO.File.Exists(IO.Path.Combine(IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Scairim"), "temp.txt")) Then
            Loading("show")
            For Each line In IO.File.ReadAllLines(IO.Path.Combine(IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Scairim"), "temp.txt"))
                ScriviInJson("temp.json", Split(line, "=")(0), Split(line, "=")(1))
            Next
            If IO.File.Exists(IO.Path.Combine(IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Scairim"), "history_CM.txt")) Then
                For Each line In IO.File.ReadAllLines(IO.Path.Combine(IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Scairim"), "history_CM.txt"))
                    ScriviInJson("temp.json", "CM", line)
                Next
            End If
            IO.File.Delete(IO.Path.Combine(IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Scairim"), "temp.txt"))
            IO.File.Delete(IO.Path.Combine(IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Scairim"), "history_CM.txt"))
            Loading("close")
            Logga("Ho finito! Ho radunato in un unico file le informazioni di accesso ai CM di Scala")
        End If
        Login.ShowDialog()
    End Sub

    Private Sub PlayerToolStripMenuItem_Click(sender As Object, e As EventArgs)
        Logga("Feature ancora non implementata. Very very sorry :'(")
        'FrmCreatePlayer.Show()
    End Sub

    Private Sub SetPlayerMetadataToolStripMenuItem_Click(sender As Object, e As EventArgs)
        Logga("Feature ancora non implementata. Very very sorry :'(")
    End Sub

    Private Sub ByChannelToolStripMenuItem_Click(sender As Object, e As EventArgs)
        Logga("Feature ancora non implementata. Very very sorry :'(")
    End Sub

    Private Sub ByPlaylistsToolStripMenuItem_Click(sender As Object, e As EventArgs)
        Logga("Feature ancora non implementata. Very very sorry :'(")
    End Sub

    Private Sub ByMetadataToolStripMenuItem_Click(sender As Object, e As EventArgs)
        Logga("Feature ancora non implementata. Very very sorry :'(")
    End Sub

    Private Sub ByMetadataToolStripMenuItem1_Click(sender As Object, e As EventArgs)
        Logga("Disabilitata perché è un pò bugga...")
        'frmExtractionByMetadata.Show()
    End Sub

    Private Sub AboutToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AboutToolStripMenuItem.Click
        About.Show()
    End Sub

    Private Sub ToolStripMenuItem1_Click(sender As Object, e As EventArgs)
        If logged = True And InStr(SpecialPermission, Username) > 0 Then
            Logga("Hai i permessi per accedere alla pulizia del CM.")
            frmDeleteOldStuffs.Show()
        Else
            Logga("Non hai i permessi per farlo.")
        End If
    End Sub

    Private Sub OpenToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles OpenToolStripMenuItem.Click
        frmWait.Show()
    End Sub

    Private Sub CloseToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CloseToolStripMenuItem.Click
        frmWait.Close()
    End Sub

    Private Sub ToolStripMenuItem3_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem3.Click
        frmDebug.Show()
    End Sub

    Private Sub AsanaToolStripMenuItem_Click_1(sender As Object, e As EventArgs) Handles AsanaToolStripMenuItem.Click
        frmExtractionAsana.Show()
    End Sub

    Private Sub ToolStripMenuItem5_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem5.Click
        Logga("Un attimo che carico la lista dei metadati...")
        frmExtractionByPlayer.Show()
        Logga("Fatto! :D")
    End Sub

    Private Sub ToolStripMenuItem7_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem7.Click
        frmExtractionByPlaylists.Show()
    End Sub

    Private Sub ToolStripMenuItem8_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem8.Click
        If logged = False Then
            Logga("Per favore esegui il login prima di utilizzare questa funzione.")
        Else
            Logga("Un attimo che carico un pò di liste... (Metadati, gruppi, schedulazioni... Uff un pò di roba...)")
            frmUpdatePlayers.Show()
            Logga("Fatto! Enjoy!")
        End If
    End Sub

    Private Sub ToolStripMenuItem9_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem9.Click
        frmUpdateMetadata.Show()
    End Sub

    Private Sub ToolStripMenuItem1_Click_1(sender As Object, e As EventArgs) Handles ToolStripMenuItem1.Click
        If logged = True And InStr(SpecialPermission, Username) > 0 Then
            Logga("Hai i permessi per accedere alla gestione utenti sul CM.")
            frmUsers.Show()
        Else
            Logga("Non hai i permessi per farlo.")
        End If
    End Sub

    Private Sub ToolStripMenuItem2_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem2.Click
        AskCM.Show()
    End Sub

    Private Sub ToolStripMenuItem10_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem10.Click
        Login.ShowDialog()
    End Sub

    Private Sub ToolStripMenuItem11_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem11.Click
        If logged = True Then
            Logga("Logout: " & GetLogout(token, apiToken, LinkCM))
            logged = False
        Else
            Logga("Non sei loggato.")
        End If
    End Sub

    Private Sub LoginToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles LoginToolStripMenuItem1.Click
        frmLoginTV.Show()
    End Sub

    Private Sub ExtractionToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles ExtractionToolStripMenuItem1.Click
        If tokenTeamviewer = "" Then
            Dim file As String = IO.Path.Combine(IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Scairim"), "temp.json")
            If IO.File.Exists(file) Then
                oJson = JObject.Parse(IO.File.ReadAllText(file))
                If oJson.ContainsKey("TokenTV") Then
                    tokenTeamviewer = oJson.Item("TokenTV").ToString
                End If
            End If
        End If
        If tokenTeamviewer <> "" Then
            frmExtractionTV.Show()
        Else
            Logga("Please Login.")
        End If
    End Sub

    Private Sub UpdateToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles UpdateToolStripMenuItem.Click
        If tokenTeamviewer = "" Then
            Dim file As String = IO.Path.Combine(IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Scairim"), "temp.json")
            If IO.File.Exists(file) Then
                oJson = JObject.Parse(IO.File.ReadAllText(file))
                If oJson.ContainsKey("TokenTV") Then
                    tokenTeamviewer = oJson.Item("TokenTV").ToString
                End If
            End If
        End If
        If tokenTeamviewer <> "" Then
            frmUpdateTV.Show()
        Else
            Logga("Please Login.")
        End If
    End Sub

    Private Sub ChannelTimeTriggerToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ChannelTimeTriggerToolStripMenuItem.Click
        If logged = True And InStr(SpecialPermission, Username) > 0 Then
            Logga("Hai i permessi per accedere alla gestione dei time trigger sul CM.")
            frmUpdateChannel.Show()
        Else
            Logga("Non hai i permessi per farlo.")
        End If
    End Sub
End Class
