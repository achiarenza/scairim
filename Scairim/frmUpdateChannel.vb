﻿Imports Newtonsoft.Json.Linq

Public Class frmUpdateChannel
    Private Sub frmUpdateChannel_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Icon = Scairim.Icon
        txtChannelList.Text = ""
        txtOrario.Text = ""
    End Sub

    Private Sub btnGo_Click(sender As Object, e As EventArgs) Handles btnGo.Click
        Dim result As String = ""
        If (Scairim.logged) Then
            Utilities.timeout = 120000
            Logga("Aggiorno i Canali...")
            Try
                Loading("show")
                result = UpdateTimeTriggers()
                Loading("close")
            Catch ex As Exception
                Logga("ERROR - Aggiornamento Canali: " & ex.ToString)
            End Try
            Logga(result)
            MsgBox(result)
        Else
            Logga("Se prima non fai il login su un CM non posso fare nulla :)")
        End If
    End Sub

    Function UpdateTimeTriggers()
        Dim oJsonR, oJsonW As JObject
        Dim TriggerArray As JArray
        Dim trovato As Boolean
        Dim dic_channel_id As Dictionary(Of String, String) = New Dictionary(Of String, String)
        Dim dic_channel_frame As Dictionary(Of String, List(Of String)) = New Dictionary(Of String, List(Of String))
        Dim query As String = "channels?limit=9999&fields=name,frameset"
        Dim result = GetStuffs(Scairim.token, Scairim.apiToken, Scairim.LinkCM & "/api/rest/" & query)
        If InStr(result, "ERROR") > 0 Then
            UpdateTimeTriggers = "ERROR - UpdateTimeTriggers: Lista Channel. Controlla di aver fatto il Login correttamente."
            Exit Function
        End If
        oJsonR = JObject.Parse(result)
        If oJsonR.ContainsKey("list") Then
            For Each ch In oJsonR.Item("list").Children
                If ch.Item("frameset") Is Nothing Then
                    Continue For
                End If
                If ch.Item("frameset").Item("frames") Is Nothing Then
                    Continue For
                End If
                trovato = False
                For Each line In txtChannelList.Lines
                    If line = ch.Item("name").ToString Then
                        trovato = True
                    End If
                Next
                If trovato = False Then
                    Continue For
                End If
                trovato = False
                For Each frame In ch.Item("frameset").Item("frames").Children
                    If frame.Item("timeTriggersCount").ToString = "1" Then
                        Logga(frame.Item("timeTriggersCount").ToString & " - " & ch.Item("name").ToString & " - " & frame.Item("id").ToString)
                        If dic_channel_frame.ContainsKey(ch.Item("name").ToString) Then
                            dic_channel_frame(ch.Item("name").ToString).Add(frame.Item("id").ToString)
                        Else
                            dic_channel_frame.Add(ch.Item("name").ToString, New List(Of String))
                            dic_channel_frame(ch.Item("name").ToString).Add(frame.Item("id").ToString)
                        End If
                        trovato = True
                        End If
                Next
                If trovato Then
                    dic_channel_id.Add(ch.Item("name").ToString, ch.Item("id").ToString)
                End If
            Next
        End If
        For Each line In txtChannelList.Lines
            Logga(line)
            If Not dic_channel_frame.ContainsKey(line) Then
                Logga("Frame non trovati sul canale: " & line)
                Continue For
            End If
            For Each frm In dic_channel_frame(line)
                query = "channels/" & dic_channel_id(line) & "/frames/" & frm & "/timetriggers"
                result = GetStuffs(Scairim.token, Scairim.apiToken, Scairim.LinkCM & "/api/rest/" & query)
                If InStr(result, "ERROR") > 0 Then
                    UpdateTimeTriggers = "ERROR - UpdateTimeTriggers: Lista timetriggers. Controlla di aver fatto il Login correttamente."
                    Exit Function
                End If
                If result = "" Then
                    Logga("Timetrigger non trovato sul frame: " & frm & " Canale: " & line)
                    Continue For
                End If
                oJsonR = JObject.Parse(result)
                If Not oJsonR.ContainsKey("timeTriggers") Then
                    Logga("Timetrigger non trovato sul frame: " & frm & " Canale: " & line)
                    Continue For
                End If
                TriggerArray = New JArray()
                For Each timetrigger In oJsonR.Item("timeTriggers").Children
                    TriggerArray.Add(
                        New JObject(
                            New JProperty("id", timetrigger.Item("id").ToString),
                            New JProperty("name", timetrigger.Item("name").ToString),
                            New JProperty("playlist", New JObject(New JProperty("id", timetrigger.Item("playlist").Item("id").ToString))),
                            New JProperty("playFullScreen", timetrigger.Item("playFullScreen").ToString),
                            New JProperty("audioDucking", timetrigger.Item("audioDucking").ToString),
                            New JProperty("controlledByAdManager", Nothing),
                            New JProperty("itemsToPick", timetrigger.Item("itemsToPick").ToString),
                            New JProperty("recurrencePattern", timetrigger.Item("recurrencePattern").ToString),
                            New JProperty("days", timetrigger.Item("days")),
                            New JProperty("startDate", timetrigger.Item("startDate").ToString),
                            New JProperty("endDate", ""),
                            New JProperty("time", txtOrario.Text)))
                Next
                oJsonW = New JObject(
                    New JProperty("frames", New JArray(
                    New JObject(
                        New JProperty("eventTriggers", New JArray()),
                        New JProperty("timeTriggers", TriggerArray),
                        New JProperty("timeslots", New JArray()),
                        New JProperty("alternateType", "NONE"),
                        New JProperty("alternatePlaylist", Nothing),
                        New JProperty("id", frm)))),
                        New JProperty("id", ""))
                query = "channels/" & dic_channel_id(line) & "/schedules"
                Logga("Aggiornando " & line & ": " & UpdateStuffs(Scairim.token, Scairim.apiToken, oJsonW.ToString, Scairim.LinkCM & "/api/rest/" & query))
                'Logga(oJsonW.ToString)
            Next
        Next
        UpdateTimeTriggers = "OK"
    End Function
End Class