﻿Imports Excel = Microsoft.Office.Interop.Excel
Imports Newtonsoft.Json.Linq

Public Class frmExtractionByMetadata

    Dim Json, Json2 As JObject
    Dim result As String

    Private Sub frmExtractionByMetadata_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Icon = Scairim.Icon
        btnEstraiExcel.Visible = False
        dtgridExport.Rows.Clear()
        dtgridExport.Columns.Clear()
    End Sub

    Private Sub btnEstraiExcel_Click(sender As Object, e As EventArgs) Handles btnEstraiExcel.Click
        Dim excelfile As String
        Dim xlApp As Excel.Application = New Microsoft.Office.Interop.Excel.Application()

        If xlApp Is Nothing Then
            Logga("Excel Non è installato nel sistema!!")
            Return
        End If

        Dim savefile As New SaveFileDialog With {
            .InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyComputer),
            .Filter = "excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*",
            .FilterIndex = 1,
            .RestoreDirectory = True,
            .OverwritePrompt = True
        }
        If savefile.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            excelfile = savefile.FileName
        Else
            Return
        End If

        Loading("show")

        Dim xlWorkBook As Excel.Workbook
        xlWorkBook = xlApp.Workbooks.Add()
        Dim xlWorkSheet = xlWorkBook.Sheets(1)

        For Each column In dtgridExport.Columns
            xlWorkSheet.Cells(1, column.Index + 1).Value = column.HeaderText
        Next

        Dim i As Integer = 2
        Dim c As Integer = 1
        Dim Contatore = 0
        For Each row In dtgridExport.Rows
            If Contatore Mod 10 = 0 Then
                Loading(CInt((Contatore * 100) / dtgridExport.Rows.Count))
            End If
            c = 1
            For Each column In dtgridExport.Columns
                If Not dtgridExport.Item(column.Index, row.Index).Value Is Nothing Then
                    xlWorkSheet.Cells(i, c).Value = dtgridExport.Item(column.Index, row.Index).Value.ToString
                End If
                c += 1
            Next
            i += 1
        Next

        xlWorkBook.SaveAs(excelfile)
        xlWorkBook.Close()
        xlApp.Quit()

        ReleaseComObject(xlWorkSheet)
        ReleaseComObject(xlWorkBook)
        ReleaseComObject(xlApp)

        Loading("close")

        Logga("Estrazione Esportata!")
        MsgBox("Estrazione Esportata!")
    End Sub

    Private Sub btnGo_Click(sender As Object, e As EventArgs) Handles btnGo.Click
        If (Scairim.logged) Then
            Utilities.timeout = 120000
            Logga("Faccio un'estrazione!!!")
            Try
                Loading("show")
                result = Estrai()
                Loading("close")
            Catch ex As Exception
                Logga("ERROR - Estrazione: " & ex.ToString)
            End Try
            Logga(result)
            MsgBox(result)
            btnEstraiExcel.Visible = True
        Else
            Logga("Se prima non fai il login su un CM non posso estrarre nulla :)")
        End If
    End Sub

    Private Function Estrai()

        Dim toExtract As String = ""
        Dim RowIndex As Integer
        Dim ColumnCount As Integer = 0
        Dim clIndxPlayerName As Integer
        Dim clIndxMetadato1 As Integer
        Dim clIndxMetadato2 As Integer
        Dim clIndxMetadato3 As Integer
        Dim clIndxMetadato4 As Integer
        Dim clIndxMetadato5 As Integer
        Dim clIndxMetadato6 As Integer
        Dim clIndxMetadato7 As Integer
        Dim clIndxMetadato8 As Integer
        Dim clIndxMetadato9 As Integer
        Dim clIndxMetadato10 As Integer
        Dim Metadati() As String = {"", "", "", "", "", "", "", "", "", ""}
        Dim Valori() As String = {"", "", "", "", "", "", "", "", "", ""}
        Dim temp As String = ""
        Dim temp2 As String = ""
        Dim check As String
        Dim NumChild As Integer
        Dim Contatore As Integer
        Dim Index As Integer = 0
        Dim PlayerOk As Boolean
        Dim Trovato As Boolean

        dtgridExport.Rows.Clear()
        dtgridExport.Columns.Clear()

        dtgridExport.Columns.Add("PlayerName", "Player Name")
        toExtract = AddTextComma(toExtract, "name", False)
        clIndxPlayerName = ColumnCount
        ColumnCount = ColumnCount + 1

        If txtMetadato1.Text <> "" Then
            dtgridExport.Columns.Add("Metadato1", txtMetadato1.Text)
            Metadati(ColumnCount - 1) = txtMetadato1.Text
            Valori(ColumnCount - 1) = txtValore1.Text
            clIndxMetadato1 = ColumnCount
            ColumnCount = ColumnCount + 1
        End If

        If txtMetadato2.Text <> "" Then
            dtgridExport.Columns.Add("Metadato2", txtMetadato2.Text)
            Metadati(ColumnCount - 1) = txtMetadato2.Text
            Valori(ColumnCount - 1) = txtValore2.Text
            clIndxMetadato2 = ColumnCount
            ColumnCount = ColumnCount + 1
        End If

        If txtMetadato3.Text <> "" Then
            dtgridExport.Columns.Add("Metadato3", txtMetadato3.Text)
            Metadati(ColumnCount - 1) = txtMetadato3.Text
            Valori(ColumnCount - 1) = txtValore3.Text
            clIndxMetadato3 = ColumnCount
            ColumnCount = ColumnCount + 1
        End If

        If txtMetadato4.Text <> "" Then
            dtgridExport.Columns.Add("Metadato4", txtMetadato4.Text)
            Metadati(ColumnCount - 1) = txtMetadato4.Text
            Valori(ColumnCount - 1) = txtValore4.Text
            clIndxMetadato4 = ColumnCount
            ColumnCount = ColumnCount + 1
        End If

        If txtMetadato5.Text <> "" Then
            dtgridExport.Columns.Add("Metadato5", txtMetadato5.Text)
            Metadati(ColumnCount - 1) = txtMetadato5.Text
            Valori(ColumnCount - 1) = txtValore5.Text
            clIndxMetadato5 = ColumnCount
            ColumnCount = ColumnCount + 1
        End If

        If txtMetadato6.Text <> "" Then
            dtgridExport.Columns.Add("Metadato6", txtMetadato6.Text)
            Metadati(ColumnCount - 1) = txtMetadato6.Text
            Valori(ColumnCount - 1) = txtValore6.Text
            clIndxMetadato6 = ColumnCount
            ColumnCount = ColumnCount + 1
        End If

        If txtMetadato7.Text <> "" Then
            dtgridExport.Columns.Add("Metadato7", txtMetadato7.Text)
            Metadati(ColumnCount - 1) = txtMetadato7.Text
            Valori(ColumnCount - 1) = txtValore7.Text
            clIndxMetadato7 = ColumnCount
            ColumnCount = ColumnCount + 1
        End If

        If txtMetadato8.Text <> "" Then
            dtgridExport.Columns.Add("Metadato8", txtMetadato8.Text)
            Metadati(ColumnCount - 1) = txtMetadato8.Text
            Valori(ColumnCount - 1) = txtValore8.Text
            clIndxMetadato8 = ColumnCount
            ColumnCount = ColumnCount + 1
        End If

        If txtMetadato9.Text <> "" Then
            dtgridExport.Columns.Add("Metadato9", txtMetadato9.Text)
            Metadati(ColumnCount - 1) = txtMetadato9.Text
            Valori(ColumnCount - 1) = txtValore9.Text
            clIndxMetadato9 = ColumnCount
            ColumnCount = ColumnCount + 1
        End If

        If txtMetadato10.Text <> "" Then
            dtgridExport.Columns.Add("Metadato10", txtMetadato10.Text)
            Metadati(ColumnCount - 1) = txtMetadato10.Text
            Valori(ColumnCount - 1) = txtValore10.Text
            clIndxMetadato10 = ColumnCount
            ColumnCount = ColumnCount + 1
        End If

        If Not dtgridExport.ColumnCount = 0 Then
            check = GetStuffs(Scairim.token, Scairim.apiToken, Scairim.LinkCM & "/api/rest/players?limit=9999&offset=0&fields=" & toExtract)
            If InStr(check, "(401)") > 0 Then
                Return "Sessione scaduta, per favore rifai il Login."
            End If
            Json = JObject.Parse(check)
            If Json.ContainsKey("list") Then
                NumChild = CInt(Json.Item("count").ToString)
                Contatore = 0
                For Each child In Json.Item("list").Children()
                    If Contatore Mod 10 = 0 Then
                        Loading(CInt((Contatore * 100) / NumChild))
                    End If
                    If Not txtName.Text = "" Then
                        If chkUpLow.Checked Then
                            If InStr(child.Item("name").ToString, txtName.Text) = 0 Then
                                Contatore = Contatore + 1
                                Continue For
                            End If
                        Else
                            If InStr(child.Item("name").ToString.ToLower, txtName.Text.ToLower) = 0 Then
                                Contatore = Contatore + 1
                                Continue For
                            End If
                        End If
                    End If
                        temp = GetStuffs(Scairim.token, Scairim.apiToken, Scairim.LinkCM & "/api/rest/players/" & child.Item("id").ToString & "?fields=metadataValue")
                    If InStr(temp, "(401)") > 0 Then
                        Return "Sessione scaduta, per favore rifai il Login."
                    End If
                    temp2 = "Not Set"
                    Json2 = JObject.Parse(temp)
                    'If Json2.ContainsKey("metadataValue") Then
                    PlayerOk = True
                        Index = 0
                        Do While Index < 10
                            If Metadati(Index) = "" Then
                                Exit Do
                            End If
                            If Valori(Index) <> "" Then
                                Trovato = False
                                If Json2.ContainsKey("metadataValue") Then
                                    For Each metadato In Json2.Item("metadataValue").Children
                                        If metadato.Item("playerMetadata").Item("name").ToString = "Player." & Metadati(Index) Then
                                            If metadato.Item("playerMetadata").Item("valueType").ToString = "PICKLIST" Then
                                                For Each value In metadato.Item("playerMetadata").Item("predefinedValues").Children()
                                                    If value.Item("id").ToString = metadato.Item("value").ToString Then
                                                        If String.Compare(value.Item("value").ToString, Valori(Index), True) <> 0 Then
                                                            PlayerOk = False
                                                            Exit Do
                                                        Else
                                                            Trovato = True
                                                            Exit For
                                                        End If
                                                    End If
                                                Next
                                            End If
                                            If metadato.Item("playerMetadata").Item("valueType").ToString = "ANY" Then
                                                If String.Compare(metadato.Item("value").ToString, Valori(Index), True) <> 0 Then
                                                    PlayerOk = False
                                                    Exit Do
                                                Else
                                                    Trovato = True
                                                End If
                                            End If
                                        End If
                                        If Trovato = True Then
                                            Exit For
                                        End If
                                    Next
                                End If
                                If Trovato = False And String.Compare(Valori(Index), "true", True) = 0 Then
                                    PlayerOk = False
                                    Exit Do
                                End If
                            End If
                            Index = Index + 1
                        Loop
                        If PlayerOk = False Then
                            Continue For
                        Else
                            RowIndex = dtgridExport.Rows.Add()
                            dtgridExport.Item(clIndxPlayerName, RowIndex).Value = child.Item("name").ToString
                            Index = 0
                            Do While Index < 10
                                If Metadati(Index) = "" Then
                                    Exit Do
                                End If
                                Trovato = False
                                If Json2.ContainsKey("metadataValue") Then
                                    For Each metadato In Json2.Item("metadataValue").Children
                                        If metadato.Item("playerMetadata").Item("name").ToString = "Player." & Metadati(Index) Then
                                            If metadato.Item("playerMetadata").Item("valueType").ToString = "PICKLIST" Then
                                                For Each value In metadato.Item("playerMetadata").Item("predefinedValues").Children()
                                                    If value.Item("id").ToString = metadato.Item("value").ToString Then
                                                        dtgridExport.Item(Index + 1, RowIndex).Value = value.Item("value").ToString()
                                                        Exit For
                                                    End If
                                                Next
                                            End If
                                            If metadato.Item("playerMetadata").Item("valueType").ToString = "ANY" Then
                                                dtgridExport.Item(Index + 1, RowIndex).Value = metadato.Item("value").ToString
                                            End If
                                            Trovato = True
                                        End If
                                        If Trovato = True Then
                                            Exit For
                                        End If
                                    Next
                                End If
                                If Trovato = False Then
                                    dtgridExport.Item(Index + 1, RowIndex).Value = "Not Set/False"
                                End If
                                Index = Index + 1
                            Loop
                        End If
                    'End If
                    Contatore = Contatore + 1
                Next
            Else
                Return "Errore nel caricamento del Json."
            End If
        Else
                Return "Niente Estrazione..."
        End If
        Return "Estrazione Completata!!!"
    End Function

End Class