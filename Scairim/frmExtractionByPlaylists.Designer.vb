﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmExtractionByPlaylists
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dtgridExport = New System.Windows.Forms.DataGridView()
        Me.chkUpLow = New System.Windows.Forms.CheckBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.txtName = New System.Windows.Forms.TextBox()
        Me.btnEstraiExcel = New System.Windows.Forms.Button()
        Me.btnGo = New System.Windows.Forms.Button()
        Me.chkItemList = New System.Windows.Forms.CheckBox()
        Me.chkItemCondition = New System.Windows.Forms.CheckBox()
        Me.chkItemType = New System.Windows.Forms.CheckBox()
        Me.chkValidDate = New System.Windows.Forms.CheckBox()
        Me.chkItemOrder = New System.Windows.Forms.CheckBox()
        CType(Me.dtgridExport, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dtgridExport
        '
        Me.dtgridExport.AllowUserToAddRows = False
        Me.dtgridExport.AllowUserToDeleteRows = False
        Me.dtgridExport.AllowUserToOrderColumns = True
        Me.dtgridExport.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dtgridExport.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dtgridExport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgridExport.Location = New System.Drawing.Point(9, 170)
        Me.dtgridExport.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.dtgridExport.Name = "dtgridExport"
        Me.dtgridExport.ReadOnly = True
        Me.dtgridExport.RowTemplate.Height = 24
        Me.dtgridExport.Size = New System.Drawing.Size(582, 262)
        Me.dtgridExport.TabIndex = 0
        '
        'chkUpLow
        '
        Me.chkUpLow.AutoSize = True
        Me.chkUpLow.Location = New System.Drawing.Point(166, 30)
        Me.chkUpLow.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.chkUpLow.Name = "chkUpLow"
        Me.chkUpLow.Size = New System.Drawing.Size(96, 17)
        Me.chkUpLow.TabIndex = 48
        Me.chkUpLow.Text = "Case Sensitive"
        Me.chkUpLow.UseVisualStyleBackColor = True
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(9, 14)
        Me.Label21.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(60, 13)
        Me.Label21.TabIndex = 47
        Me.Label21.Text = "Filtro Nome"
        '
        'txtName
        '
        Me.txtName.Location = New System.Drawing.Point(9, 30)
        Me.txtName.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(152, 20)
        Me.txtName.TabIndex = 46
        '
        'btnEstraiExcel
        '
        Me.btnEstraiExcel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnEstraiExcel.Location = New System.Drawing.Point(9, 444)
        Me.btnEstraiExcel.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.btnEstraiExcel.Name = "btnEstraiExcel"
        Me.btnEstraiExcel.Size = New System.Drawing.Size(122, 32)
        Me.btnEstraiExcel.TabIndex = 49
        Me.btnEstraiExcel.Text = "Estrai in Excel"
        Me.btnEstraiExcel.UseVisualStyleBackColor = True
        '
        'btnGo
        '
        Me.btnGo.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnGo.Location = New System.Drawing.Point(255, 444)
        Me.btnGo.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.btnGo.Name = "btnGo"
        Me.btnGo.Size = New System.Drawing.Size(103, 32)
        Me.btnGo.TabIndex = 50
        Me.btnGo.Text = "Scan Playlists"
        Me.btnGo.UseVisualStyleBackColor = True
        '
        'chkItemList
        '
        Me.chkItemList.AutoSize = True
        Me.chkItemList.Location = New System.Drawing.Point(9, 61)
        Me.chkItemList.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.chkItemList.Name = "chkItemList"
        Me.chkItemList.Size = New System.Drawing.Size(65, 17)
        Me.chkItemList.TabIndex = 51
        Me.chkItemList.Text = "Item List"
        Me.chkItemList.UseVisualStyleBackColor = True
        '
        'chkItemCondition
        '
        Me.chkItemCondition.AutoSize = True
        Me.chkItemCondition.Location = New System.Drawing.Point(22, 105)
        Me.chkItemCondition.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.chkItemCondition.Name = "chkItemCondition"
        Me.chkItemCondition.Size = New System.Drawing.Size(98, 17)
        Me.chkItemCondition.TabIndex = 52
        Me.chkItemCondition.Text = "Item Conditions"
        Me.chkItemCondition.UseVisualStyleBackColor = True
        '
        'chkItemType
        '
        Me.chkItemType.AutoSize = True
        Me.chkItemType.Location = New System.Drawing.Point(22, 83)
        Me.chkItemType.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.chkItemType.Name = "chkItemType"
        Me.chkItemType.Size = New System.Drawing.Size(73, 17)
        Me.chkItemType.TabIndex = 53
        Me.chkItemType.Text = "Item Type"
        Me.chkItemType.UseVisualStyleBackColor = True
        '
        'chkValidDate
        '
        Me.chkValidDate.AutoSize = True
        Me.chkValidDate.Location = New System.Drawing.Point(22, 127)
        Me.chkValidDate.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.chkValidDate.Name = "chkValidDate"
        Me.chkValidDate.Size = New System.Drawing.Size(98, 17)
        Me.chkValidDate.TabIndex = 54
        Me.chkValidDate.Text = "Item Valid Date"
        Me.chkValidDate.UseVisualStyleBackColor = True
        '
        'chkItemOrder
        '
        Me.chkItemOrder.AutoSize = True
        Me.chkItemOrder.Location = New System.Drawing.Point(22, 148)
        Me.chkItemOrder.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.chkItemOrder.Name = "chkItemOrder"
        Me.chkItemOrder.Size = New System.Drawing.Size(75, 17)
        Me.chkItemOrder.TabIndex = 55
        Me.chkItemOrder.Text = "Item Order"
        Me.chkItemOrder.UseVisualStyleBackColor = True
        '
        'frmExtractionByPlaylists
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(600, 486)
        Me.Controls.Add(Me.chkItemOrder)
        Me.Controls.Add(Me.chkValidDate)
        Me.Controls.Add(Me.chkItemType)
        Me.Controls.Add(Me.chkItemCondition)
        Me.Controls.Add(Me.chkItemList)
        Me.Controls.Add(Me.btnGo)
        Me.Controls.Add(Me.btnEstraiExcel)
        Me.Controls.Add(Me.chkUpLow)
        Me.Controls.Add(Me.Label21)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.dtgridExport)
        Me.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.MinimumSize = New System.Drawing.Size(616, 524)
        Me.Name = "frmExtractionByPlaylists"
        Me.Text = "frmExtractionByPlaylists"
        CType(Me.dtgridExport, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents dtgridExport As DataGridView
    Friend WithEvents chkUpLow As CheckBox
    Friend WithEvents Label21 As Label
    Friend WithEvents txtName As TextBox
    Friend WithEvents btnEstraiExcel As Button
    Friend WithEvents btnGo As Button
    Friend WithEvents chkItemList As CheckBox
    Friend WithEvents chkItemCondition As CheckBox
    Friend WithEvents chkItemType As CheckBox
    Friend WithEvents chkValidDate As CheckBox
    Friend WithEvents chkItemOrder As CheckBox
End Class
