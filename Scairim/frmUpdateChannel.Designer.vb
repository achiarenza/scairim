﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUpdateChannel
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtChannelList = New System.Windows.Forms.TextBox()
        Me.txtOrario = New System.Windows.Forms.TextBox()
        Me.btnGo = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'txtChannelList
        '
        Me.txtChannelList.Location = New System.Drawing.Point(12, 12)
        Me.txtChannelList.Multiline = True
        Me.txtChannelList.Name = "txtChannelList"
        Me.txtChannelList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtChannelList.Size = New System.Drawing.Size(366, 325)
        Me.txtChannelList.TabIndex = 0
        '
        'txtOrario
        '
        Me.txtOrario.Location = New System.Drawing.Point(73, 343)
        Me.txtOrario.Name = "txtOrario"
        Me.txtOrario.Size = New System.Drawing.Size(100, 20)
        Me.txtOrario.TabIndex = 1
        '
        'btnGo
        '
        Me.btnGo.Location = New System.Drawing.Point(154, 382)
        Me.btnGo.Name = "btnGo"
        Me.btnGo.Size = New System.Drawing.Size(76, 40)
        Me.btnGo.TabIndex = 2
        Me.btnGo.Text = "Button1"
        Me.btnGo.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 345)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(55, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "HH:mm:ss"
        '
        'frmUpdateChannel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(390, 431)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnGo)
        Me.Controls.Add(Me.txtOrario)
        Me.Controls.Add(Me.txtChannelList)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmUpdateChannel"
        Me.Text = "Update Channel"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txtChannelList As TextBox
    Friend WithEvents txtOrario As TextBox
    Friend WithEvents btnGo As Button
    Friend WithEvents Label1 As Label
End Class
