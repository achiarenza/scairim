﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLoginTV
    Inherits System.Windows.Forms.Form

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla mediante l'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtUsr = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtPwd = New System.Windows.Forms.TextBox()
        Me.btnLogin = New System.Windows.Forms.Button()
        Me.Token = New System.Windows.Forms.Label()
        Me.txtToken = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'txtUsr
        '
        Me.txtUsr.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txtUsr.Enabled = False
        Me.txtUsr.Location = New System.Drawing.Point(69, 59)
        Me.txtUsr.Name = "txtUsr"
        Me.txtUsr.Size = New System.Drawing.Size(325, 22)
        Me.txtUsr.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(66, 39)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(193, 17)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Username (Not implemented)"
        '
        'Label2
        '
        Me.Label2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(66, 107)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(189, 17)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Password (Not implemented)"
        '
        'txtPwd
        '
        Me.txtPwd.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txtPwd.Enabled = False
        Me.txtPwd.Location = New System.Drawing.Point(69, 127)
        Me.txtPwd.Name = "txtPwd"
        Me.txtPwd.Size = New System.Drawing.Size(325, 22)
        Me.txtPwd.TabIndex = 2
        Me.txtPwd.UseSystemPasswordChar = True
        '
        'btnLogin
        '
        Me.btnLogin.Location = New System.Drawing.Point(163, 247)
        Me.btnLogin.Name = "btnLogin"
        Me.btnLogin.Size = New System.Drawing.Size(136, 57)
        Me.btnLogin.TabIndex = 4
        Me.btnLogin.Text = "Login"
        Me.btnLogin.UseVisualStyleBackColor = True
        '
        'Token
        '
        Me.Token.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Token.AutoSize = True
        Me.Token.Location = New System.Drawing.Point(66, 175)
        Me.Token.Name = "Token"
        Me.Token.Size = New System.Drawing.Size(48, 17)
        Me.Token.TabIndex = 6
        Me.Token.Text = "Token"
        '
        'txtToken
        '
        Me.txtToken.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txtToken.Location = New System.Drawing.Point(69, 195)
        Me.txtToken.Name = "txtToken"
        Me.txtToken.Size = New System.Drawing.Size(325, 22)
        Me.txtToken.TabIndex = 5
        '
        'frmLoginTV
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(463, 328)
        Me.Controls.Add(Me.Token)
        Me.Controls.Add(Me.txtToken)
        Me.Controls.Add(Me.btnLogin)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtPwd)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtUsr)
        Me.MinimumSize = New System.Drawing.Size(481, 375)
        Me.Name = "frmLoginTV"
        Me.Text = "Login Teamviewer"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txtUsr As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents txtPwd As TextBox
    Friend WithEvents btnLogin As Button
    Friend WithEvents Token As Label
    Friend WithEvents txtToken As TextBox
End Class
