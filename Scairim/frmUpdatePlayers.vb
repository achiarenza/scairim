﻿Imports System.Windows.Forms.LinkLabel
Imports Microsoft.Office.Interop
Imports Newtonsoft.Json.Linq

Public Class frmUpdatePlayers

    Dim result As String
    Dim Json, Json2, JsonW As JObject
    Dim JArrayR As JArray
    Dim GroupIdMap As New Dictionary(Of String, String)
    Dim MetadataIdMap As New Dictionary(Of String, String)
    Dim ScheduleList As List(Of String)

    Private Sub frmUpdatePlayer_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Icon = Scairim.Icon
        lbAvailableGroups.Items.Clear()
        lbGroupsAdd.Items.Clear()
        lbAvailableMetadata.Items.Clear()
        lbSelectedMetadata.Items.Clear()
        txtPlayers.Text = ""
        txtReplace.Text = ""
        txtWith.Text = ""
        txtPrefix.Text = ""
        txtMetadatoAny.Text = ""
        txtMetadatoAnyValue.Text = ""
        dtSchedule.Value = Now
        ScheduleList = New List(Of String)
        btnDeleteSchedule.Hide()
        dtgridExportScheduleDone.Visible = False
        btnShowDone.Text = "Show Done"
        Try
            Loading("show")
            RefreshDataGridSchedule()
            Dim check = GetStuffs(Scairim.token, Scairim.apiToken, Scairim.LinkCM & "/api/rest/playergroup?limit=9999&offset=0")
            If InStr(check, "(401)") > 0 Then
                Logga("Sessione scaduta, per favore rifai il Login.")
                Exit Sub
            End If
            Loading(10)
            Json = JObject.Parse(check)
            If Json.ContainsKey("list") Then
                For Each child In Json.Item("list").Children()
                    lbAvailableGroups.Items.Add(child.Item("name").ToString)
                    GroupIdMap.Add(child.Item("name").ToString, child.Item("id").ToString)
                Next
            End If
            Loading(40)
            check = GetStuffs(Scairim.token, Scairim.apiToken, Scairim.LinkCM & "/api/rest/playerMetadata?limit=9999&offset=0")
            If InStr(check, "(401)") > 0 Then
                Logga("Sessione scaduta, per favore rifai il Login.")
                Exit Sub
            End If
            Loading(50)
            Json = JObject.Parse(check)
            If Json.ContainsKey("list") Then
                For Each child In Json.Item("list").Children()
                    lbAvailableMetadata.Items.Add(child.Item("name").ToString)
                    MetadataIdMap.Add(child.Item("name").ToString, child.Item("id").ToString)
                Next
            End If
            Loading("close")
        Catch ex As Exception
            Logga("ERROR - frmUpdatePlayer_Load: " & ex.ToString)
        End Try
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        For i As Integer = lbAvailableGroups.SelectedItems.Count - 1 To 0 Step -1
            lbGroupsAdd.Items.Add(lbAvailableGroups.SelectedItems.Item(i))
            lbAvailableGroups.Items.Remove(lbAvailableGroups.SelectedItems(i))
        Next
    End Sub

    Private Sub btnRmv_Click(sender As Object, e As EventArgs) Handles btnRmv.Click
        For i As Integer = lbGroupsAdd.SelectedItems.Count - 1 To 0 Step -1
            lbAvailableGroups.Items.Add(lbGroupsAdd.SelectedItems.Item(i))
            lbGroupsAdd.Items.Remove(lbGroupsAdd.SelectedItems(i))
        Next
    End Sub

    Private Sub btnGo_Click(sender As Object, e As EventArgs) Handles btnGo.Click
        If (Scairim.logged) Then
            Utilities.timeout = 120000
            Logga("Aggiorno i player!!!")
            Try
                Loading("show")
                result = UpdatePlayers()
                Loading("close")
            Catch ex As Exception
                Logga("ERROR - Aggiornamento: " & ex.ToString)
            End Try
            JsonW = Nothing
            Logga(result)
            MsgBox(result)
        Else
            Logga("Se prima non fai il login su un CM non posso fare nulla :)")
        End If
    End Sub

    Private Function UpdatePlayers()
        Dim res As String = ""
        Dim NumChild As Integer = txtPlayers.Lines.Count
        Dim Contatore As Integer
        Dim temp As String = ""
        Dim GroupString As New List(Of String)
        Dim MetadataString As New List(Of String)
        Dim WasError As Boolean = False
        If lbGroupsAdd.Items.Count <> 0 Then
            For Each group In lbGroupsAdd.Items
                GroupString.Add(group)
            Next
        End If
        If lbSelectedMetadata.Items.Count <> 0 Then
            For Each meta In lbSelectedMetadata.Items
                MetadataString.Add(meta)
            Next
        End If

        Contatore = 0
        WasError = False
        For Each playerName In txtPlayers.Lines
            If Contatore Mod 10 = 0 Then
                Loading(CInt((Contatore * 100) / NumChild))
            End If
            If Not PlayerExists(playerName) Then
                Logga("Player Inesistente: " & playerName)
                Contatore += 1
                Continue For
            End If
            temp = ""
            If lbGroupsAdd.Items.Count <> 0 Then
                res = UpdatePlayerGroups(GetPlayerId(playerName), ReplaceName(playerName), GroupString, GroupIdMap)
                If InStr(res.ToUpper, "ERROR") > 0 Then
                    Logga("Impossibile aggiornare i Player Group sul Player: " & playerName & " - " & res)
                    WasError = True
                Else
                    Logga("PlayerGroups del Player Aggiornati: " & playerName)
                End If
            End If
            If lbSelectedMetadata.Items.Count <> 0 Then
                res = UpdatePlayerDescFromMetadata(GetPlayerId(playerName), playerName, MetadataString, MetadataIdMap)
                If InStr(res.ToUpper, "ERROR") > 0 Then
                    Logga("Impossibile aggiornare la descrizione sul Player: " & playerName & " - " & res)
                    WasError = True
                Else
                    Logga("Description Updated on: " & playerName)
                End If
            End If
            If txtReplace.Text <> "" Then
                temp = Replace(ReplaceName(playerName), txtReplace.Text, txtWith.Text)
                res = UpdatePlayerName(GetPlayerId(playerName), temp)
                If InStr(res.ToUpper, "ERROR") > 0 Then
                    Logga("Impossibile aggiornare il nome sul Player: " & playerName & " - " & res)
                    WasError = True
                Else
                    Logga("Nome del Player Aggiornato da: " & playerName & " a: " & temp)
                End If
            End If
            If txtPrefix.Text <> "" Then
                temp = txtPrefix.Text & playerName
                res = UpdatePlayerName(GetPlayerId(playerName), temp)
                If InStr(res.ToUpper, "ERROR") > 0 Then
                    Logga("Impossibile aggiornare il nome sul Player: " & playerName & " - " & res)
                    WasError = True
                Else
                    Logga("Nome del Player Aggiornato da: " & playerName & " a: " & temp)
                End If
            End If
            If txtSuffix.Text <> "" Then
                temp = playerName & txtSuffix.Text
                res = UpdatePlayerName(GetPlayerId(playerName), temp)
                If InStr(res.ToUpper, "ERROR") > 0 Then
                    Logga("Impossibile aggiornare il nome sul Player: " & playerName & " - " & res)
                    WasError = True
                Else
                    Logga("Nome del Player Aggiornato da: " & playerName & " a: " & temp)
                End If
            End If
            If chkModifyEnabled.Checked Then
                If rdbEnable.Checked Then
                    temp = "true"
                End If
                If rdbDisable.Checked Then
                    temp = "false"
                End If
                res = UpdatePlayerEnabled(GetPlayerId(playerName), playerName, temp)
                If InStr(res.ToUpper, "ERROR") > 0 Then
                    Logga("Impossibile aggiornare il Player: " & playerName & " - " & res)
                    WasError = True
                Else
                    Logga("Player Enabled: " & playerName & " : " & temp)
                End If
            End If
            Contatore += 1
        Next
        If WasError = True Then
            res = "ATTENZIONE! Controlla i log..."
        Else
            res = "Fatto!"
        End If
        UpdatePlayers = res
    End Function

    Private Sub btnMeta_Click(sender As Object, e As EventArgs) Handles btnMeta.Click
        If (Scairim.logged) Then
            Utilities.timeout = 120000
            Try
                Loading("show")
                If txtMetadataExcel.Text <> "" Then
                    If IO.File.Exists(txtMetadataExcel.Text) Then
                        Logga("Schedulo gli aggiornamenti dei metadata!!!")
                        result = UpdatePlayersMetadataExcel(txtMetadataExcel.Text)
                    Else
                        Logga("ERROR - btnMeta_Click: File inesistente")
                        result = "ERROR - File inesistente"
                    End If
                ElseIf chkSchedule.Checked Then
                    Logga("Schedulo gli aggiornamenti dei metadata!!!")
                    result = UpdatePlayersMetadata()
                Else
                    Logga("Aggiorno i metadati di quei player!!!")
                    result = UpdatePlayersMetadata()
                End If
                RefreshDataGridSchedule()
                Loading("close")
            Catch ex As Exception
                Logga("ERROR - Aggiornamento: " & ex.ToString)
            End Try
            JsonW = Nothing
            Logga(result)
            MsgBox(result)
        Else
            Logga("Se prima non fai il login su un CM non posso fare nulla :)")
        End If
    End Sub

    Private Function UpdatePlayersMetadata()
        Dim res As String = ""
        Dim NumChild As Integer = txtPlayers.Lines.Count
        Dim Contatore As Integer
        Dim temp As String = ""
        Dim WasError As Boolean = False
        Contatore = 0
        WasError = False
        For Each playerName In txtPlayers.Lines
            If Contatore Mod 2 = 0 Then
                Loading(CInt((Contatore * 100) / NumChild))
            End If
            If Not PlayerExists(playerName) Then
                Logga("Player Inesistente: " & playerName)
                Continue For
            End If
            temp = ""
            If txtMetadatoBoolean.Text <> "" Then
                If rdOn.Checked Then
                    res = UpdatePlayerMetadato(GetPlayerId(playerName), ReplaceName(playerName),
                                               txtMetadatoBoolean.Text, "true", chkSchedule.Checked, dtSchedule.Value)
                Else
                    res = UpdatePlayerMetadato(GetPlayerId(playerName), ReplaceName(playerName),
                                               txtMetadatoBoolean.Text, "false", chkSchedule.Checked, dtSchedule.Value)
                End If
                If InStr(res.ToUpper, "ERROR") > 0 Then
                    Logga("Impossibile aggiornare il Metadato " & txtMetadatoBoolean.Text & " sul Player: " & playerName & " - " & res)
                    WasError = True
                Else
                    Logga("Metadato del Player Aggiornato: " & playerName)
                End If
            End If
            If txtMetadatoAny.Text <> "" Then
                res = UpdatePlayerMetadato(GetPlayerId(playerName), ReplaceName(playerName), txtMetadatoAny.Text,
                                           txtMetadatoAnyValue.Text, chkSchedule.Checked, dtSchedule.Value)
                If InStr(res.ToUpper, "ERROR") > 0 Then
                    Logga("Impossibile aggiornare il Metadato " & txtMetadatoAny.Text & " sul Player: " & playerName & " - " & res)
                    WasError = True
                Else
                    Logga("Metadato del Player Aggiornato: " & playerName)
                End If
            End If
        Next
        If WasError = True Then
            res = "ATTENZIONE! Controlla i log..."
        Else
            res = "Fatto!"
        End If
        UpdatePlayersMetadata = res
    End Function

    Private Function UpdatePlayersMetadataExcel(ExcelFile As String)
        Dim res As String = ""
        Dim NumChild As Integer = 0
        Dim Contatore As Integer
        Dim playerName As String = ""
        Dim temp As String = ""
        Dim WasError As Boolean = False
        Dim FromDate As Date = Nothing
        Dim ToDate As Date = Nothing
        Dim OreFuso As Integer
        Dim xlApp As Excel.Application = New Microsoft.Office.Interop.Excel.Application()
        If xlApp Is Nothing Then
            UpdatePlayersMetadataExcel = "ERROR - UpdatePlayersMetadataExcel: Excel Non è installato nel sistema"
            Exit Function
        End If
        Dim xlWorkBook As Excel.Workbook
        xlWorkBook = xlApp.Workbooks.Open(ExcelFile)
        Dim xlWorkSheet = xlWorkBook.Sheets(1)
        While True
            'Logga(xlWorkSheet.Cells(NumChild + 1, 1).Value)
            If xlWorkSheet.Cells(NumChild + 1, 1).Value = "" Then
                Exit While
            End If
            NumChild = NumChild + 1
        End While
        'Logga(NumChild)
        Contatore = 0
        WasError = False
        For row = 1 To NumChild
            If Contatore Mod 10 = 0 Then
                Loading(CInt((Contatore * 100) / NumChild))
            End If
            If row = 1 Then
                Continue For
            End If
            playerName = xlWorkSheet.Cells(row, 1).Value
            If Not PlayerExists(playerName) Then
                Logga("Player Inesistente: " & playerName)
                Continue For
            End If
            temp = ""
            FromDate = Nothing
            ToDate = Nothing
            If xlWorkSheet.Cells(row, 8).Value Is Nothing Then
                temp = GetPlayerTimeZone(GetPlayerId(playerName))
                If temp = "" Then
                    OreFuso = 0
                Else
                    OreFuso = CInt(Split(Split(Split(GetPlayerTimeZone(GetPlayerId(playerName)), ")")(0), "C")(1), ":")(0))
                End If
                FromDate = Date.Parse(xlWorkSheet.Cells(row, 6).Value)
                FromDate = FromDate.AddHours(OreFuso)
                If Not xlWorkSheet.Cells(row, 7).Value Is Nothing Then
                    ToDate = Date.Parse(xlWorkSheet.Cells(row, 7).Value)
                    ToDate = ToDate.AddHours(OreFuso)
                End If
            Else
                FromDate = Date.Parse(xlWorkSheet.Cells(row, 6).Value + " " + xlWorkSheet.Cells(row, 8).Value.ToString + ":00")
                If Not xlWorkSheet.Cells(row, 7).Value Is Nothing Then
                    ToDate = Date.Parse(xlWorkSheet.Cells(row, 7).Value + " " + xlWorkSheet.Cells(row, 8).Value.ToString + ":00")
                End If
            End If
            If xlWorkSheet.Cells(row, 3).Value.ToString.ToLower = "boolean" Then
                If xlWorkSheet.Cells(row, 4).Value.ToString.ToLower = "true" Or xlWorkSheet.Cells(row, 4).Value.ToString.ToLower = "on" Then
                    res = UpdatePlayerMetadato(GetPlayerId(playerName), ReplaceName(playerName),
                                               xlWorkSheet.Cells(row, 2).Value, "true", True, FromDate)
                Else
                    res = UpdatePlayerMetadato(GetPlayerId(playerName), ReplaceName(playerName),
                                               xlWorkSheet.Cells(row, 2).Value, "false", True, FromDate)
                End If
                If ToDate <> Nothing Then
                    If xlWorkSheet.Cells(row, 5).Value.ToString.ToLower = "true" Or xlWorkSheet.Cells(row, 5).Value.ToString.ToLower = "on" Then
                        res = UpdatePlayerMetadato(GetPlayerId(playerName), ReplaceName(playerName),
                                                   xlWorkSheet.Cells(row, 2).Value, "true", True, ToDate)
                    Else
                        res = UpdatePlayerMetadato(GetPlayerId(playerName), ReplaceName(playerName),
                                                   xlWorkSheet.Cells(row, 2).Value, "false", True, ToDate)
                    End If
                End If
                If InStr(res.ToUpper, "ERROR") > 0 Then
                    Logga("Impossibile aggiornare il Metadato " & txtMetadatoBoolean.Text & " sul Player: " & playerName & " - " & res)
                    WasError = True
                Else
                    Logga("Metadato del Player Aggiornato: " & playerName)
                End If
            End If
            If xlWorkSheet.Cells(row, 3).Value.ToString.ToLower = "any" Then
                res = UpdatePlayerMetadato(GetPlayerId(playerName), ReplaceName(playerName), xlWorkSheet.Cells(row, 2).Value,
                                           xlWorkSheet.Cells(row, 4).Value, True, FromDate)
                If ToDate <> Nothing Then
                    res = UpdatePlayerMetadato(GetPlayerId(playerName), ReplaceName(playerName), xlWorkSheet.Cells(row, 2).Value,
                                           xlWorkSheet.Cells(row, 5).Value, True, ToDate)
                End If
                If InStr(res.ToUpper, "ERROR") > 0 Then
                    Logga("Impossibile aggiornare il Metadato " & txtMetadatoAny.Text & " sul Player: " & playerName & " - " & res)
                    WasError = True
                Else
                    Logga("Metadato del Player Aggiornato: " & playerName)
                End If
            End If
        Next
        If WasError = True Then
            res = "ATTENZIONE! Controlla i log..."
        Else
            res = "Fatto!"
        End If
        xlWorkBook.Close()
        xlApp.Workbooks.Close()
        xlApp.Quit()
        UpdatePlayersMetadataExcel = res
    End Function

    Private Sub btnDeleteSchedule_Click(sender As Object, e As EventArgs) Handles btnDeleteSchedule.Click
        If MsgBox("Cancellare le schedulazioni evidenziate?", vbYesNo) = MsgBoxResult.No Then
            Exit Sub
        End If
        Dim ids As String = ""
        For Each row In dtgridExportSchedule.SelectedRows
            'Logga(DeleteFileFromShare(CDate(Split(dtgridExportSchedule.Item(3, row.Index).Value, " ")(0)).ToString("ddMMyyyy") + "_" +
            '                          Replace(Split(dtgridExportSchedule.Item(3, row.Index).Value, " ")(1), ":", "") + "_" +
            '                          dtgridExportSchedule.Item(0, row.Index).Value + "_" + dtgridExportSchedule.Item(1, row.Index).Value + ".json"))
            ids = ids + dtgridExportSchedule.Item(5, row.Index).Value + ","
        Next
        'Logga(ids.Substring(0, ids.Length - 1))
        Logga(DeleteUpdatePlayer(ids))
        RefreshDataGridSchedule()
    End Sub

    Private Sub RefreshDataGridSchedule()
        Dim temp As String = ""
        Dim rowid As Integer
        dtgridExportSchedule.Rows.Clear()
        dtgridExportScheduleDone.Rows.Clear()
        temp = GetUpdatePlayer(Scairim.LinkCM)
        If InStr(temp, "ERROR") > 0 Then
            Logga(temp)
            Exit Sub
        End If
        If temp = "" Then
            Exit Sub
        End If
        JArrayR = JArray.Parse(temp)
        If JArrayR.Count = 0 Then
            Exit Sub
        End If
        For Each element In JArrayR
            If element.Item("processed") Then
                dtgridExportScheduleDone.Rows.Add(element.Item("player_name").ToString,
                                          element.Item("metadato").ToString, element.Item("metadata_value").ToString,
                                          CDate(element.Item("schedule").ToString).ToString("yyyy/MM/dd HH:mm"),
                                          element.Item("uploader").ToString, element.Item("id").ToString)
            Else
                rowid = dtgridExportSchedule.Rows.Add(element.Item("player_name").ToString,
                                          element.Item("metadato").ToString, element.Item("metadata_value").ToString,
                                          CDate(element.Item("schedule").ToString).ToString("yyyy/MM/dd HH:mm"),
                                          element.Item("uploader").ToString, element.Item("id").ToString)
                If CDate(element.Item("schedule").ToString) < Now Then
                    dtgridExportSchedule.Rows(rowid).DefaultCellStyle.BackColor = Color.Red
                End If
            End If
        Next
    End Sub

    Private Sub RefreshDataGridScheduleDone()
        Dim temp As String = ""
        dtgridExportScheduleDone.Rows.Clear()
        ScheduleList.Clear()
        temp = ReadFileFromShareElaborati(ScheduleList)
        If InStr(temp, "ERROR") > 0 Then
            Logga(temp)
            Exit Sub
        End If
        If ScheduleList.Count = 0 Then
            Exit Sub
        End If
        For Each element In ScheduleList
            Json = JObject.Parse(element)
            If Json.Item("link").ToString = Scairim.LinkCM Then
                dtgridExportScheduleDone.Rows.Add(Json.Item("playerName").ToString,
                                          Json.Item("metadato").ToString, Json.Item("metadataValue").ToString,
                                          CDate(Json.Item("ScheduleDate").ToString).ToString("yyyy/MM/dd") & " " & Json.Item("ScheduleTime").ToString,
                                          Json.Item("user").ToString)
            End If
        Next
    End Sub

    Private Sub btnBrowseExcel_Click(sender As Object, e As EventArgs) Handles btnBrowseExcel.Click
        Dim BrowseExcel As OpenFileDialog = New OpenFileDialog()
        BrowseExcel.DefaultExt = "xlsx"
        BrowseExcel.Filter = "Excel File (*.xlsx, *.xls)|*.xlsx;*.xls|All Files (*.*)|*.*"
        BrowseExcel.ShowDialog()
        txtMetadataExcel.Text = BrowseExcel.FileName
    End Sub

    Private Sub btnShowDone_Click(sender As Object, e As EventArgs) Handles btnShowDone.Click
        If dtgridExportScheduleDone.Visible Then
            dtgridExportScheduleDone.Visible = False
            btnShowDone.Text = "Show Done"
        Else
            dtgridExportScheduleDone.Show()
            btnShowDone.Text = "Show Schedule"
        End If
    End Sub

    Private Sub dtgridExportSchedule_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgridExportSchedule.CellClick
        Dim dgv As DataGridView = sender
        If dgv Is Nothing Then
            Exit Sub
        End If
        If dgv.CurrentRow.Selected Then
            btnDeleteSchedule.Show()
        Else
            btnDeleteSchedule.Hide()
        End If
        If dgv.CurrentRow.DefaultCellStyle.BackColor = Color.Red Then
            dgv.RowsDefaultCellStyle.SelectionBackColor = Color.DarkRed
        Else
            dgv.RowsDefaultCellStyle.SelectionBackColor = Color.Blue
        End If
    End Sub

    Private Sub btnAdd2_Click(sender As Object, e As EventArgs) Handles btnAdd2.Click
        For i As Integer = lbAvailableMetadata.SelectedItems.Count - 1 To 0 Step -1
            lbSelectedMetadata.Items.Add(lbAvailableMetadata.SelectedItems.Item(i))
            lbAvailableMetadata.Items.Remove(lbAvailableMetadata.SelectedItems(i))
        Next
    End Sub

    Private Sub btnRmv2_Click(sender As Object, e As EventArgs) Handles btnRmv2.Click
        For i As Integer = lbSelectedMetadata.SelectedItems.Count - 1 To 0 Step -1
            lbAvailableMetadata.Items.Add(lbSelectedMetadata.SelectedItems.Item(i))
            lbSelectedMetadata.Items.Remove(lbSelectedMetadata.SelectedItems(i))
        Next
    End Sub

    Function GetPlayerTimeZone(PlayerId As String)
        Dim temp = GetStuffs(Scairim.token, Scairim.apiToken, Scairim.LinkCM & "/api/rest/players/" & PlayerId & "?field=metadata")
        If InStr(temp, "(401)") > 0 Then
            Logga("Sessione scaduta, per favore rifai il Login.")
            GetPlayerTimeZone = ""
            Exit Function
        End If
        GetPlayerTimeZone = GetMetadatoValue(temp, "TimeZone")
    End Function

End Class