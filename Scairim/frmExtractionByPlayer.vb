﻿Imports Excel = Microsoft.Office.Interop.Excel
Imports Newtonsoft.Json.Linq
Imports System.Threading
Imports System.ComponentModel
Imports System.Text.RegularExpressions

Public Class frmExtractionByPlayer

    Dim Json, Json2 As JObject
    Dim result As String
    Dim MetadataIdMap As New Dictionary(Of String, String)

    Private Sub FrmExtractionByPlayer_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Icon = Scairim.Icon
        btnExportExcel.Visible = False
        dtgridExport.Rows.Clear()
        dtgridExport.Columns.Clear()
        lbAvailableMetadata.Items.Clear()
        lbSelectedMetadata.Items.Clear()
        CheckForIllegalCrossThreadCalls = False
        Try
            Dim check = GetStuffs(Scairim.token, Scairim.apiToken, Scairim.LinkCM & "/api/rest/playerMetadata?limit=9999&offset=0")
            If InStr(check, "(401)") > 0 Then
                Logga("Sessione scaduta, per favore rifai il Login.")
                Exit Sub
            End If
            Json = JObject.Parse(check)
            If Json.ContainsKey("list") Then
                For Each child In Json.Item("list").Children()
                    lbAvailableMetadata.Items.Add(child.Item("name").ToString)
                    MetadataIdMap.Add(child.Item("name").ToString, child.Item("id").ToString)
                Next
            End If
        Catch ex As Exception
            Logga("ERROR - frmExtractionByPlayer_Load: " & ex.ToString)
        End Try
    End Sub

    Private Sub BtnEstrai_Click(sender As Object, e As EventArgs) Handles btnEstrai.Click
        If (Scairim.logged) Then
            Utilities.timeout = 120000
            Logga("Faccio un'estrazione!!!")
            Try
                Loading("show", "EBP")
                Estrai()
                Loading("close")
            Catch ex As Exception
                Logga("ERROR - Estrazione: " & ex.ToString)
            End Try
            Logga(result)
            MsgBox(result)
            btnExportExcel.Visible = True
        Else
            Logga("Se prima non fai il login su un CM non posso estrarre nulla :)")
        End If
    End Sub

    Private Sub ChkAll_CheckedChanged(sender As Object, e As EventArgs) Handles chkAll.CheckedChanged
        If chkAll.Checked Then
            For Each ctrl In Me.Controls
                If TypeOf ctrl Is CheckBox Then
                    DirectCast(ctrl, CheckBox).Checked = True
                End If
            Next
        Else
            For Each ctrl In Me.Controls
                If TypeOf ctrl Is CheckBox Then
                    DirectCast(ctrl, CheckBox).Checked = False
                End If
            Next
        End If
    End Sub

    Private Sub BtnExportExcel_Click(sender As Object, e As EventArgs) Handles btnExportExcel.Click

        Dim excelfile As String
        Dim xlApp As Excel.Application = New Microsoft.Office.Interop.Excel.Application()

        If xlApp Is Nothing Then
            Logga("Excel Non è installato nel sistema!!")
            Return
        End If

        Dim savefile As New SaveFileDialog With {
            .InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyComputer),
            .Filter = "excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*",
            .FilterIndex = 1,
            .RestoreDirectory = True,
            .OverwritePrompt = True
        }
        If savefile.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            excelfile = savefile.FileName
        Else
            Return
        End If

        Loading("show")

        Dim xlWorkBook As Excel.Workbook
        xlWorkBook = xlApp.Workbooks.Add()
        Dim xlWorkSheet = xlWorkBook.Sheets(1)

        For Each column In dtgridExport.Columns
            xlWorkSheet.Cells(1, column.Index + 1).Value = column.HeaderText
        Next

        Dim i As Integer = 2
        Dim c As Integer = 1
        Dim Contatore = 0
        For Each row In dtgridExport.Rows
            If Contatore Mod 10 = 0 Then
                Loading(CInt((Contatore * 100) / dtgridExport.Rows.Count))
            End If
            c = 1
            For Each column In dtgridExport.Columns
                If Not dtgridExport.Item(column.Index, row.Index).Value Is Nothing Then
                    xlWorkSheet.Cells(i, c).Value = dtgridExport.Item(column.Index, row.Index).Value.ToString
                End If
                c += 1
            Next
            i += 1
            Contatore += 1
        Next

        xlWorkBook.SaveAs(excelfile)
        xlWorkBook.Close()
        xlApp.Quit()

        ReleaseComObject(xlWorkSheet)
        ReleaseComObject(xlWorkBook)
        ReleaseComObject(xlApp)

        Loading("close")

        Logga("Estrazione Esportata!")
        MsgBox("Estrazione Esportata!")

    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        For i As Integer = lbAvailableMetadata.SelectedItems.Count - 1 To 0 Step -1
            lbSelectedMetadata.Items.Add(lbAvailableMetadata.SelectedItems.Item(i))
            lbAvailableMetadata.Items.Remove(lbAvailableMetadata.SelectedItems(i))
        Next
    End Sub

    Private Sub btnRmv_Click(sender As Object, e As EventArgs) Handles btnRmv.Click
        For i As Integer = lbSelectedMetadata.SelectedItems.Count - 1 To 0 Step -1
            lbAvailableMetadata.Items.Add(lbSelectedMetadata.SelectedItems.Item(i))
            lbSelectedMetadata.Items.Remove(lbSelectedMetadata.SelectedItems(i))
        Next
    End Sub

    Private Sub Estrai()

        Dim toExtract As String = ""
        Dim RowIndex As Integer
        Dim temp As String
        Dim temp2 As String
        Dim check As String
        Dim NumChild As Integer
        Dim Contatore As Integer

        dtgridExport.Rows.Clear()
        dtgridExport.Columns.Clear()

        If chkPlayerID.Checked Then
            dtgridExport.Columns.Add("PlayerID", "Player ID")
            toExtract = AddTextComma(toExtract, "id", False)
        End If

        dtgridExport.Columns.Add("PlayerName", "Player Name")
        toExtract = AddTextComma(toExtract, "name", False)

        If chkDescription.Checked Then
            dtgridExport.Columns.Add("PlayerDesc", "Player Description")
            toExtract = AddTextComma(toExtract, "description", False)
        End If
        If chkUUID.Checked Then
            dtgridExport.Columns.Add("PlayerUUID", "Player UUID")
            toExtract = AddTextComma(toExtract, "uuid", False)
        End If
        If chkMAC.Checked Then
            dtgridExport.Columns.Add("PlayerMAC", "Player MAC")
            toExtract = AddTextComma(toExtract, "mac", False)
        End If
        If chkPlayerEnabled.Checked Then
            dtgridExport.Columns.Add("PlayerEnabled", "Player Enabled")
            toExtract = AddTextComma(toExtract, "enabled", False)
        End If
        If chkIP.Checked Then
            dtgridExport.Columns.Add("PlayerIP", "Player IP")
        End If
        If chkVersion.Checked Then
            dtgridExport.Columns.Add("PlayerVersion", "Player Version")
            toExtract = AddTextComma(toExtract, "active", True)
        End If
        If chkGroups.Checked Then
            dtgridExport.Columns.Add("PlayerGroups", "Player Groups")
            toExtract = AddTextComma(toExtract, "playergroups", True)
        End If
        If chkChannel.Checked Then
            dtgridExport.Columns.Add("Channel", "Channel Name")
            toExtract = AddTextComma(toExtract, "channelName", True)
        End If
        If chkFrameset.Checked Then
            dtgridExport.Columns.Add("Frameset", "Frameset Resolution")
            toExtract = AddTextComma(toExtract, "channelName", True)
        End If
        If chkFrames.Checked Then
            dtgridExport.Columns.Add("Frames", "Frames")
            toExtract = AddTextComma(toExtract, "channelName", True)
        End If
        If chkStatus.Checked Then
            dtgridExport.Columns.Add("Status", "Status")
            toExtract = AddTextComma(toExtract, "active", True)
        End If
        If chkLastHearthbeat.Checked Then
            dtgridExport.Columns.Add("LastReported", "LastReported")
            toExtract = AddTextComma(toExtract, "active", True)
        End If
        If lbSelectedMetadata.Items.Count > 0 Then
            For Each Meta In lbSelectedMetadata.Items
                dtgridExport.Columns.Add(Meta.ToString, Meta.ToString)
                toExtract = AddTextComma(toExtract, "metadataValue", True)
            Next
        End If
        If chkPlayerSerial.Checked Then
            dtgridExport.Columns.Add("PlayerSerialNumber", "Player Serial Number")
            toExtract = AddTextComma(toExtract, "uuid", True)
        End If
        If chkMonitorSerial.Checked Then
            dtgridExport.Columns.Add("MonitorSerialNumber", "Monitor Serial Number")
            toExtract = AddTextComma(toExtract, "uuid", True)
        End If

        If Not dtgridExport.ColumnCount = 0 Then
            check = GetStuffs(Scairim.token, Scairim.apiToken, Scairim.LinkCM & "/api/rest/players?limit=" & txtLimit.Text & "&offset=" & txtOffset.Text & "&fields=" & toExtract)
            If InStr(check, "(401)") > 0 Then
                result = "Sessione scaduta, per favore rifai il Login."
                Exit Sub
            End If
            If InStr(check, "ERROR") > 0 Then
                result = check
                Exit Sub
            End If
            Json = JObject.Parse(check)
            If Json.ContainsKey("list") Then
                NumChild = CInt(Json.Item("count").ToString)
                Contatore = 0
                For Each child In Json.Item("list").Children()
                    If Contatore Mod 10 = 0 Then
                        Loading(CInt((Contatore * 100) / NumChild))
                    End If
                    If Not txtPlayerName.Text = "" Then
                        If chkUpLow.Checked Then
                            If InStr(child.Item("name").ToString, txtPlayerName.Text) = 0 Then
                                Contatore += 1
                                Continue For
                            End If
                        Else
                            If InStr(child.Item("name").ToString.ToLower, txtPlayerName.Text.ToLower) = 0 Then
                                Contatore += 1
                                Continue For
                            End If
                        End If
                    End If
                    RowIndex = dtgridExport.Rows.Add()
                    If chkPlayerID.Checked Then
                        dtgridExport.Item(dtgridExport.Columns("PlayerID").Index, RowIndex).Value = child.Item("id").ToString
                    End If
                    dtgridExport.Item(dtgridExport.Columns("PlayerName").Index, RowIndex).Value = child.Item("name").ToString
                    If chkDescription.Checked Then
                        If child.Item("description") Is Nothing Then
                            dtgridExport.Item(dtgridExport.Columns("PlayerDesc").Index, RowIndex).Value = ""
                        Else
                            dtgridExport.Item(dtgridExport.Columns("PlayerDesc").Index, RowIndex).Value = child.Item("description").ToString
                        End If
                    End If
                    If chkUUID.Checked Then
                        If child.Item("uuid") Is Nothing Then
                            dtgridExport.Item(dtgridExport.Columns("PlayerUUID").Index, RowIndex).Value = ""
                        Else
                            dtgridExport.Item(dtgridExport.Columns("PlayerUUID").Index, RowIndex).Value = child.Item("uuid").ToString
                        End If
                    End If
                    If chkMAC.Checked Then
                        If child.Item("mac") Is Nothing Then
                            dtgridExport.Item(dtgridExport.Columns("PlayerMAC").Index, RowIndex).Value = ""
                        Else
                            dtgridExport.Item(dtgridExport.Columns("PlayerMAC").Index, RowIndex).Value = child.Item("mac").ToString
                        End If
                    End If
                    If chkPlayerEnabled.Checked Then
                        If child.Item("enabled") Is Nothing Then
                            dtgridExport.Item(dtgridExport.Columns("PlayerEnabled").Index, RowIndex).Value = ""
                        Else
                            dtgridExport.Item(dtgridExport.Columns("PlayerEnabled").Index, RowIndex).Value = child.Item("enabled").ToString
                        End If
                    End If
                    If chkIP.Checked Then
                        temp = GetStuffs(Scairim.token, Scairim.apiToken, Scairim.LinkCM & "/api/rest/players/" & child.Item("id").ToString & "/state")
                        If InStr(temp, "(401)") > 0 Then
                            result = "Sessione scaduta, per favore rifai il Login."
                            Exit Sub
                        End If
                        If Not InStr(temp, "ip") = 0 Then
                            Json2 = JObject.Parse(temp)
                            dtgridExport.Item(dtgridExport.Columns("PlayerIP").Index, RowIndex).Value = Json2.Item("ip").ToString
                        Else
                            dtgridExport.Item(dtgridExport.Columns("PlayerIP").Index, RowIndex).Value = "IP never reported."
                        End If
                    End If
                    If chkVersion.Checked Then
                        If child.Item("active").ToString = "UNKNOWN" Then
                            dtgridExport.Item(dtgridExport.Columns("PlayerVersion").Index, RowIndex).Value = ""
                        Else
                            temp = GetStuffs(Scairim.token, Scairim.apiToken, Scairim.LinkCM & "/api/rest/players/" & child.Item("id").ToString & "/version")
                            If InStr(temp, "(401)") > 0 Then
                                result = "Sessione scaduta, per favore rifai il Login."
                                Exit Sub
                            End If
                            If Not InStr(temp, "releaseString") = 0 Then
                                Json2 = JObject.Parse(temp)
                                dtgridExport.Item(dtgridExport.Columns("PlayerVersion").Index, RowIndex).Value = Json2.Item("releaseString").ToString
                            Else
                                dtgridExport.Item(dtgridExport.Columns("PlayerVersion").Index, RowIndex).Value = "Software version never reported."
                            End If
                        End If
                    End If
                    If chkGroups.Checked Then
                        temp = ""
                        If Not child.Item("playergroups") Is Nothing Then
                            For Each group In child.Item("playergroups").Children()
                                temp = AddTextComma(temp, group.Item("name").ToString())
                            Next
                        End If
                        dtgridExport.Item(dtgridExport.Columns("PlayerGroups").Index, RowIndex).Value = temp
                    End If
                    If chkChannel.Checked Then
                        temp = ""
                        If child.Item("playerDisplays").HasValues Then
                            For Each playerdisplay In child.Item("playerDisplays").Children
                                If playerdisplay.Item("channel") IsNot Nothing Then
                                    temp = AddTextComma(temp, playerdisplay.Item("channel").Item("name").ToString, False)
                                End If
                            Next
                        End If
                        dtgridExport.Item(dtgridExport.Columns("Channel").Index, RowIndex).Value = temp
                    End If
                    If chkFrameset.Checked Then
                        temp = ""
                        If child.Item("playerDisplays").HasValues Then
                            For Each playerdisplay In child.Item("playerDisplays").Children
                                If playerdisplay.Item("channel") IsNot Nothing Then
                                    check = GetStuffs(Scairim.token, Scairim.apiToken, Scairim.LinkCM & "/api/rest/channels/" &
                                                      playerdisplay.Item("channel").Item("id").ToString & "?fields=frameset")
                                    If InStr(check, "(401)") > 0 Then
                                        result = "Sessione scaduta, per favore rifai il Login."
                                        Exit Sub
                                    End If
                                    Json2 = JObject.Parse(check)
                                    temp = AddTextComma(temp, Json2.Item("frameset").Item("width").ToString & "x" &
                                                        Json2.Item("frameset").Item("height").ToString, False)
                                End If
                            Next
                        End If
                        dtgridExport.Item(dtgridExport.Columns("Frameset").Index, RowIndex).Value = temp
                    End If
                    If chkFrames.Checked Then
                        temp = ""
                        temp2 = ""
                        If child.Item("playerDisplays").HasValues Then
                            For Each playerdisplay In child.Item("playerDisplays").Children
                                If playerdisplay.Item("channel") IsNot Nothing Then
                                    check = GetStuffs(Scairim.token, Scairim.apiToken, Scairim.LinkCM & "/api/rest/channels/" &
                                                      playerdisplay.Item("channel").Item("id").ToString & "?fields=frameset")
                                    If InStr(check, "(401)") > 0 Then
                                        result = "Sessione scaduta, per favore rifai il Login."
                                        Exit Sub
                                    End If
                                    Json2 = JObject.Parse(check)
                                    If Json2.Item("frameset").Item("frames").HasValues Then
                                        For Each frame In Json2.Item("frameset").Item("frames").Children()
                                            If chkFramesWithRes.Checked Then
                                                temp2 = " " & frame.Item("width").ToString & "x" & frame.Item("height").ToString
                                            End If
                                            If frame.Item("hidden").ToString = True Then
                                                temp = AddTextComma(temp, frame.Item("name").ToString & temp2 & "(hidden)", False)
                                            Else
                                                temp = AddTextComma(temp, frame.Item("name").ToString & temp2, False)
                                            End If
                                        Next
                                    End If
                                End If
                            Next
                        End If
                        dtgridExport.Item(dtgridExport.Columns("Frames").Index, RowIndex).Value = temp
                    End If
                    If chkStatus.Checked Then
                        dtgridExport.Item(dtgridExport.Columns("Status").Index, RowIndex).Value = child.Item("active").ToString
                    End If
                    If chkLastHearthbeat.Checked Then
                        check = GetStuffs(Scairim.token, Scairim.apiToken, Scairim.LinkCM & "/api/rest/players/" & child.Item("id").ToString & "/state")
                        If InStr(check, "(401)") > 0 Then
                            result = "Sessione scaduta, per favore rifai il Login."
                            Exit Sub
                        End If
                        Json2 = JObject.Parse(check)
                        If Json2.Item("state") = "NOT_REPORTED" Then
                            dtgridExport.Item(dtgridExport.Columns("LastReported").Index, RowIndex).Value = "Never Reported"
                        Else
                            dtgridExport.Item(dtgridExport.Columns("LastReported").Index, RowIndex).Value = Json2.Item("lastReportedTimestamp").ToString
                        End If
                    End If
                    If chkPlayerSerial.Checked Then
                        check = GetStuffs(Scairim.token, Scairim.apiToken, Split(Scairim.LinkCM, "/")(0) & "//" & Split(Scairim.LinkCM, "/")(2) & "/" &
                                          "player-monitor/extapi/getPlayer.do?uuid=" & child.Item("uuid").ToString)
                        dtgridExport.Item(dtgridExport.Columns("PlayerSerialNumber").Index, RowIndex).Value = ""
                        If InStr(check, "(404)") = 0 Then
                            Json2 = JObject.Parse(check)
                            If Json2.ContainsKey("hostname") Then
                                Dim pattern As String = "Serial:(.)+\s- OS"
                                Dim text As String = Json2.Item("hostname").ToString
                                Dim matches As MatchCollection = Regex.Matches(text, pattern, RegexOptions.IgnoreCase)
                                If matches.Count <> 0 Then
                                    dtgridExport.Item(dtgridExport.Columns("PlayerSerialNumber").Index, RowIndex).Value =
                                        Trim(Replace(Replace(matches(0).Value, "Serial:", ""), "- OS", ""))
                                End If
                            End If
                        End If
                    End If
                    If chkMonitorSerial.Checked Then
                        check = GetStuffs(Scairim.token, Scairim.apiToken, Split(Scairim.LinkCM, "/")(0) & "//" & Split(Scairim.LinkCM, "/")(2) &
                                          "/" & "player-monitor/extapi/getPlayer.do?uuid=" & child.Item("uuid").ToString)
                        temp = ""
                        If InStr(check, "(404)") = 0 Then
                            Json2 = JObject.Parse(check)
                            If Json2.ContainsKey("displays") Then
                                For Each display In Json2.Item("displays").Children()
                                    temp = AddTextComma(temp, display.Item("serialNumber").ToString)
                                Next
                            End If
                        End If
                        dtgridExport.Item(dtgridExport.Columns("MonitorSerialNumber").Index, RowIndex).Value = temp
                    End If
                    If lbSelectedMetadata.Items.Count > 0 Then
                        If child.Item("metadataValue") IsNot Nothing AndAlso child.Item("metadataValue").HasValues Then
                            For Each metadato In child.Item("metadataValue").Children
                                If lbSelectedMetadata.FindString(metadato.Item("playerMetadata").Item("name").ToString) >= 0 Then
                                    If metadato.Item("playerMetadata").Item("valueType").ToString = "PICKLIST" Then
                                        For Each value In metadato.Item("playerMetadata").Item("predefinedValues").Children()
                                            If value.Item("id").ToString = metadato.Item("value").ToString Then
                                                dtgridExport.Item(dtgridExport.Columns(metadato.Item("playerMetadata").Item("name").ToString).Index,
                                                                  RowIndex).Value = value.Item("value").ToString
                                            End If
                                        Next
                                    End If
                                    If metadato.Item("playerMetadata").Item("valueType").ToString = "ANY" Then
                                        dtgridExport.Item(dtgridExport.Columns(metadato.Item("playerMetadata").Item("name").ToString).Index,
                                                          RowIndex).Value = metadato.Item("value").ToString
                                    End If
                                End If
                            Next
                        End If
                        For Each Meta In lbSelectedMetadata.Items
                            If dtgridExport.Item(dtgridExport.Columns(Meta).Index, RowIndex).Value = "" Then
                                dtgridExport.Item(dtgridExport.Columns(Meta).Index, RowIndex).Value = "Not Set"
                            End If
                        Next
                        'For Each Meta In lbSelectedMetadata.Items
                        'Dim Jtemp As JToken = child.SelectToken("$.playerMetadata[?(@.name == '" & Meta.ToString & "')]")
                        'If  Then
                        '    dtgridExport.Item(dtgridExport.Columns(Meta).Index, RowIndex).Value =
                        '    child.SelectToken("$.playerMetadata[?(@.name == '" & Meta.ToString & "')]").ToString()
                        'End If
                        'Next
                    End If
                    Contatore += 1
                Next
            End If
        Else
            result = "Niente Estrazione..."
            Exit Sub
        End If
        result = "Estrazione Completata!!!"
    End Sub

End Class