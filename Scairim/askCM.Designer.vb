﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AskCM
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AskCM))
        Me.txtOut = New System.Windows.Forms.TextBox()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.txtIn = New System.Windows.Forms.TextBox()
        Me.lblApiRest = New System.Windows.Forms.Label()
        Me.btnAsk = New System.Windows.Forms.Button()
        Me.btnGetStuffs = New System.Windows.Forms.Button()
        Me.lblGetStuffs = New System.Windows.Forms.Label()
        Me.txtGetStuffs = New System.Windows.Forms.TextBox()
        Me.txtJSON = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnCreate = New System.Windows.Forms.Button()
        Me.btnUpdate = New System.Windows.Forms.Button()
        Me.btnGetStuffsBearer = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtBearerURL = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtBearerToken = New System.Windows.Forms.TextBox()
        Me.btnUpdateStuffsBearer = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'txtOut
        '
        Me.txtOut.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.txtOut.Location = New System.Drawing.Point(208, 10)
        Me.txtOut.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txtOut.MaxLength = 0
        Me.txtOut.Multiline = True
        Me.txtOut.Name = "txtOut"
        Me.txtOut.ReadOnly = True
        Me.txtOut.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtOut.Size = New System.Drawing.Size(503, 447)
        Me.txtOut.TabIndex = 0
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(61, 4)
        '
        'txtIn
        '
        Me.txtIn.Location = New System.Drawing.Point(9, 29)
        Me.txtIn.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txtIn.Name = "txtIn"
        Me.txtIn.Size = New System.Drawing.Size(195, 20)
        Me.txtIn.TabIndex = 2
        '
        'lblApiRest
        '
        Me.lblApiRest.AutoSize = True
        Me.lblApiRest.Location = New System.Drawing.Point(7, 13)
        Me.lblApiRest.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblApiRest.Name = "lblApiRest"
        Me.lblApiRest.Size = New System.Drawing.Size(62, 13)
        Me.lblApiRest.TabIndex = 3
        Me.lblApiRest.Text = "/api/rest/..."
        '
        'btnAsk
        '
        Me.btnAsk.Location = New System.Drawing.Point(67, 64)
        Me.btnAsk.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.btnAsk.Name = "btnAsk"
        Me.btnAsk.Size = New System.Drawing.Size(72, 34)
        Me.btnAsk.TabIndex = 4
        Me.btnAsk.Text = "Ask"
        Me.btnAsk.UseVisualStyleBackColor = True
        '
        'btnGetStuffs
        '
        Me.btnGetStuffs.Location = New System.Drawing.Point(67, 168)
        Me.btnGetStuffs.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.btnGetStuffs.Name = "btnGetStuffs"
        Me.btnGetStuffs.Size = New System.Drawing.Size(72, 34)
        Me.btnGetStuffs.TabIndex = 7
        Me.btnGetStuffs.Text = "GetStuffs"
        Me.btnGetStuffs.UseVisualStyleBackColor = True
        '
        'lblGetStuffs
        '
        Me.lblGetStuffs.AutoSize = True
        Me.lblGetStuffs.Location = New System.Drawing.Point(7, 118)
        Me.lblGetStuffs.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblGetStuffs.Name = "lblGetStuffs"
        Me.lblGetStuffs.Size = New System.Drawing.Size(47, 13)
        Me.lblGetStuffs.TabIndex = 6
        Me.lblGetStuffs.Text = "http://..."
        '
        'txtGetStuffs
        '
        Me.txtGetStuffs.Location = New System.Drawing.Point(9, 134)
        Me.txtGetStuffs.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txtGetStuffs.Name = "txtGetStuffs"
        Me.txtGetStuffs.Size = New System.Drawing.Size(195, 20)
        Me.txtGetStuffs.TabIndex = 5
        '
        'txtJSON
        '
        Me.txtJSON.Location = New System.Drawing.Point(9, 231)
        Me.txtJSON.Multiline = True
        Me.txtJSON.Name = "txtJSON"
        Me.txtJSON.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtJSON.Size = New System.Drawing.Size(195, 179)
        Me.txtJSON.TabIndex = 10
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 215)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(35, 13)
        Me.Label2.TabIndex = 11
        Me.Label2.Text = "JSON"
        '
        'btnCreate
        '
        Me.btnCreate.Location = New System.Drawing.Point(11, 423)
        Me.btnCreate.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.btnCreate.Name = "btnCreate"
        Me.btnCreate.Size = New System.Drawing.Size(72, 34)
        Me.btnCreate.TabIndex = 12
        Me.btnCreate.Text = "Create"
        Me.btnCreate.UseVisualStyleBackColor = True
        '
        'btnUpdate
        '
        Me.btnUpdate.Location = New System.Drawing.Point(117, 423)
        Me.btnUpdate.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.btnUpdate.Name = "btnUpdate"
        Me.btnUpdate.Size = New System.Drawing.Size(72, 34)
        Me.btnUpdate.TabIndex = 13
        Me.btnUpdate.Text = "Update"
        Me.btnUpdate.UseVisualStyleBackColor = True
        '
        'btnGetStuffsBearer
        '
        Me.btnGetStuffsBearer.Location = New System.Drawing.Point(774, 118)
        Me.btnGetStuffsBearer.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.btnGetStuffsBearer.Name = "btnGetStuffsBearer"
        Me.btnGetStuffsBearer.Size = New System.Drawing.Size(98, 34)
        Me.btnGetStuffsBearer.TabIndex = 16
        Me.btnGetStuffsBearer.Text = "GetStuffsBearer"
        Me.btnGetStuffsBearer.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(723, 13)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(47, 13)
        Me.Label1.TabIndex = 15
        Me.Label1.Text = "http://..."
        '
        'txtBearerURL
        '
        Me.txtBearerURL.Location = New System.Drawing.Point(725, 29)
        Me.txtBearerURL.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txtBearerURL.Name = "txtBearerURL"
        Me.txtBearerURL.Size = New System.Drawing.Size(195, 20)
        Me.txtBearerURL.TabIndex = 14
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(723, 56)
        Me.Label3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(47, 13)
        Me.Label3.TabIndex = 18
        Me.Label3.Text = "Bearer..."
        '
        'txtBearerToken
        '
        Me.txtBearerToken.Location = New System.Drawing.Point(725, 72)
        Me.txtBearerToken.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txtBearerToken.Name = "txtBearerToken"
        Me.txtBearerToken.Size = New System.Drawing.Size(195, 20)
        Me.txtBearerToken.TabIndex = 17
        '
        'btnUpdateStuffsBearer
        '
        Me.btnUpdateStuffsBearer.Location = New System.Drawing.Point(763, 168)
        Me.btnUpdateStuffsBearer.Margin = New System.Windows.Forms.Padding(2)
        Me.btnUpdateStuffsBearer.Name = "btnUpdateStuffsBearer"
        Me.btnUpdateStuffsBearer.Size = New System.Drawing.Size(121, 34)
        Me.btnUpdateStuffsBearer.TabIndex = 19
        Me.btnUpdateStuffsBearer.Text = "UpdateStuffsBearer"
        Me.btnUpdateStuffsBearer.UseVisualStyleBackColor = True
        '
        'AskCM
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(928, 468)
        Me.Controls.Add(Me.btnUpdateStuffsBearer)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtBearerToken)
        Me.Controls.Add(Me.btnGetStuffsBearer)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtBearerURL)
        Me.Controls.Add(Me.btnUpdate)
        Me.Controls.Add(Me.btnCreate)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtJSON)
        Me.Controls.Add(Me.btnGetStuffs)
        Me.Controls.Add(Me.lblGetStuffs)
        Me.Controls.Add(Me.txtGetStuffs)
        Me.Controls.Add(Me.btnAsk)
        Me.Controls.Add(Me.lblApiRest)
        Me.Controls.Add(Me.txtIn)
        Me.Controls.Add(Me.txtOut)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.MaximizeBox = False
        Me.MinimumSize = New System.Drawing.Size(735, 385)
        Me.Name = "AskCM"
        Me.Text = "Test API"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txtOut As TextBox
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents txtIn As TextBox
    Friend WithEvents lblApiRest As Label
    Friend WithEvents btnAsk As Button
    Friend WithEvents btnGetStuffs As Button
    Friend WithEvents lblGetStuffs As Label
    Friend WithEvents txtGetStuffs As TextBox
    Friend WithEvents txtJSON As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents btnCreate As Button
    Friend WithEvents btnUpdate As Button
    Friend WithEvents btnGetStuffsBearer As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents txtBearerURL As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents txtBearerToken As TextBox
    Friend WithEvents btnUpdateStuffsBearer As Button
End Class
