﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmDeleteOldStuffs
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.txtDate = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtStuffsToDelete = New System.Windows.Forms.TextBox()
        Me.chkPlaylists = New System.Windows.Forms.CheckBox()
        Me.chkMedia = New System.Windows.Forms.CheckBox()
        Me.btnCheck = New System.Windows.Forms.Button()
        Me.btnDelete = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'txtDate
        '
        Me.txtDate.Location = New System.Drawing.Point(392, 17)
        Me.txtDate.Name = "txtDate"
        Me.txtDate.Size = New System.Drawing.Size(143, 22)
        Me.txtDate.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 17)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(377, 17)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Cancellare playlist o contenuti piu' vecchi di...(dd/mm/yyyy)"
        '
        'txtStuffsToDelete
        '
        Me.txtStuffsToDelete.Location = New System.Drawing.Point(12, 96)
        Me.txtStuffsToDelete.Multiline = True
        Me.txtStuffsToDelete.Name = "txtStuffsToDelete"
        Me.txtStuffsToDelete.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtStuffsToDelete.Size = New System.Drawing.Size(523, 185)
        Me.txtStuffsToDelete.TabIndex = 2
        '
        'chkPlaylists
        '
        Me.chkPlaylists.AutoSize = True
        Me.chkPlaylists.Location = New System.Drawing.Point(12, 55)
        Me.chkPlaylists.Name = "chkPlaylists"
        Me.chkPlaylists.Size = New System.Drawing.Size(81, 21)
        Me.chkPlaylists.TabIndex = 3
        Me.chkPlaylists.Text = "Playlists"
        Me.chkPlaylists.UseVisualStyleBackColor = True
        '
        'chkMedia
        '
        Me.chkMedia.AutoSize = True
        Me.chkMedia.Location = New System.Drawing.Point(118, 55)
        Me.chkMedia.Name = "chkMedia"
        Me.chkMedia.Size = New System.Drawing.Size(68, 21)
        Me.chkMedia.TabIndex = 4
        Me.chkMedia.Text = "Media"
        Me.chkMedia.UseVisualStyleBackColor = True
        '
        'btnCheck
        '
        Me.btnCheck.Location = New System.Drawing.Point(231, 287)
        Me.btnCheck.Name = "btnCheck"
        Me.btnCheck.Size = New System.Drawing.Size(104, 31)
        Me.btnCheck.TabIndex = 5
        Me.btnCheck.Text = "Check"
        Me.btnCheck.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Location = New System.Drawing.Point(12, 287)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(104, 31)
        Me.btnDelete.TabIndex = 6
        Me.btnDelete.Text = "Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        Me.btnDelete.Visible = False
        '
        'frmDeleteOldStuffs
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(547, 329)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.btnCheck)
        Me.Controls.Add(Me.chkMedia)
        Me.Controls.Add(Me.chkPlaylists)
        Me.Controls.Add(Me.txtStuffsToDelete)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtDate)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmDeleteOldStuffs"
        Me.Text = "Delete Old Stuffs (Pulizia CM)"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txtDate As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents txtStuffsToDelete As TextBox
    Friend WithEvents chkPlaylists As CheckBox
    Friend WithEvents chkMedia As CheckBox
    Friend WithEvents btnCheck As Button
    Friend WithEvents btnDelete As Button
End Class
