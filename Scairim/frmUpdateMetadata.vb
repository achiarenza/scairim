﻿Imports Newtonsoft.Json.Linq

Public Class frmUpdateMetadata

    Private Sub frmUpdateMetadata_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Icon = Scairim.Icon
        txtMetadato.Text = ""
        txtValue.Text = ""
    End Sub

    Private Sub btnGo_Click(sender As Object, e As EventArgs) Handles btnGo.Click
        Dim result As String = ""
        If (Scairim.logged) Then
            Utilities.timeout = 120000
            Logga("Aggiorno/Creo il Metadato...")
            Try
                Loading("show")
                result = CreateUpdatePicklist()
                Loading("close")
            Catch ex As Exception
                Logga("ERROR - Aggiornamento\Creazione Metadato: " & ex.ToString)
            End Try
            Logga(result)
            MsgBox(result)
        Else
            Logga("Se prima non fai il login su un CM non posso fare nulla :)")
        End If
    End Sub

    Function CreateUpdatePicklist()
        Dim oJsonR As JObject
        Dim result = GetStuffs(Scairim.token, Scairim.apiToken, Scairim.LinkCM & "/api/rest/playerMetadata?limit=0&search=" & txtMetadato.Text)
        Dim MetadatoId As String = ""
        Dim MetadatoType As String = ""
        Dim MetadatoValue As String = ""
        Dim ValuePicklist As Integer = 0
        Dim lsValue As List(Of String) = New List(Of String)
        For Each line In txtValue.Lines
            lsValue.Add(line)
        Next
        If InStr(result, "ERROR") > 0 Then
            CreateUpdatePicklist = "ERROR - CreateUpdatePicklist: Ricerca Metadato sul CM fallita. Controlla di aver fatto il Login correttamente."
        Else
            oJsonR = JObject.Parse(result)
            If oJsonR.ContainsKey("list") Then
                For Each child In oJsonR.Item("list").Children()
                    If child.Item("name").ToString = "Player." & txtMetadato.Text Then
                        MetadatoId = child.Item("id").ToString
                        MetadatoType = child.Item("datatype").ToString
                        MetadatoValue = child.Item("valueType").ToString
                    End If
                Next
                If MetadatoId = "" Or MetadatoType = "" Or MetadatoValue = "" Then
                    CreateUpdatePicklist = CreateMetadata(txtMetadato.Text, "PICKLIST", "STRING")
                    oJsonR = JObject.Parse(CreateUpdatePicklist)
                    CreateUpdatePicklist = AddPicklistValue(oJsonR.Item("id").ToString, lsValue)
                    If InStr(CreateUpdatePicklist, "ERROR") > 0 Then
                        CreateUpdatePicklist = "ERROR - Controlla che non ci siano doppioni nella lista."
                    Else
                        CreateUpdatePicklist = "OK"
                    End If
                Else
                    If MetadatoValue = "PICKLIST" Then
                        CreateUpdatePicklist = AddPicklistValue(MetadatoId, lsValue)
                        If InStr(CreateUpdatePicklist, "ERROR") > 0 Then
                            CreateUpdatePicklist = "ERROR - Controlla che non ci siano doppioni nella lista."
                        Else
                            CreateUpdatePicklist = "OK"
                        End If
                    Else
                        CreateUpdatePicklist = "Metadato non Picklist"
                    End If
                End If
            Else
                CreateUpdatePicklist = CreateMetadata(txtMetadato.Text, "PICKLIST", "STRING")
                oJsonR = JObject.Parse(CreateUpdatePicklist)
                CreateUpdatePicklist = AddPicklistValue(oJsonR.Item("id").ToString, lsValue)
                If InStr(CreateUpdatePicklist, "ERROR") > 0 Then
                    CreateUpdatePicklist = "ERROR - Controlla che non ci siano doppioni nella lista."
                Else
                    CreateUpdatePicklist = "OK"
                End If
            End If
        End If
    End Function

End Class