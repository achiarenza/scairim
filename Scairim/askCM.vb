﻿Public Class AskCM
    Private Sub AskCM_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Icon = Scairim.Icon
        lblApiRest.Text = "http://(hostname)/cm/api/rest/..."
        lblGetStuffs.Text = "http://(hostname)/..."
    End Sub

    Private Sub AskCM_Closing(sender As Object, e As EventArgs) Handles MyBase.Closing
        txtOut.Clear()
    End Sub

    Private Sub BtnAsk_Click(sender As Object, e As EventArgs) Handles btnAsk.Click
        Logga("askCM - Richiedo: " & Scairim.LinkCM & "/api/rest/" & txtIn.Text)
        txtOut.Text = GetStuffs(Scairim.token, Scairim.apiToken, Scairim.LinkCM & "/api/rest/" & txtIn.Text)
        If InStr(txtOut.Text, "(401)") > 0 Then
            txtOut.Text = "Sessione scaduta, per favore riesegui il login"
        End If
    End Sub

    Private Sub btnGetStuffs_Click(sender As Object, e As EventArgs) Handles btnGetStuffs.Click
        If Scairim.LinkCM <> "" Then
            Logga("askCM - Richiedo: " & Split(Scairim.LinkCM, "/")(0) & "//" & Split(Scairim.LinkCM, "/")(2) & "/" & txtGetStuffs.Text)
            txtOut.Text = GetStuffs(Scairim.token, Scairim.apiToken,
                                    Split(Scairim.LinkCM, "/")(0) & "//" & Split(Scairim.LinkCM, "/")(2) & "/" & txtGetStuffs.Text)
        Else
            Logga("Effettua il login su un CM.")
        End If
    End Sub

    Private Sub btnCreate_Click(sender As Object, e As EventArgs) Handles btnCreate.Click
        txtOut.Text = ""
        Try
            If Scairim.LinkCM <> "" Then
                Logga("askCM - Eseguo POST: " & Scairim.LinkCM & "/api/rest/" & txtIn.Text)
                txtOut.Text = CreateStuffs(Scairim.token, Scairim.apiToken, txtJSON.Text, Scairim.LinkCM & "/api/rest/" & txtIn.Text)
            Else
                Logga("Effettua il login su un CM.")
            End If
        Catch ex As Exception
            Logga(ex.Message)
        End Try
    End Sub

    Private Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        If Scairim.LinkCM <> "" Then
            Logga("askCM - Eseguo PUT: " & Scairim.LinkCM & "/api/rest/" & txtIn.Text)
            txtOut.Text = UpdateStuffs(Scairim.token, Scairim.apiToken, txtJSON.Text, Scairim.LinkCM & "/api/rest/" & txtIn.Text)
        Else
            Logga("Effettua il login su un CM.")
        End If
    End Sub

    Private Sub btnGetStuffsBearer_Click(sender As Object, e As EventArgs) Handles btnGetStuffsBearer.Click
        Logga("askCM - Richiedo: " & txtBearerURL.Text)
        txtOut.Text = GetStuffsBearer(txtBearerToken.Text, txtBearerURL.Text)
    End Sub

    Private Sub btnUpdateStuffsBearer_Click(sender As Object, e As EventArgs) Handles btnUpdateStuffsBearer.Click
        Logga("askCM - Eseguo PUT: " & txtBearerURL.Text)
        txtOut.Text = UpdateStuffsBearer(Scairim.tokenTeamviewer, txtJSON.Text, txtBearerURL.Text)
    End Sub
End Class