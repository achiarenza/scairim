﻿Imports Excel = Microsoft.Office.Interop.Excel
Imports Newtonsoft.Json.Linq

Public Class frmExtractionByPlaylists

    Dim Json, Json2, Json3 As JObject
    Dim result As String

    Private Sub frmExtractionByPlaylists_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Icon = Scairim.Icon
        btnEstraiExcel.Visible = False
        chkItemCondition.Enabled = False
        chkItemType.Enabled = False
        chkValidDate.Enabled = False
        chkItemOrder.Enabled = False
        dtgridExport.Rows.Clear()
        dtgridExport.Columns.Clear()
    End Sub

    Private Sub btnEstraiExcel_Click(sender As Object, e As EventArgs) Handles btnEstraiExcel.Click
        EstraiInExcel(dtgridExport)
    End Sub

    Private Sub btnGo_Click(sender As Object, e As EventArgs) Handles btnGo.Click
        If (Scairim.logged) Then
            Utilities.timeout = 120000
            Logga("Faccio un'estrazione!!!")
            Try
                Loading("show")
                result = Estrai()
                Loading("close")
            Catch ex As Exception
                Logga("ERROR - Estrazione: " & ex.ToString)
            End Try
            Logga(result)
            MsgBox(result)
            btnEstraiExcel.Visible = True
        Else
            Logga("Se prima non fai il login su un CM non posso estrarre nulla :)")
        End If
    End Sub

    Private Sub chkItemList_CheckedChanged(sender As Object, e As EventArgs) Handles chkItemList.CheckedChanged
        If chkItemList.Checked Then
            chkItemType.Enabled = True
            chkItemCondition.Enabled = True
            chkValidDate.Enabled = True
            chkItemOrder.Enabled = True
        Else
            chkItemType.Checked = False
            chkItemCondition.Checked = False
            chkValidDate.Checked = False
            chkItemOrder.Checked = False
            chkItemType.Enabled = False
            chkItemCondition.Enabled = False
            chkValidDate.Enabled = False
            chkItemOrder.Enabled = False
        End If
    End Sub

    Private Function Estrai()

        Dim toExtract As String = ""
        Dim RowIndex As Integer
        Dim ColumnCount As Integer = 0
        Dim clIndxPlaylistName As Integer
        Dim clIndxItemName As Integer
        Dim clIndxItemType As Integer
        Dim clIndxItemOrder As Integer
        Dim clIndxConditionsToMeet As Integer
        Dim clIndxConditionType As Integer
        Dim clIndxConditionName As Integer
        Dim clIndxConditionValue As Integer
        Dim clIndxStartValidDate As Integer
        Dim clIndxEndValidDate As Integer
        Dim clIndxIsValid As Integer
        Dim temp As String = ""
        Dim temp2 As String = ""
        Dim check As String
        Dim NumChild As Integer
        Dim Contatore As Integer
        Dim Index As Integer = 0

        dtgridExport.Rows.Clear()
        dtgridExport.Columns.Clear()

        dtgridExport.Columns.Add("PlaylistName", "Playlist Name")
        toExtract = AddTextComma(toExtract, "name", False)
        clIndxPlaylistName = ColumnCount
        ColumnCount = ColumnCount + 1

        If chkItemType.Checked Then
            dtgridExport.Columns.Add("ItemType", "Item Type")
            clIndxItemType = ColumnCount
            ColumnCount = ColumnCount + 1
        End If

        If chkItemList.Checked Then
            dtgridExport.Columns.Add("ItemName", "Item Name")
            clIndxItemName = ColumnCount
            ColumnCount = ColumnCount + 1
        End If

        If chkItemOrder.Checked Then
            dtgridExport.Columns.Add("ItemOrder", "Item Order")
            clIndxItemOrder = ColumnCount
            ColumnCount = ColumnCount + 1
        End If

        If chkItemCondition.Checked Then
            dtgridExport.Columns.Add("ItemConditionsToMeet", "Conditions To Meet")
            clIndxConditionsToMeet = ColumnCount
            ColumnCount = ColumnCount + 1
            dtgridExport.Columns.Add("ItemConditionType", "Condition Type")
            clIndxConditionType = ColumnCount
            ColumnCount = ColumnCount + 1
            dtgridExport.Columns.Add("ItemConditionName", "Condition Name")
            clIndxConditionName = ColumnCount
            ColumnCount = ColumnCount + 1
            dtgridExport.Columns.Add("ItemConditionValue", "Condition Value")
            clIndxConditionValue = ColumnCount
            ColumnCount = ColumnCount + 1
        End If

        If chkValidDate.Checked Then
            dtgridExport.Columns.Add("ItemStartValid", "Start Valid Date")
            clIndxStartValidDate = ColumnCount
            ColumnCount = ColumnCount + 1
            dtgridExport.Columns.Add("ItemEndValid", "Start End Date")
            clIndxEndValidDate = ColumnCount
            ColumnCount = ColumnCount + 1
            dtgridExport.Columns.Add("Status", "Status")
            clIndxIsValid = ColumnCount
            ColumnCount = ColumnCount + 1
        End If

        check = GetStuffs(Scairim.token, Scairim.apiToken, Scairim.LinkCM & "/api/rest/playlists/all?limit=9999&fields=" & toExtract)
        If InStr(check, "(401)") > 0 Then
            Return "Sessione scaduta, per favore rifai il Login."
        End If
        Json = JObject.Parse(check)
        If Json.ContainsKey("list") Then
            NumChild = CInt(Json.Item("count").ToString)
            Contatore = 0
            For Each child In Json.Item("list").Children()
                If Contatore Mod 10 = 0 Then
                    Loading(CInt((Contatore * 100) / NumChild))
                End If
                If Not txtName.Text = "" Then
                    If chkUpLow.Checked Then
                        If InStr(child.Item("name").ToString, txtName.Text) = 0 Then
                            Contatore = Contatore + 1
                            Continue For
                        End If
                    Else
                        If InStr(child.Item("name").ToString.ToLower, txtName.Text.ToLower) = 0 Then
                            Contatore = Contatore + 1
                            Continue For
                        End If
                    End If
                End If
                If chkItemList.Checked Then
                    check = GetStuffs(Scairim.token, Scairim.apiToken, Scairim.LinkCM & "/api/rest/playlists/" & child.Item("id").ToString & "/items?limits=9999")
                    Json2 = JObject.Parse(check)
                    If Json2.ContainsKey("list") Then
                        For Each item In Json2.Item("list").Children()
                            If chkItemCondition.Checked Then
                                If item.Item("conditions") Is Nothing Then
                                    RowIndex = dtgridExport.Rows.Add()
                                    dtgridExport.Item(clIndxPlaylistName, RowIndex).Value = child.Item("name").ToString
                                    temp = item.Item("playlistItemType").ToString
                                    If chkItemType.Checked Then
                                        dtgridExport.Item(clIndxItemType, RowIndex).Value = temp
                                    End If
                                    If temp = "MEDIA_ITEM" Or temp = "MESSAGE" Then
                                        dtgridExport.Item(clIndxItemName, RowIndex).Value = item.Item("media").Item("name").ToString
                                    ElseIf temp = "SUB_PLAYLIST" Then
                                        dtgridExport.Item(clIndxItemName, RowIndex).Value = item.Item("subplaylist").Item("name").ToString
                                    Else
                                        dtgridExport.Item(clIndxItemName, RowIndex).Value = ""
                                    End If
                                    If chkItemOrder.Checked Then
                                        dtgridExport.Item(clIndxItemOrder, RowIndex).Value = item.Item("sortOrder").ToString
                                    End If
                                    If chkValidDate.Checked Then
                                        If Not item.Item("startValidDate") Is Nothing Then
                                            dtgridExport.Item(clIndxStartValidDate, RowIndex).Value = item.Item("startValidDate").ToString
                                        End If
                                        If item.Item("endValidDate") Is Nothing Then
                                            dtgridExport.Item(clIndxEndValidDate, RowIndex).Value = ""
                                            dtgridExport.Item(clIndxIsValid, RowIndex).Value = ""
                                        Else
                                            temp2 = item.Item("endValidDate").ToString
                                            dtgridExport.Item(clIndxEndValidDate, RowIndex).Value = temp2
                                            If item.Item("status") Is Nothing Then
                                                dtgridExport.Item(clIndxIsValid, RowIndex).Value = ""
                                            ElseIf InStr(item.Item("status").ToString, "EXPIRED") > 0 Then
                                                dtgridExport.Item(clIndxIsValid, RowIndex).Value = "TIMESCHEDULE_EXPIRED"
                                            End If
                                        End If
                                    End If
                                Else
                                    For Each condition In item.Item("conditions")
                                        RowIndex = dtgridExport.Rows.Add()
                                        dtgridExport.Item(clIndxPlaylistName, RowIndex).Value = child.Item("name").ToString
                                        temp = item.Item("playlistItemType").ToString
                                        If chkItemType.Checked Then
                                            dtgridExport.Item(clIndxItemType, RowIndex).Value = temp
                                        End If
                                        If temp = "MEDIA_ITEM" Or temp = "MESSAGE" Then
                                            dtgridExport.Item(clIndxItemName, RowIndex).Value = item.Item("media").Item("name").ToString
                                        ElseIf temp = "SUB_PLAYLIST" Then
                                            dtgridExport.Item(clIndxItemName, RowIndex).Value = item.Item("subplaylist").Item("name").ToString
                                        Else
                                            dtgridExport.Item(clIndxItemName, RowIndex).Value = ""
                                        End If
                                        If chkItemOrder.Checked Then
                                            dtgridExport.Item(clIndxItemOrder, RowIndex).Value = item.Item("sortOrder").ToString
                                        End If
                                        If chkValidDate.Checked Then
                                            If Not item.Item("startValidDate") Is Nothing Then
                                                dtgridExport.Item(clIndxStartValidDate, RowIndex).Value = item.Item("startValidDate").ToString
                                            End If
                                            If item.Item("endValidDate") Is Nothing Then
                                                dtgridExport.Item(clIndxEndValidDate, RowIndex).Value = ""
                                            Else
                                                temp2 = item.Item("endValidDate").ToString
                                                dtgridExport.Item(clIndxEndValidDate, RowIndex).Value = temp2
                                                If item.Item("status") Is Nothing Then
                                                    dtgridExport.Item(clIndxIsValid, RowIndex).Value = ""
                                                ElseIf InStr(item.Item("status").ToString, "EXPIRED") > 0 Then
                                                    dtgridExport.Item(clIndxIsValid, RowIndex).Value = "TIMESCHEDULE_EXPIRED"
                                                End If
                                            End If
                                        End If
                                        If item.Item("meetAllConditions").ToString = "true" Then
                                            dtgridExport.Item(clIndxConditionsToMeet, RowIndex).Value = "All"
                                        Else
                                            dtgridExport.Item(clIndxConditionsToMeet, RowIndex).Value = "Any"
                                        End If
                                        temp = condition.Item("type").ToString
                                        dtgridExport.Item(clIndxConditionType, RowIndex).Value = temp
                                        If temp = "PLAYER_NAME" Then
                                            dtgridExport.Item(clIndxConditionName, RowIndex).Value = temp & " " & condition.Item("comparator").ToString
                                            check = GetStuffs(Scairim.token, Scairim.apiToken, Scairim.LinkCM & "/api/rest/players/" & condition.Item("value").ToString & "?fields=name")
                                            If InStr(check, "(400)") = 0 Then
                                                Json3 = JObject.Parse(check)
                                                dtgridExport.Item(clIndxConditionValue, RowIndex).Value = Json3.Item("name").ToString
                                            Else
                                                dtgridExport.Item(clIndxConditionValue, RowIndex).Value = "PLAYER_NOT_FOUND"
                                            End If
                                        ElseIf temp = "METADATA" Then
                                                If condition.Item("metadata").Item("datatype").ToString = "BOOLEAN" Then
                                                dtgridExport.Item(clIndxConditionName, RowIndex).Value = condition.Item("metadata").Item("name").ToString
                                                dtgridExport.Item(clIndxConditionValue, RowIndex).Value = condition.Item("comparator").ToString
                                            ElseIf condition.Item("metadata").Item("datatype").ToString = "STRING" Then
                                                dtgridExport.Item(clIndxConditionName, RowIndex).Value = condition.Item("metadata").Item("name").ToString & " " & condition.Item("comparator").ToString
                                                dtgridExport.Item(clIndxConditionValue, RowIndex).Value = condition.Item("value").ToString
                                            End If
                                        End If
                                    Next
                                End If
                            Else
                                RowIndex = dtgridExport.Rows.Add()
                                dtgridExport.Item(clIndxPlaylistName, RowIndex).Value = child.Item("name").ToString
                                temp = item.Item("playlistItemType").ToString
                                If chkItemType.Checked Then
                                    dtgridExport.Item(clIndxItemType, RowIndex).Value = temp
                                End If
                                If temp = "MEDIA_ITEM" Or temp = "MESSAGE" Then
                                    dtgridExport.Item(clIndxItemName, RowIndex).Value = item.Item("media").Item("name").ToString
                                ElseIf temp = "SUB_PLAYLIST" Then
                                    dtgridExport.Item(clIndxItemName, RowIndex).Value = item.Item("subplaylist").Item("name").ToString
                                Else
                                    dtgridExport.Item(clIndxItemName, RowIndex).Value = ""
                                End If
                                If chkItemOrder.Checked Then
                                    dtgridExport.Item(clIndxItemOrder, RowIndex).Value = item.Item("sortOrder").ToString
                                End If
                                If chkValidDate.Checked Then
                                    If Not item.Item("startValidDate") Is Nothing Then
                                        dtgridExport.Item(clIndxStartValidDate, RowIndex).Value = item.Item("startValidDate").ToString
                                    End If
                                    If item.Item("endValidDate") Is Nothing Then
                                        dtgridExport.Item(clIndxEndValidDate, RowIndex).Value = ""
                                    Else
                                        temp2 = item.Item("endValidDate").ToString
                                        dtgridExport.Item(clIndxEndValidDate, RowIndex).Value = temp2
                                        If item.Item("status") Is Nothing Then
                                            dtgridExport.Item(clIndxIsValid, RowIndex).Value = ""
                                        ElseIf InStr(item.Item("status").ToString, "EXPIRED") > 0 Then
                                            dtgridExport.Item(clIndxIsValid, RowIndex).Value = "TIMESCHEDULE_EXPIRED"
                                        End If
                                    End If
                                End If
                            End If
                        Next
                    Else
                        RowIndex = dtgridExport.Rows.Add()
                        dtgridExport.Item(clIndxPlaylistName, RowIndex).Value = child.Item("name").ToString
                    End If
                Else
                    RowIndex = dtgridExport.Rows.Add()
                    dtgridExport.Item(clIndxPlaylistName, RowIndex).Value = child.Item("name").ToString
                End If
                Contatore = Contatore + 1
            Next
        Else
            Return "Errore nel caricamento del Json."
        End If
        Return "Estrazione Completata!!!"
    End Function

End Class