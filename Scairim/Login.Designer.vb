﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Login
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Login))
        Me.Tuser = New System.Windows.Forms.TextBox()
        Me.lNome = New System.Windows.Forms.Label()
        Me.lWelcome = New System.Windows.Forms.Label()
        Me.lPwd = New System.Windows.Forms.Label()
        Me.lLink = New System.Windows.Forms.Label()
        Me.Tpwd = New System.Windows.Forms.TextBox()
        Me.Blogin = New System.Windows.Forms.Button()
        Me.cmbCM = New System.Windows.Forms.ComboBox()
        Me.SuspendLayout()
        '
        'Tuser
        '
        Me.Tuser.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Tuser.Location = New System.Drawing.Point(66, 89)
        Me.Tuser.Margin = New System.Windows.Forms.Padding(2)
        Me.Tuser.Name = "Tuser"
        Me.Tuser.Size = New System.Drawing.Size(213, 20)
        Me.Tuser.TabIndex = 0
        '
        'lNome
        '
        Me.lNome.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lNome.AutoSize = True
        Me.lNome.Location = New System.Drawing.Point(7, 92)
        Me.lNome.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lNome.Name = "lNome"
        Me.lNome.Size = New System.Drawing.Size(55, 13)
        Me.lNome.TabIndex = 1
        Me.lNome.Text = "Username"
        '
        'lWelcome
        '
        Me.lWelcome.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lWelcome.AutoSize = True
        Me.lWelcome.Location = New System.Drawing.Point(79, 19)
        Me.lWelcome.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lWelcome.Name = "lWelcome"
        Me.lWelcome.Size = New System.Drawing.Size(127, 13)
        Me.lWelcome.TabIndex = 2
        Me.lWelcome.Text = "Effettua il login su un CM!"
        '
        'lPwd
        '
        Me.lPwd.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lPwd.AutoSize = True
        Me.lPwd.Location = New System.Drawing.Point(7, 124)
        Me.lPwd.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lPwd.Name = "lPwd"
        Me.lPwd.Size = New System.Drawing.Size(53, 13)
        Me.lPwd.TabIndex = 3
        Me.lPwd.Text = "Password"
        '
        'lLink
        '
        Me.lLink.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lLink.AutoSize = True
        Me.lLink.Location = New System.Drawing.Point(7, 57)
        Me.lLink.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lLink.Name = "lLink"
        Me.lLink.Size = New System.Drawing.Size(64, 13)
        Me.lLink.TabIndex = 4
        Me.lLink.Text = "Indirizzo CM"
        '
        'Tpwd
        '
        Me.Tpwd.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Tpwd.Location = New System.Drawing.Point(66, 124)
        Me.Tpwd.Margin = New System.Windows.Forms.Padding(2)
        Me.Tpwd.Name = "Tpwd"
        Me.Tpwd.Size = New System.Drawing.Size(213, 20)
        Me.Tpwd.TabIndex = 5
        Me.Tpwd.UseSystemPasswordChar = True
        '
        'Blogin
        '
        Me.Blogin.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.Blogin.Location = New System.Drawing.Point(88, 165)
        Me.Blogin.Margin = New System.Windows.Forms.Padding(2)
        Me.Blogin.Name = "Blogin"
        Me.Blogin.Size = New System.Drawing.Size(116, 37)
        Me.Blogin.TabIndex = 7
        Me.Blogin.Text = "Login"
        Me.Blogin.UseVisualStyleBackColor = True
        '
        'cmbCM
        '
        Me.cmbCM.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmbCM.FormattingEnabled = True
        Me.cmbCM.Location = New System.Drawing.Point(74, 55)
        Me.cmbCM.Margin = New System.Windows.Forms.Padding(2)
        Me.cmbCM.Name = "cmbCM"
        Me.cmbCM.Size = New System.Drawing.Size(205, 21)
        Me.cmbCM.TabIndex = 8
        '
        'Login
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(290, 224)
        Me.Controls.Add(Me.cmbCM)
        Me.Controls.Add(Me.Blogin)
        Me.Controls.Add(Me.Tpwd)
        Me.Controls.Add(Me.lLink)
        Me.Controls.Add(Me.lPwd)
        Me.Controls.Add(Me.lWelcome)
        Me.Controls.Add(Me.lNome)
        Me.Controls.Add(Me.Tuser)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.MinimumSize = New System.Drawing.Size(306, 262)
        Me.Name = "Login"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Scairim"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Tuser As TextBox
    Friend WithEvents lNome As Label
    Friend WithEvents lWelcome As Label
    Friend WithEvents lPwd As Label
    Friend WithEvents lLink As Label
    Friend WithEvents Tpwd As TextBox
    Friend WithEvents Blogin As Button
    Friend WithEvents cmbCM As ComboBox
End Class
