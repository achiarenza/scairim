﻿Imports Newtonsoft.Json.Linq

Public Class frmUsers
    Dim Json, JsonW As JObject
    Dim result As String

    Private Sub btnGo_Click(sender As Object, e As EventArgs) Handles btnGo.Click
        If (Scairim.logged) Then
            Utilities.timeout = 120000
            Logga("Faccio un'estrazione!!!")
            Try
                Loading("show")
                Estrai()
                Loading("close")
            Catch ex As Exception
                Logga("ERROR - Estrazione: " & ex.ToString)
            End Try
            Logga(result)
            MsgBox(result)
        Else
            Logga("Se prima non fai il login su un CM non posso estrarre nulla :)")
        End If
    End Sub

    Private Sub frmUsers_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Icon = Scairim.Icon
        btnEnable.Visible = False
        btnDisable.Visible = False
        btnDelete.Visible = False
        dtgridExport.Rows.Clear()
        dtgridExport.Columns.Clear()
    End Sub

    Private Sub Estrai()
        Dim toExtract As String = ""
        Dim RowIndex As Integer
        Dim ColumnCount As Integer = 0
        Dim clIndxUserId As Integer
        Dim clIndxUser As Integer
        Dim clIndxUserName As Integer
        Dim clIndxUserLastname As Integer
        Dim clIndxUserEmail As Integer
        Dim clIndxUserFullName As Integer
        Dim clIndxUserLastLogin As Integer
        Dim clIndxUserRoles As Integer
        Dim clIndxUserEnabled As Integer
        Dim temp As String = ""
        Dim check As String
        Dim NumChild As Integer
        Dim Contatore As Integer

        dtgridExport.Rows.Clear()
        dtgridExport.Columns.Clear()

        dtgridExport.Columns.Add("Id", "ID")
        toExtract = AddTextComma(toExtract, "id", False)
        clIndxUserId = ColumnCount
        ColumnCount = ColumnCount + 1

        dtgridExport.Columns.Add("UserName", "Username")
        toExtract = AddTextComma(toExtract, "username", False)
        clIndxUser = ColumnCount
        ColumnCount = ColumnCount + 1

        If chkFirstname.Checked Then
            dtgridExport.Columns.Add("UserFirstname", "Firstname")
            toExtract = AddTextComma(toExtract, "firstname", False)
            clIndxUserName = ColumnCount
            ColumnCount = ColumnCount + 1
        End If
        If chkLastname.Checked Then
            dtgridExport.Columns.Add("UserLastname", "Lastname")
            toExtract = AddTextComma(toExtract, "lastname", False)
            clIndxUserLastname = ColumnCount
            ColumnCount = ColumnCount + 1
        End If
        If chkEmail.Checked Then
            dtgridExport.Columns.Add("UserEmail", "Email")
            toExtract = AddTextComma(toExtract, "emailaddress", False)
            clIndxUserEmail = ColumnCount
            ColumnCount = ColumnCount + 1
        End If
        If chkFullName.Checked Then
            dtgridExport.Columns.Add("UserFullName", "Full Name")
            toExtract = AddTextComma(toExtract, "name", False)
            clIndxUserFullName = ColumnCount
            ColumnCount = ColumnCount + 1
        End If
        If chkLastLogin.Checked Then
            dtgridExport.Columns.Add("UserLastLogin", "Last Login")
            toExtract = AddTextComma(toExtract, "lastLogin", False)
            clIndxUserLastLogin = ColumnCount
            ColumnCount = ColumnCount + 1
        End If
        If chkRoles.Checked Then
            dtgridExport.Columns.Add("UserRoles", "Roles")
            toExtract = AddTextComma(toExtract, "roles", False)
            clIndxUserRoles = ColumnCount
            ColumnCount = ColumnCount + 1
        End If
        If chkEnabled.Checked Then
            dtgridExport.Columns.Add("UserEnabled", "Enabled")
            toExtract = AddTextComma(toExtract, "enabled", False)
            clIndxUserEnabled = ColumnCount
            ColumnCount = ColumnCount + 1
        End If

        check = GetStuffs(Scairim.token, Scairim.apiToken, Scairim.LinkCM & "/api/rest/users?limit=9999&offset=0&fields=" & toExtract)
        If InStr(check, "(401)") > 0 Then
            result = "Sessione scaduta, per favore rifai il Login."
            Exit Sub
        End If
        Json = JObject.Parse(check)
        If Json.ContainsKey("list") Then
            NumChild = CInt(Json.Item("count").ToString)
            Contatore = 0
            For Each child In Json.Item("list").Children()
                If Contatore Mod 10 = 0 Then
                    Loading(CInt((Contatore * 100) / NumChild))
                End If
                If Not txtName.Text = "" Then
                    If chkUpLow.Checked Then
                        If InStr(child.Item("username").ToString, txtName.Text) = 0 Then
                            Contatore = Contatore + 1
                            Continue For
                        End If
                    Else
                        If InStr(child.Item("username").ToString.ToLower, txtName.Text.ToLower) = 0 Then
                            Contatore = Contatore + 1
                            Continue For
                        End If
                    End If
                End If
                RowIndex = dtgridExport.Rows.Add()
                dtgridExport.Item(clIndxUserId, RowIndex).Value = child.Item("id").ToString
                dtgridExport.Item(clIndxUser, RowIndex).Value = child.Item("username").ToString
                If chkFirstname.Checked Then
                    dtgridExport.Item(clIndxUserName, RowIndex).Value = child.Item("firstname").ToString
                End If
                If chkLastname.Checked Then
                    dtgridExport.Item(clIndxUserLastname, RowIndex).Value = child.Item("lastname").ToString
                End If
                If chkEmail.Checked Then
                    If child.Item("emailaddress") IsNot Nothing Then
                        dtgridExport.Item(clIndxUserEmail, RowIndex).Value = child.Item("emailaddress").ToString
                    Else
                        dtgridExport.Item(clIndxUserEmail, RowIndex).Value = ""
                    End If
                End If
                If chkFullName.Checked Then
                    dtgridExport.Item(clIndxUserFullName, RowIndex).Value = child.Item("name").ToString
                End If
                If chkLastLogin.Checked Then
                    If child.Item("lastLogin") IsNot Nothing Then
                        dtgridExport.Item(clIndxUserLastLogin, RowIndex).Value = child.Item("lastLogin").ToString
                    Else
                        dtgridExport.Item(clIndxUserLastLogin, RowIndex).Value = ""
                    End If
                End If
                If chkRoles.Checked Then
                    temp = ""
                    For Each role In child.Item("roles").Children
                        temp = AddTextComma(temp, child.Item("name").ToString)
                    Next
                    dtgridExport.Item(clIndxUserRoles, RowIndex).Value = temp
                End If
                If chkEnabled.Checked Then
                    If child.Item("enabled") IsNot Nothing Then
                        dtgridExport.Item(clIndxUserEnabled, RowIndex).Value = child.Item("enabled").ToString
                    Else
                        dtgridExport.Item(clIndxUserEnabled, RowIndex).Value = "False"
                    End If
                End If
            Next
        End If
        Json = Nothing
        result = "Estrazione Completata!!!"
    End Sub

    Private Sub dtgridExport_RowHeaderMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dtgridExport.RowHeaderMouseClick
        btnEnable.Visible = True
        btnDisable.Visible = True
        btnDelete.Visible = True
    End Sub

    Private Sub btnDisable_Click(sender As Object, e As EventArgs) Handles btnDisable.Click
        If (Scairim.logged) Then
            Utilities.timeout = 120000
            Try
                If dtgridExport.CurrentRow.Selected Then
                    Loading("show")
                    For Each row In dtgridExport.SelectedRows
                        DisableUser(row.Cells(0).Value.ToString, False)
                    Next
                    Loading("close")
                Else
                    Logga("Per favore, seleziona una riga e riprova.")
                End If
                Logga(result)
                Loading("show")
                Estrai()
                Loading("close")
                MsgBox("Fatto!")
            Catch ex As Exception
                Logga("ERROR - Disable: " & ex.ToString)
            End Try
        Else
            Logga("Se prima non fai il login su un CM non posso estrarre nulla :)")
        End If
    End Sub

    Private Sub DisableUser(id As String, Enabled As Boolean)
        Dim check As String
        check = GetStuffs(Scairim.token, Scairim.apiToken, Scairim.LinkCM & "/api/rest/users/" & id)
        If InStr(check, "(401)") > 0 Then
            result = "Sessione scaduta, per favore rifai il Login."
            Exit Sub
        End If
        Json = JObject.Parse(check)
        id = Json.Item("id").ToString
        If id <> "" Then
            JsonW = New JObject(
                        New JProperty("authenticationMethod", Json.Item("authenticationMethod")),
                        New JProperty("canChangePassword", Json.Item("canChangePassword")),
                        New JProperty("dateFormat", Json.Item("dateFormat")),
                        New JProperty("emailaddress", Json.Item("emailaddress")),
                        New JProperty("enabled", Enabled),
                        New JProperty("firstname", Json.Item("firstname")),
                        New JProperty("forcePasswordChange", Json.Item("forcePasswordChange")),
                        New JProperty("id", Json.Item("id")),
                        New JProperty("isAutoMediaApprover", Json.Item("isAutoMediaApprover")),
                        New JProperty("isAutoMessageApprover", Json.Item("isAutoMessageApprover")),
                        New JProperty("isSuperAdministrator", Json.Item("isSuperAdministrator")),
                        New JProperty("isWebserviceUser", Json.Item("isWebserviceUser")),
                        New JProperty("language", Json.Item("language")),
                        New JProperty("languageCode", Json.Item("languageCode")),
                        New JProperty("lastname", Json.Item("lastname")),
                        New JProperty("name", Json.Item("name")),
                        New JProperty("passwordLastChanged", Json.Item("passwordLastChanged")),
                        New JProperty("receiveApprovalEmails", Json.Item("receiveApprovalEmails")),
                        New JProperty("receiveEmailAlerts", Json.Item("receiveEmailAlerts")),
                        New JProperty("roles", Json.Item("roles")),
                        New JProperty("timeFormat", Json.Item("timeFormat")),
                        New JProperty("username", Json.Item("username")))
            result = UpdateStuffs(Scairim.token, Scairim.apiToken, JsonW.ToString, Scairim.LinkCM & "/api/rest/users/" & id)
            JsonW.RemoveAll()
            JsonW = Nothing
            If InStr(result, "ERROR") = 0 Then
                If Enabled Then
                    result = "Utente " & Json.Item("username").ToString & " Abilitato."
                Else
                    result = "Utente " & Json.Item("username").ToString & " Disabilitato."
                End If
            End If
            Else
            Logga("id non trovato.")
        End If
    End Sub

    Private Sub btnEnable_Click(sender As Object, e As EventArgs) Handles btnEnable.Click
        If (Scairim.logged) Then
            Utilities.timeout = 120000
            Try
                If dtgridExport.CurrentRow.Selected Then
                    Loading("show")
                    For Each row In dtgridExport.SelectedRows
                        DisableUser(row.Cells(0).Value.ToString, True)
                    Next
                    Loading("close")
                Else
                    Logga("Per favore, seleziona una riga e riprova.")
                End If
                Logga(result)
                Loading("show")
                Estrai()
                Loading("close")
                MsgBox("Fatto!")
            Catch ex As Exception
                Logga("ERROR - Disable: " & ex.ToString)
            End Try
        Else
            Logga("Se prima non fai il login su un CM non posso estrarre nulla :)")
        End If
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        If (Scairim.logged) Then
            Utilities.timeout = 120000
            Try
                If dtgridExport.CurrentRow.Selected Then
                    Loading("show")
                    For Each row In dtgridExport.SelectedRows
                        If MsgBox("Sicuro di voler eliminare l'utente " & row.Cells(1).Value.ToString & "?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                            DeleteUser(row.Cells(0).Value.ToString)
                        End If
                    Next
                    Loading("close")
                Else
                    Logga("Per favore, seleziona una riga e riprova.")
                End If
                Logga(result)
                Loading("show")
                Estrai()
                Loading("close")
                Logga(result)
                MsgBox(result)
            Catch ex As Exception
                Logga("ERROR - Disable: " & ex.ToString)
            End Try
        Else
            Logga("Se prima non fai il login su un CM non posso estrarre nulla :)")
        End If
    End Sub

    Private Sub DeleteUser(id As String)
        result = DeleteStuffs(Scairim.token, Scairim.apiToken, Scairim.LinkCM & "/api/rest/users/" & id)
    End Sub

    Private Sub dtgridExport_CurrentCellChanged(sender As Object, e As EventArgs) Handles dtgridExport.CurrentCellChanged
        btnEnable.Visible = False
        btnDisable.Visible = False
        btnDelete.Visible = False
    End Sub
End Class