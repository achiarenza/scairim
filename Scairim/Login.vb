﻿Imports Newtonsoft.Json.Linq

Public Class Login

    Dim oJson As JObject

    Private Sub Blogin_Click(sender As Object, e As EventArgs) Handles Blogin.Click
        Utilities.timeout = 10000
        Logout()

        Scairim.Username = Tuser.Text
        Scairim.Password = Tpwd.Text
        Scairim.LinkCM = cmbCM.Text.Trim("/")

        If InStr(Scairim.LinkCM, "http://") > 0 Or InStr(Scairim.LinkCM, "https://") > 0 Then
            Logga("Ciao " & Scairim.Username & "! Effettuo il Login su " & Scairim.LinkCM & "...")
            Login()
        Else
            Logga("Url non corretto: " & Scairim.LinkCM & " - Per favore inserisci un URL valido con http o https")
        End If
        Hide()
        Scairim.Show()
    End Sub

    Private Sub Login_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Icon = Scairim.Icon
        cmbCM.Items.Clear()
        Dim file As String = IO.Path.Combine(IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Scairim"), "temp.json")
        If IO.File.Exists(file) Then
            oJson = JObject.Parse(IO.File.ReadAllText(file))
            Tuser.Text = oJson.Item("Username").ToString
            If oJson.ContainsKey("CMs") Then
                For Each cm In oJson.Item("CMs").Children
                    cmbCM.Items.Add(cm.ToString)
                Next
            End If
        End If
    End Sub

    Public Sub Login()
        Dim result As String
        result = GetLogin(Scairim.Username, Scairim.Password, Scairim.LinkCM)
        If InStr(result, "ERROR") = 0 Then
            If Parsa(result) Then
                Scairim.token = EstraiFromJsonSingleValue("token")
                Scairim.apiToken = EstraiFromJsonSingleValue("apiToken")
                If EstraiFromJsonSingleValue("status") = "login.success" Then
                    Logga("Login Eseguito correttamente")
                    Scairim.logged = True
                    ScriviInJson("temp.json", "Username", Scairim.Username)
                    ScriviInJson("temp.json", "CM", Scairim.LinkCM)
                    If Scairim.Username = "a.chiarenza" Then
                        Scairim.DebugToolStripMenuItem.Visible = True
                    End If
                Else
                    Logga("Login Fallito: " & EstraiFromJsonSingleValue("status"))
                End If
            Else
                Logga("ERROR - Login: Parsing del Json fallito.")
            End If
        Else
            Logga("Ops, c'è qualquadra che non cosa. Per favore controlla che URL, username e password siano corretti.")
            Logga("Ricorda che l'URL deve essere nel formato 'http://indirizzo/cm'")
            Logga("ERRORE: " & result)
        End If
    End Sub

    Public Sub Logout()
        If Scairim.logged = True Then
            Logga("Logout: " & GetLogout(Scairim.token, Scairim.apiToken, Scairim.LinkCM))
            Scairim.logged = False
        End If
    End Sub

    Private Sub Tuser_PreviewKeyDown(sender As Object, e As PreviewKeyDownEventArgs) Handles Tuser.PreviewKeyDown
        If e.KeyCode = Keys.Enter Then
            Utilities.timeout = 10000
            Logout()

            Scairim.Username = Tuser.Text
            Scairim.Password = Tpwd.Text
            Scairim.LinkCM = cmbCM.Text.Trim("/")

            If InStr(Scairim.LinkCM, "http://") > 0 Or InStr(Scairim.LinkCM, "https://") > 0 Then
                Logga("Ciao " & Scairim.Username & "! Effettuo il Login su " & Scairim.LinkCM & "...")
                Login()
            Else
                Logga("Url non corretto: " & Scairim.LinkCM & " - Per favore inserisci un URL valido con http o https")
            End If

            Hide()
            Scairim.Show()
        End If
    End Sub

    Private Sub Tpwd_PreviewKeyDown(sender As Object, e As PreviewKeyDownEventArgs) Handles Tpwd.PreviewKeyDown
        If e.KeyCode = Keys.Enter Then
            Utilities.timeout = 10000
            Logout()

            Scairim.Username = Tuser.Text
            Scairim.Password = Tpwd.Text
            Scairim.LinkCM = cmbCM.Text.Trim("/")

            If InStr(Scairim.LinkCM, "http://") > 0 Or InStr(Scairim.LinkCM, "https://") > 0 Then
                Logga("Ciao " & Scairim.Username & "! Effettuo il Login su " & Scairim.LinkCM & "...")
                Login()
            Else
                Logga("Url non corretto: " & Scairim.LinkCM & " - Per favore inserisci un URL valido con http o https")
            End If

            Hide()
            Scairim.Show()
        End If
    End Sub
End Class