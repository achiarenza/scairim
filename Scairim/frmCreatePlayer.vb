﻿Imports Microsoft.Office.Interop
Imports System.Data.OleDb

Public Class FrmCreatePlayer
    Private Sub FrmCreatePlayer_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Icon = Scairim.Icon
        Logga("Creazione Player? Va bene, seleziona le opportune scelte e clicca 'FusRohDah!'")
    End Sub

    Private Sub BtnFile_Click(sender As Object, e As EventArgs) Handles btnFile.Click
        Dim openfile As New OpenFileDialog With {
            .InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyComputer),
            .Filter = "excel files (*.xlsx)|*.xlsx|txt files (*.txt)|*.txt|All files (*.*)|*.*",
            .FilterIndex = 1,
            .RestoreDirectory = True
        }

        If openfile.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            txtFile.Text = openfile.FileName
        End If
    End Sub

    Private Sub BtnGo_Click(sender As Object, e As EventArgs) Handles btnGo.Click

        Select Case True
            Case rdbIntesaSanPaolo.Checked
                CreaSanPaolo()
            Case Else
                Logga("Non ancora Implementato")
        End Select

    End Sub

    '##############################
    '        SanPaolo Zone
    '##############################

#Region "SanPaolo"

    Private Sub CreaSanPaolo()
        Dim oConn As OleDbConnection
        Dim oCommand As OleDbCommand
        Dim da As OleDbDataAdapter
        Dim connectionString As String = "provider=Microsoft.ACE.OLEDB.12.0; Data Source='" & txtFile.Text & "';Extended Properties=Excel 12.0;"
        Dim sql As String
        Try
            If IO.File.Exists(txtFile.Text) And IsExcelFile(txtFile.Text) Then
                oConn = New OleDbConnection(connectionString)
                Dim Index As Integer = 0
                Dim PlayerIndex As Integer = 1
                Dim PlayerName As String
                Dim Banca As String
                Dim CodFiliale As String
                Dim IsVetrina As Boolean
                Dim Sigla As String
                Dim Result As String
                Dim SheetName As String
                Dim RowCount As Integer
                Dim dtSheet As DataTable
                Dim dt As DataTable

                oConn.Open()
                dtSheet = oConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
                SheetName = dtSheet.Rows(0)("TABLE_NAME").ToString()
                sql = "SELECT * FROM [" & SheetName & "]"
                dt = New DataTable
                dt.TableName = SheetName
                da = New OleDbDataAdapter(sql, oConn)
                da.Fill(dt)
                da = Nothing
                RowCount = dt.Rows.Count
                While True
                    If dt.Rows(Index).Item(0) Is DBNull.Value Then
                        Exit While
                    End If
                    CodFiliale = dt.Rows(Index).Item(0)
                    If dt.Rows(Index).Item(12) Is DBNull.Value Then
                        Sigla = GetSiglaSanPaolo(dt.Rows(Index).Item(14))
                        If Sigla = "" Then
                            Logga("Impossibile trovare una Sigla per: " & dt.Rows(Index).Item(14))
                        Else
                            PlayerName = CodFiliale & "-" & Sigla & PlayerIndex.ToString("00")
                            While PlayerExists(PlayerName)
                                PlayerIndex = PlayerIndex + 1
                                PlayerName = CodFiliale & "-" & Sigla & PlayerIndex.ToString("00")
                            End While
                            Banca = GetBanca(CodFiliale)
                            If Banca = "" Then
                                Logga("Nessuna corrispondenza di Banca trovata per il codice: " & CodFiliale.Substring(0, 2))
                            Else
                                If InStr(dt.Rows(Index).Item(14), "VETRINA") > 0 Then
                                    IsVetrina = True
                                Else
                                    IsVetrina = False
                                End If
                                Result = CreaSanPaoloPlayer(PlayerName, Banca, CodFiliale, dt.Rows(Index).Item(10), dt.Rows(Index).Item(8), IsVetrina)
                                If Result = "OK" Then
                                    sql = "UPDATE [" & dt.TableName & "] SET [Nome Player Scala] = '" & PlayerName & "' WHERE [Note] = '" & dt.Rows(Index).Item(14) & "'"
                                    oCommand = oConn.CreateCommand()
                                    oCommand.CommandText = sql
                                    oCommand.ExecuteNonQuery()
                                    oCommand = Nothing
                                    Logga("Creato sul CM di Intesa Sanpaolo: " & PlayerName)
                                Else
                                    Logga(Result)
                                End If
                            End If
                        End If
                    End If
                    Index = Index + 1
                    PlayerIndex = 1
                End While
                oConn.Close()
                oConn = Nothing
                dt = Nothing
            Else
                Logga("File " & txtFile.Text & "non trovato o non è un file Excel")
            End If
        Catch ex As Exception
            Logga("ERROR - CreaSanPaolo: " & ex.ToString)
        End Try
    End Sub

    Private Function GetBanca(CodFiliale As String)
        If CodFiliale.Substring(0, 2) = "01" Then
            GetBanca = "INTESA SANPAOLO"
        Else
            GetBanca = ""
        End If
    End Function

    Private Function GetSiglaSanPaolo(type As String)
        Select Case True
            Case InStr(type, "ACCOGLIENZA") > 0
                GetSiglaSanPaolo = "AO"
            Case InStr(type, "VETRINA") > 0
                GetSiglaSanPaolo = "VE"
            Case InStr(type, "LIVING") > 0
                GetSiglaSanPaolo = "OI"
            Case InStr(type, "VIDEOCONFER") > 0
                GetSiglaSanPaolo = "VC"
            Case InStr(type, "PRIVACY") > 0
                GetSiglaSanPaolo = "OS"
            Case InStr(type, "CODOMETRO") > 0
                GetSiglaSanPaolo = "CD"
            Case Else
                GetSiglaSanPaolo = ""
        End Select
    End Function

    '#Deprecated

    Private Sub CreaSanPaoloWExcel()
        If IO.File.Exists(txtFile.Text) And IsExcelFile(txtFile.Text) Then
            Dim xlApp As Excel.Application = Nothing
            Dim xlWorkBooks As Excel.Workbooks = Nothing
            Dim xlWorkBook As Excel.Workbook = Nothing
            Dim xlWorkSheet As Excel.Worksheet = Nothing
            Dim xlWorkSheets As Excel.Sheets = Nothing
            Dim xlCells As Excel.Range = Nothing
            Dim Index As Integer = 2
            Dim PlayerIndex As Integer = 1
            Dim PlayerName As String
            Dim Banca As String
            Dim CodFiliale As String
            Dim IsVetrina As Boolean
            Dim Sigla As String
            Dim Result As String
            xlApp = New Excel.Application
            xlApp.DisplayAlerts = False
            xlWorkBooks = xlApp.Workbooks
            xlWorkBook = xlWorkBooks.Open(txtFile.Text)
            xlWorkSheets = xlWorkBook.Sheets
            xlWorkSheet = xlWorkSheets(1)
            xlWorkSheet.Activate()

            While True
                CodFiliale = xlWorkSheet.Range("A" & Index.ToString).Value
                If CodFiliale = "" Then
                    Exit While
                End If
                If xlWorkSheet.Range("M" & Index.ToString).Value = "" Then
                    Sigla = GetSiglaSanPaolo(xlWorkSheet.Range("O" & Index.ToString).Value)
                    If Sigla = "" Then
                        Logga("Impossibile trovare una Sigla per: " & xlWorkSheet.Range("O" & Index.ToString).Value)
                    Else
                        PlayerName = CodFiliale & "-" & Sigla & PlayerIndex.ToString("00")
                        While PlayerExists(PlayerName)
                            PlayerIndex = PlayerIndex + 1
                            PlayerName = CodFiliale & "-" & Sigla & PlayerIndex.ToString("00")
                        End While
                        Banca = GetBanca(CodFiliale)
                        If Banca = "" Then
                            Logga("Nessuna corrispondenza di Banca trovata per il codice: " & CodFiliale.Substring(0, 2))
                        Else
                            If InStr(xlWorkSheet.Range("O" & Index.ToString).Value, "VETRINA") > 0 Then
                                IsVetrina = True
                            Else
                                IsVetrina = False
                            End If
                            Result = CreaSanPaoloPlayer(PlayerName, Banca, CodFiliale, xlWorkSheet.Range("K" & Index.ToString).Value, xlWorkSheet.Range("I" & Index.ToString).Value, IsVetrina)
                            If Result = "OK" Then
                                xlWorkSheet.Range("M" & Index.ToString).Value = PlayerName
                                Logga("Creato sul CM di Intesa Sanpaolo: " & PlayerName)
                            Else
                                Logga(Result)
                            End If
                        End If
                    End If
                End If
                Index = Index + 1
                PlayerIndex = 1
            End While

            xlWorkBook.Save()
            xlWorkBook.Close()
            xlApp.Quit()
            ReleaseComObject(xlCells)
            ReleaseComObject(xlWorkSheets)
            ReleaseComObject(xlWorkSheet)
            ReleaseComObject(xlWorkBook)
            ReleaseComObject(xlWorkBooks)
            ReleaseComObject(xlApp)
        Else
            Logga("File " & txtFile.Text & "non trovato o non è un file Excel")
        End If
    End Sub

#End Region

End Class